﻿using DbUp;
using DbUp.Helpers;
using DbUp.ScriptProviders;
using Microsoft.Extensions.Configuration;
using System;
using System.Configuration;
using System.IO;

namespace mss_dbup
{
    class Program
    {
        static string application = "";
        static bool IsSilentMode = false;
        static int Timeout = 1;
        static int Main(string[] args)
        {
            if (args.Length > 0)
                bool.TryParse(args[0], out IsSilentMode);

            if (IsSilentMode)
                Console.WriteLine("DB-up is running in a silent mode");

            IConfiguration configuration = new ConfigurationBuilder()
              .AddJsonFile("appsettings.json", true, true)
              .Build();

            application = configuration.GetValue<string>("Application");
            string connectionString = configuration.GetValue<string>("ConnectionString");
            Timeout = configuration.GetValue<int>("Timeout");

            string scriptDeploymentsPath = Path.Combine(Directory.GetCurrentDirectory(), "Scripts", "MSS");
            if (application.Equals("snapp", StringComparison.InvariantCultureIgnoreCase))
            {
                scriptDeploymentsPath = Path.Combine(Directory.GetCurrentDirectory(), "Scripts", "SNAPP");
            }

            if (ScriptsExecution(connectionString, scriptDeploymentsPath, "PreDeployment") != 0)
                return -1;
            Console.WriteLine($"Processing scripts for '{application.ToUpper()}' from 'PreDeployment' folder finished");

            if (ScriptsExecution(connectionString, scriptDeploymentsPath, "Deployment") != 0)
                return -1;
            Console.WriteLine($"Processing scripts for '{application.ToUpper()}' from 'Deployment' folder finished");

            ScriptsExecution(connectionString, scriptDeploymentsPath, "PostDeployment");
            Console.WriteLine($"Processing scripts for '{application.ToUpper()}' from 'PostDeployment' folder finished");
            return 0;
        }
        static void WriteMessage(ConsoleColor consoleColor, string message)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        static int ScriptsExecution(string connectionString, string scriptsPath, string endFolder)
        {
            string answer = "";
            Console.WriteLine($"Processing scripts for '{application.ToUpper()}' from '{endFolder}' folder.");

            var config =
                   DeployChanges.To
                       .MySqlDatabase(connectionString)
                       .WithScriptsFromFileSystem(Path.Combine(scriptsPath, endFolder), new FileSystemScriptOptions
                       {
                           IncludeSubDirectories = false
                       })
                       .WithExecutionTimeout(TimeSpan.FromMinutes(Timeout))
                       .WithTransactionPerScript()
                       .LogToConsole()
                       .LogScriptOutput();

            var upgradeEngine = endFolder.Equals("Deployment") ? config.Build() : config.JournalTo(new NullJournal()).Build();
            var scripts = upgradeEngine.GetScriptsToExecute();
            if (scripts.Count == 0)
            {
                WriteMessage(ConsoleColor.Red, $"For the '{application.ToUpper()}' database zero scripts needs to be executed");
                return 0;
            }

            Console.WriteLine("Provided scripts will be executed:");
            foreach (var script in scripts)
            {
                Console.WriteLine($"\t{script.Name}");
            }

            do
            {
                if (IsSilentMode)
                    break;
                //Ask the user if they want to continue
                Console.WriteLine("Would you like to continue execution (y or n)?");
                //Get the user's response and validate that it is either 'y' or 'n'.
                answer = Console.ReadLine();
            } while (!(answer.Equals("y", StringComparison.InvariantCultureIgnoreCase) || answer.Equals("n", StringComparison.InvariantCultureIgnoreCase)));

            if (answer.Equals("n", StringComparison.InvariantCultureIgnoreCase))
            {
                WriteMessage(ConsoleColor.Yellow, "Execution cancelled by user");
                return 0;
            }

            //scripts execution command
            var result = upgradeEngine.PerformUpgrade();

            if (!result.Successful)
            {
                WriteMessage(ConsoleColor.Red, result.Error.ToString());
                return -1;
            }

            WriteMessage(ConsoleColor.Green, "Success!");
            return 0;
        }
    }
}
