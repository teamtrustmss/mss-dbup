-- delete old data in snapp mssrequests
START TRANSACTION;
delete * from emails e where Id in (
select EmailId from mssrequests m where ModifyDate < DATE_SUB(NOW(), INTERVAL 1 MONTH) and Success = 1);
COMMIT;

START TRANSACTION;
delete * from mssrequests m where ModifyDate < DATE_SUB(NOW(), INTERVAL 1 MONTH) and Success = 1;
COMMIT;