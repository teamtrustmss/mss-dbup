START TRANSACTION;

	CREATE TABLE `dbo_OrderingCustomerContactTypes` (
	  `Id` int NOT NULL,
	  `Name` varchar(50) NOT NULL,
	  `SortOrder` int NOT NULL DEFAULT '0',
	  PRIMARY KEY (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO MssPlatformDb.dbo_OrderingCustomerContactTypes (Id,Name,SortOrder) VALUES
		 (0,'Contract / Commercial',0),
		 (1,'Service Updates',10),
		 (2,'IT/Integration Contacts',20),
		 (3,'Deployments',30),
		 (4,'HIV positive',40),
		 (5,'Adverse result',50),
		 (6,'Data Breach',60);

	CREATE TABLE `dbo_OrderingCustomerContacts` (
	  `Id` varchar(64) NOT NULL,
	  `OrderingCustomerId` varchar(64) NOT NULL,
	  `ContactTypeId` int NOT NULL,
	  `FullName` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  `Email` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  `PhoneNumber` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
	  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
	  `CreateTime` datetime(6) NOT NULL,
	  `ModifyTime` datetime(6) NOT NULL,
	  PRIMARY KEY (`Id`),
	  UNIQUE KEY `Id` (`Id`),
	  KEY `FK_OrderingCustomerContacts_ContactTypes` (`ContactTypeId`),
	  KEY `FK_OrderingCustomerContacts_OrderingCustomers` (`OrderingCustomerId`),
	  CONSTRAINT `FK_OrderingCustomerContacts_ContactTypes` FOREIGN KEY (`ContactTypeId`) REFERENCES `dbo_OrderingCustomerContactTypes` (`Id`),
	  CONSTRAINT `FK_OrderingCustomerContacts_OrderingCustomers` FOREIGN KEY (`OrderingCustomerId`) REFERENCES `dbo_OrderingCustomers` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

COMMIT;