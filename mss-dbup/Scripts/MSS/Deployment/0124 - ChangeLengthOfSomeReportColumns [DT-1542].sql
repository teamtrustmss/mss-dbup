START TRANSACTION;

	ALTER TABLE dbo_OrderExamsReportsData MODIFY COLUMN ExamLocation text NULL;
	ALTER TABLE dbo_OrderExamsReportsDataNew MODIFY COLUMN ExamLocation text NULL;

	ALTER TABLE dbo_OrderExamsReportsData MODIFY COLUMN GpName varchar(550) NULL;
	ALTER TABLE dbo_OrderExamsReportsDataNew MODIFY COLUMN GpName varchar(550) NULL;

COMMIT;