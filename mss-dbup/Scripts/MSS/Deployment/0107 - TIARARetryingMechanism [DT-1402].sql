START TRANSACTION;

	CREATE TABLE `dbo_MdgRetryingRequests` (
	  `Id` varchar(64) NOT NULL,
	  `OrderExamId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  `TypeId` int NOT NULL,
	  `MdgRef` int DEFAULT NULL,
	  `RequestAttempts` int NOT NULL DEFAULT '0',
	  `Success` tinyint(1) NOT NULL DEFAULT '0',
	  `Error` longtext,
	  `NextRunAt` datetime NOT NULL,
	  `CreateTime` datetime(6) NOT NULL,
	  `ModifyTime` datetime(6) NOT NULL,
	  PRIMARY KEY (`Id`),
	  UNIQUE KEY `dbo_MdgRetryingRequests_Id` (`Id`),
	  KEY `dbo_MdgRetryingRequests_MdgRef` (`MdgRef`) USING BTREE,
	  KEY `dbo_MdgRetryingRequests_TypeId` (`TypeId`),
	  CONSTRAINT `FK_dbo_MdgRetryingRequests_OrderExamId` FOREIGN KEY (`OrderExamId`) REFERENCES `dbo_OrderExams` (`Id`) ON DELETE CASCADE
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

COMMIT;