START TRANSACTION;

	CREATE TABLE dbo_OrderExamTiaraAudioFiles (
		`Id` varchar(64) NOT NULL,
		`OrderExamId` varchar(64) NOT NULL,
		`DocumentId` varchar(64) NOT NULL,
		`UploadTime` datetime(6),
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		KEY `FK_OrderExamTiaraAudioFiles_OrderExams` (`OrderExamId`),
		KEY `FK_OrderExamTiaraAudioFiles_Documents` (`DocumentId`),
		CONSTRAINT `FK_OrderExamTiaraAudioFiles_OrderExams` FOREIGN KEY (`OrderExamId`) REFERENCES `dbo_OrderExams` (`Id`),
		CONSTRAINT `FK_OrderExamTiaraAudioFiles_Documents` FOREIGN KEY (`DocumentId`) REFERENCES `dbo_Documents` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		('a61d912b-b993-4a0e-b3a4-71a17d83e559',155,'Tiara Mp3','2024-07-04 12:00:00','2024-07-04 12:00:00',1);
	
	ALTER TABLE dbo_MdgRetryingRequests ADD OrderExamAudioFileId varchar(64) NULL;
	
	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('c447f22f-0cbf-4f69-9561-c9e3c7eee83c','11012','Tele-interview case has been completed in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('288fe773-3126-40c1-b887-5a25ba8dc2a5','11013','Tele-interview case could not be completed in MDG, moved to request queue',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('a299799d-3890-4070-a746-ab4aafc0dc0c','11014','Tele-interview audio file has been uploaded in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('01b6b4e1-3b46-4821-b358-bbdc53702009','11015','Tele-interview audio file could not be uploaded to MDG, moved to request queue',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);

COMMIT;