START TRANSACTION;
	
	ALTER TABLE dbo_OrderingCustomers ADD LastCreatedExamDate datetime(6) NULL;
	
	CREATE EVENT run_recalculateLastCreatedExamDatesForOCs
	ON SCHEDULE EVERY 1 DAY
	STARTS '2023-08-09 03:00:00.000'
	ON COMPLETION NOT PRESERVE
	ENABLE
	DO 				
	UPDATE dbo_OrderingCustomers doc
	INNER JOIN (
			SELECT MAX(oe.CreateTime) AS LatestExamCreateTime, o.OrderingCustomerId AS OrderingCustomerId
			FROM dbo_Orders o
			INNER JOIN dbo_OrderExams oe ON oe.OrderId = o.Id
			GROUP BY o.OrderingCustomerId
		) AS subquery
	SET doc.LastCreatedExamDate = subquery.LatestExamCreateTime
		WHERE doc.Id = subquery.OrderingCustomerId;
	
COMMIT;