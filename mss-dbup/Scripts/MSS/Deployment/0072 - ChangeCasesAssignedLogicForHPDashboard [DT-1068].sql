START TRANSACTION;

	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridHealthPractitioners` AS
	select
		`me`.`Id` AS `Id`,
		(case
			when (nullif(`me`.`MiddleName`, '') is null) then (case
				when (`t`.`Id` = 2) then ltrim(concat(`me`.`FirstName`, ' ', `me`.`LastName`))
				else ltrim(concat(ifnull(`me`.`Title`, ''), ' ', `me`.`FirstName`, ' ', `me`.`LastName`))
			end)
			else (case
				when (`t`.`Id` = 2) then ltrim(concat(`me`.`FirstName`, ' ', `me`.`MiddleName`, ' ', `me`.`LastName`))
				else ltrim(concat(ifnull(`me`.`Title`, ''), ' ', `me`.`FirstName`, ' ', `me`.`MiddleName`, ' ', `me`.`LastName`))
			end)
		end) AS `Name`,
		`me`.`ProfRegNumber` AS `ProfRegNumber`,
		`me`.`PreferredEmail` AS `Email`,
		`me`.`MobilePhone` AS `PhoneNumber`,
		(
		select
			group_concat(`eadr`.`PostCode` separator ', ')
		from
			`MssPlatformDb`.`dbo_ExaminerAddresses` `eadr`
		where
			(`eadr`.`ExaminerId` = `e`.`Id`)) AS `PostCode`,
		if((`eas`.`Name` = 'Unknown'),
		'',
		`eas`.`Name`) AS `Availability`,
		`ea`.`AvailabilityNotes` AS `AvailabilityNotes`,
		(
		select
			group_concat(`dc`.`Name` separator ', ')
		from
			(`MssPlatformDb`.`dbo_Capabilities` `dc`
		left join `MssPlatformDb`.`dbo_FeeGroupExaminerCapabilities` `dfgec` on
			((`dfgec`.`FeeGroupExaminerId` = `fge`.`Id`)))
		where
			((`dfgec`.`CapabilityId` = `dc`.`Id`)
				and (`dc`.`CapabilityTypeId` = 1)
					and (`t`.`Id` = 2))) AS `Qualifications`,
		(
		select
			group_concat(`dpc`.`Code` separator ',')
		from
			(`MssPlatformDb`.`dbo_MedicalExaminerServiceAreas_PostCodes` `dmesapc`
		left join `MssPlatformDb`.`dbo_PostCodes` `dpc` on
			((`dmesapc`.`PostCodeId` = `dpc`.`Id`)))
		where
			((`dmesapc`.`MedicalExaminerServiceAreaId` = `dmesa`.`Id`)
				and (`t`.`Id` = 2))) AS `ServiceArea`,
		`ea`.`WeeklyCapacity` AS `WeeklyCapacity`,
		(
		select
			count(0)
		from
			`MssPlatformDb`.`dbo_OrderExams` `doe`
		where
			((`me`.`Id` = `doe`.`AssignedExaminerId`)
				and (`doe`.`ExamTypeId` = 1)
				and (`doe`.`StatusId` in (1, 2, 8, 2048, 4096)))) AS `CasesAssigned`,
		`dmesa`.`Radius` AS `Radius`,
		`dmesa`.`RadiusUnitId` AS `RadiusUnitId`,
		if((`eas`.`Id` = 1),
		1,
		0) AS `Available`,
		`me`.`FollowUpped` AS `FollowUp`,
		`me`.`LastCompletedDate` AS `LastCompletedDate`,
		(
		select
			min(`doea`.`Date`)
		from
			(`MssPlatformDb`.`dbo_OrderExams` `doe`
		join `MssPlatformDb`.`dbo_OrderExamAppointments` `doea` on
			((`doea`.`Id` = `doe`.`Id`)))
		where
			((`me`.`Id` = `doe`.`AssignedExaminerId`)
				and (`doe`.`ExamTypeId` = 1)
					and (`doea`.`Date` is not null))) AS `MinScheduledAppointmentTime`,
		(
		select
			max(`doea`.`Date`)
		from
			(`MssPlatformDb`.`dbo_OrderExams` `doe`
		join `MssPlatformDb`.`dbo_OrderExamAppointments` `doea` on
			((`doea`.`Id` = `doe`.`Id`)))
		where
			((`me`.`Id` = `doe`.`AssignedExaminerId`)
				and (`doe`.`ExamTypeId` = 1)
					and (`doea`.`Date` is not null))) AS `MaxScheduledAppointmentTime`,
		`me`.`ApprovedDate` AS `ActiveDate`,
		`me`.`FirstAssignationDate` AS `FirstAssignationDate`,
		`me`.`DateOfBirth` AS `DateOfBirthday`,
		`me`.`FollowUpDate` AS `FollowUpDate`,
		`me`.`LicenceExpiry` AS `LicenceExpiry`,
		`ea`.`AwayFrom` AS `AwayFrom`,
		`ea`.`ReturnDate` AS `ReturnDate`,
		`e`.`CreateTime` AS `CreateTime`,
		`e`.`IsActive` AS `IsActive`
	from
		(((((((`MssPlatformDb`.`dbo_MedicalExaminers` `me`
	left join `MssPlatformDb`.`dbo_Examiners` `e` on
		((`e`.`Id` = `me`.`Id`)))
	left join `MssPlatformDb`.`dbo_ExaminerAvailabilities` `ea` on
		((`e`.`Id` = `ea`.`ExaminerId`)))
	left join `MssPlatformDb`.`dbo_MedicalExaminerServiceAreas` `dmesa` on
		((`me`.`Id` = `dmesa`.`Id`)))
	left join `MssPlatformDb`.`dbo_ExaminerAvailabilityStatuses` `eas` on
		((`eas`.`Id` = `ea`.`StatusId`)))
	left join `MssPlatformDb`.`dbo_FeeGroupExaminers` `fge` on
		((`fge`.`Id` = `me`.`Id`)))
	left join `MssPlatformDb`.`dbo_ExaminerTypes` `t` on
		((`t`.`Id` = `e`.`ExaminerTypeId`))))
	where
		((`t`.`Id` = 2)
			and (`fge`.`ExaminerStatusId` = 0));

COMMIT;