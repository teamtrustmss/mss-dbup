START TRANSACTION;

	DROP TRIGGER afterUpdate_dbo_Applicants;

DELIMITER $$ 
	CREATE TRIGGER `afterUpdate_dbo_Applicants` AFTER UPDATE ON `dbo_Applicants` FOR EACH ROW BEGIN 
		UPDATE grid_AllOrderExams SET 
			ApplicantFirstName = NEW.FirstName,
			ApplicantLastName = NEW.LastName,
			ApplicantDoB = NEW.DateOfBirth,
			Applicant = CONCAT(NEW.FirstName, ' ', NEW.LastName)
		WHERE OrderId = NEW.Id;
	END$$

DELIMITER ;

DELIMITER $$ 
	CREATE TRIGGER `afterInsert_dbo_ApplicantAddresses` AFTER INSERT ON `dbo_ApplicantAddresses` FOR EACH ROW BEGIN 
		UPDATE grid_AllOrderExams SET 
			ApplicantPostCode = (SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
						FROM dbo_Applicants a
						LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
						WHERE NEW.ApplicantId = a.Id
						GROUP BY a.Id, aa.ApplicantId)
		WHERE OrderId = NEW.ApplicantId;
	END$$

DELIMITER ;

DELIMITER $$ 
	CREATE TRIGGER `afterUpdate_dbo_ApplicantAddresses` AFTER UPDATE ON `dbo_ApplicantAddresses` FOR EACH ROW BEGIN 
		UPDATE grid_AllOrderExams SET 
			ApplicantPostCode = (SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
						FROM dbo_Applicants a
						LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
						WHERE NEW.ApplicantId = a.Id
						GROUP BY a.Id, aa.ApplicantId)
		WHERE OrderId = NEW.ApplicantId;
	END$$

DELIMITER ;

DELIMITER $$ 
	CREATE TRIGGER `afterDelete_dbo_ApplicantAddresses` AFTER DELETE ON `dbo_ApplicantAddresses` FOR EACH ROW BEGIN 
		UPDATE grid_AllOrderExams SET 
			ApplicantPostCode = (SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
						FROM dbo_Applicants a
						LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
						WHERE OLD.ApplicantId = a.Id
						GROUP BY a.Id, aa.ApplicantId)
		WHERE OrderId = OLD.ApplicantId;
	END$$

DELIMITER ;

COMMIT;