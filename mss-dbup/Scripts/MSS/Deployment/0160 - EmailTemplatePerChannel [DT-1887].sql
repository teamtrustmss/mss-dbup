START TRANSACTION;

	-- Update templates for "AllEvidenceTypes" (evidence types with Nurse, Doctor, GPReport, KitFulfilment, Tiara, Care)
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 422 -- 422 = Nurse (2) | Doctor (4) | GPReport (8) | KitFulfilment (32) | Tiara (128) | Care (256)
		WHERE dt.ReferenceNumber IN (301, 302, 303, 305, 306, 307, 308, 310, 311); -- QueriedCases, CaseUpdatedSchedule, etc.

	-- Update templates with specific evidence types

	-- Templates with "Nurse" only
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 2 -- 2 = Nurse
		WHERE dt.ReferenceNumber IN (201, 220); -- ExamAssignedMENotScheduled, ExamChangedNotScheduled

	-- Templates with "Doctor" only
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 4 -- 4 = Doctor
		WHERE dt.ReferenceNumber IN (224); -- ExamScheduledDoctorNotification

	-- Templates with "GPReport" only
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 8 -- 8 = GPReport
		WHERE dt.ReferenceNumber IN (318); -- CustomerToViewReport

	-- Templates with "Nurse" and "Doctor"
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 6 -- 6 = Nurse (2) | Doctor (4)
		WHERE dt.ReferenceNumber IN (223, 226, 316); -- ExamCancelledScheduled, ExamCancelledNotScheduled, AssignedToExaminer

	-- Templates with "Tiara" and "GPReport"
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 136 -- 136 = Tiara (128) | GPReport (8)
		WHERE dt.ReferenceNumber IN (319); -- ICPOrderingCustomerAdded

	-- Templates with "Nurse", "Doctor", "Tiara", and "Care"
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 390 -- 390 = Nurse (2) | Doctor (4) | Tiara (128) | Care (256)
		WHERE dt.ReferenceNumber IN (312, 225, 307, 308, 317); -- DNA, ExamScheduledApplicantNotification, etc.

COMMIT;