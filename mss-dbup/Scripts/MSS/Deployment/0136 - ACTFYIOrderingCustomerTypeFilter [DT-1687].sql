START TRANSACTION;

	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridACTFYIActivities` AS
	SELECT
		`EA`.`Id` AS `Id`,
		`EA`.`OrderExamId` AS `OrderExamId`,
		`E`.`ExamCustomId` AS `ExamCustomId`,
		`E`.`StatusId` AS `ExamStatus`,
		`E`.`ExamTypeId` AS `ExamType`,
		`EA`.`NotificationType` AS `NotificationType`,
		`A`.`Code` AS `Code`,
		`EA`.`CustomDescription` AS `CustomDescription`,
		`EA`.`CreateTime` AS `CreateTime`,
		`EA`.`NextActionDate` AS `NextActionDate`,
		`C`.`CompanyName` AS `CompanyName`,
		`C`.`OrderingCustomerTypeId` AS `OrderingCustomerType`,
		`EA`.`IsActive` AS `IsActive`,
		rtrim(concat(coalesce(concat(`U`.`FirstName`, ' '), ''), coalesce(concat(nullif(`U`.`MiddleName`, ''), ' '), ''), coalesce(concat(`U`.`LastName`, ' '), ''))) AS `ExamOwner`,
		`AC`.`Id` AS `ActivityType`
	FROM
		`MssPlatformDb`.`dbo_OrderExamActivities` EA
		INNER JOIN `MssPlatformDb`.`dbo_Activities` A ON EA.`ActivityId` = A.`Id`
		INNER JOIN `MssPlatformDb`.`dbo_ActivityTypes` AC ON AC.`Id` = A.`ActivityTypeId`
		INNER JOIN `MssPlatformDb`.`dbo_OrderExams` E ON EA.`OrderExamId` = E.`Id`
		INNER JOIN `MssPlatformDb`.`dbo_Orders` O ON E.`OrderId` = O.`Id`
		INNER JOIN `MssPlatformDb`.`dbo_OrderingCustomers` C ON O.`OrderingCustomerId` = C.`Id`
		LEFT JOIN `MssPlatformDb`.`dbo_Users` U ON U.`Id` = E.`ExamOwnerId`
	WHERE
		EA.`NotificationType` > 0;

COMMIT;