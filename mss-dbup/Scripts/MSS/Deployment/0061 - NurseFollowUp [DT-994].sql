START TRANSACTION;

	ALTER TABLE dbo_MedicalExaminers ADD FollowUpDate datetime(6) NULL;
	ALTER TABLE dbo_MedicalExaminers ADD FollowUpped tinyint(1) DEFAULT 0 NOT NULL;

COMMIT;