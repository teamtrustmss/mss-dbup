
DELETE FROM dbo_ApplicationSettings 
WHERE ContainerName  = 'Distribution.MSS.API'
AND `Key` IN (
    'Documents_AmazonS3AccessKeyId', 
    'Documents_AmazonS3BucketName', 
    'Documents_AmazonS3Region', 
    'Documents_AmazonS3SecretAccessKey',
    'JsonGeneration_AmazonS3AccessKeyId', 
    'JsonGeneration_AmazonS3BucketName', 
    'JsonGeneration_AmazonS3Region', 
    'JsonGeneration_AmazonS3SecretAccessKey', 
    'JsonGeneration_UploadRequirements_AmazonS3BucketName');
    
INSERT INTO dbo_ApplicationSettings (`Key`, `ContainerName`, `Value`, `Comment`)
SELECT `Key`, 'Distribution.MSS.API', `Value`, `Comment`
FROM dbo_ApplicationSettings 
WHERE `Key` IN (
    'Documents_AmazonS3AccessKeyId', 
    'Documents_AmazonS3BucketName', 
    'Documents_AmazonS3Region', 
    'Documents_AmazonS3SecretAccessKey',
    'JsonGeneration_AmazonS3AccessKeyId', 
    'JsonGeneration_AmazonS3BucketName', 
    'JsonGeneration_AmazonS3Region', 
    'JsonGeneration_AmazonS3SecretAccessKey', 
    'JsonGeneration_UploadRequirements_AmazonS3BucketName');
	