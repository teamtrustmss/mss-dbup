START TRANSACTION;

	ALTER TABLE dbo_OrderExamAppointmentsHistory ADD OtherRelevantInformation varchar(150) NULL;
	ALTER TABLE dbo_OrderExamAppointmentsHistory ADD CancelledReason varchar(250) NULL;

COMMIT;