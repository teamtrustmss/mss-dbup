START TRANSACTION;

	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		('9d3688e7-acb4-4132-b181-2715302f240d',319,'Email Exam ICP Ordering Customer Added',NOW(),NOW(),1);
		
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsICPOrderingCustomerAdded varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsICPOrderingCustomerAddedNonReply varchar(256) NULL;

COMMIT;