START TRANSACTION;

	INSERT INTO dbo_ApplicantAddressTypes (Id,Name,SortOrder) VALUES (3,'Care Home',30);
	
	ALTER TABLE dbo_ApplicantAddresses
		ADD EmailAddress varchar(255) NULL,
		ADD ContactName varchar(100) NULL;
		
	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('88096f4c-24da-4ee9-b13a-bfe0578fae01','11038','Care home letter has been sent',7,NULL,NULL,0,0,0,48010,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('942fdb6c-6858-420d-bc63-b7b7d5c2dfe8','11037','AMRA not uploaded. Please upload so letter can be sent to Care Home regarding Tele-interview',7,NULL,NULL,0,0,0,48011,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('f6b86051-f93f-49c0-bf3d-7455ade64c5b','11036','Care Home email address missing. Please obtain so letter can be sent to Care Home regarding Tele-interview',7,NULL,NULL,0,0,0,48015,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);
		
	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		('1887da84-4ea0-4cd5-9ac4-bb6d161181b8',812,'Email Care Home Reminder',NOW(),NOW(),1);

COMMIT;