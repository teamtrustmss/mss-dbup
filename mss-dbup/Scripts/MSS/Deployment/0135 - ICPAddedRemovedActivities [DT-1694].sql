START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('ed370805-4161-ef11-9b30-f09e4a5e690f','720','ICP Ordering Customer/s removed: [OCs]',7,NULL,NULL,0,0,0,48015,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('67fba639-5542-44fc-b8f7-389d4a2afb64','721','ICP Ordering Customer/s added: [OCs]',7,NULL,NULL,0,0,0,48015,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);

COMMIT;