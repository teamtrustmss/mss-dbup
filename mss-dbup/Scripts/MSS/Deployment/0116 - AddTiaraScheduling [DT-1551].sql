START TRANSACTION;

	ALTER TABLE dbo_OrderExams ADD TiaraBookingId int NULL;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('6e85ac25-0927-4a0a-bbd2-1e68d0bd20ae','11009','Tele-interview appointment has been scheduled in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);
		
COMMIT;