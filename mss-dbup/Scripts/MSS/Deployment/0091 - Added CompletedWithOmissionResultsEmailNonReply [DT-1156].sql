START TRANSACTION;

ALTER TABLE dbo_OrderingCustomerTeams
ADD CompletedWithOmissionResultsEmailNonReply varchar(256) DEFAULT NULL;

ALTER TABLE dbo_OrderingCustomerTeams
ADD CompletedWithOmissionResultsEmail varchar(256) DEFAULT NULL;

UPDATE dbo_OrderingCustomerTeams doct
SET doct.CompletedWithOmissionResultsEmailNonReply = doct.CompletedResultsEmailNonReply
WHERE doct.CompletedResultsEmailNonReply IS NOT NULL; 

UPDATE dbo_OrderingCustomerTeams doct
SET doct.CompletedWithOmissionResultsEmail = doct.CompletedResultsEmail
WHERE doct.CompletedResultsEmail IS NOT NULL; 

COMMIT;