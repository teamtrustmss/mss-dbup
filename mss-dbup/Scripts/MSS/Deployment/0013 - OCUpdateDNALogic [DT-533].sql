START TRANSACTION;

ALTER TABLE dbo_OrderingCustomers CHANGE DNAServiceCodeExclusion DNAAffectedServiceCodes varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

COMMIT;