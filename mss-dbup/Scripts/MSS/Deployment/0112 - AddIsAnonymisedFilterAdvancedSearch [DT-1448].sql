START TRANSACTION;

	CREATE INDEX grid_AllOrderExams_IsAnonymised ON grid_AllOrderExams (IsAnonymised);
	CREATE INDEX grid_AllOrderExams_IsActive ON grid_AllOrderExams (IsActive);
	CREATE INDEX grid_AllOrderExams_NextActionDateSort ON grid_AllOrderExams (NextActionDateSort);
	CREATE INDEX grid_AllOrderExams_NextActionDateSortDesc ON grid_AllOrderExams (NextActionDateSort DESC);

COMMIT;