START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		 ('18932160-a334-4541-b70e-fbeac2acee66','710','The document has been reviewed',7,NULL,NULL,0,0,0,64463,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		 ('5b827c7d-0c9d-4eef-9cad-9d18e91fc898','711','The document has been downloaded',7,NULL,NULL,0,0,0,64463,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);

COMMIT;