START TRANSACTION;

	ALTER TABLE MssPlatformDb.dbo_MedicalExaminers ADD LastCompletedDate datetime(6) NULL;
	
	DROP TRIGGER afterInsert_OrderExamActivities;

DELIMITER $$ 

CREATE TRIGGER `afterInsert_OrderExamActivities` AFTER INSERT ON `dbo_OrderExamActivities` FOR EACH ROW BEGIN 

    UPDATE grid_AllOrderExams SET 

        LatestActivityId = NEW.Id,

        TimeService = IFNULL((SELECT dbo_GetTimeServiceForActivity(NEW.Id, NULL)),0),

        NextActionDate = NEW.NextActionDate, 

        NextActionDateSort = IFNULL(NEW.NextActionDate, '9999-12-31'),

        AssignedToPrefix = NEW.AssignedToPrefix,

        AssignedTo = (CASE WHEN NEW.AssignedToPrefix IS NULL 

            THEN NEW.AssignedTo 

            ELSE CONCAT(NEW.AssignedToPrefix, ' - ', NEW.AssignedTo) 

            END),

        CreatorName = (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)

            FROM dbo_Users creator 

            WHERE NEW.CreatorId = creator.Id

             AND NEW.PreviousActivityId IS NULL

            LIMIT 1)

    WHERE Id = NEW.OrderExamId;
   
   UPDATE dbo_MedicalExaminers
	   INNER JOIN (
	   		SELECT doe.AssignedExaminerId, MAX(`doea`.`CreateTime`) AS CreateTime
	        FROM `MssPlatformDb`.`dbo_OrderExamActivities` `doea`
	        LEFT JOIN `MssPlatformDb`.`dbo_OrderExams` `doe` ON (`doea`.`OrderExamId` = `doe`.`Id`)
	        LEFT JOIN `MssPlatformDb`.`dbo_Activities` `da` ON (`da`.`Id` = `doea`.`ActivityId`)
				WHERE doea.Id = NEW.Id
				AND `doe`.`ExamTypeId` = 1
				AND doe.AssignedExaminerId IS NOT NULL
				AND `da`.`Code` IN ('610', '611')
				GROUP BY doe.AssignedExaminerId
	   ) AS subquery ON subquery.AssignedExaminerId = dbo_MedicalExaminers.Id
	   SET dbo_MedicalExaminers.LastCompletedDate = DATE(subquery.CreateTime);
  
   END$$

DELIMITER ;



	ALTER TABLE MssPlatformDb.dbo_MedicalExaminers ADD INDEX idx_FirstName (FirstName);
	ALTER TABLE MssPlatformDb.dbo_MedicalExaminers ADD INDEX idx_LastName (LastName);
	ALTER TABLE MssPlatformDb.dbo_MedicalExaminers ADD INDEX idx_ProfRegNumber (ProfRegNumber);
	ALTER TABLE MssPlatformDb.dbo_MedicalExaminers ADD INDEX idx_PreferredEmail (PreferredEmail);
	ALTER TABLE MssPlatformDb.dbo_MedicalExaminers ADD INDEX idx_MobilePhone (MobilePhone);
	ALTER TABLE MssPlatformDb.dbo_MedicalExaminerServiceAreas_PostCodes ADD INDEX idx_MedicalExaminerServiceAreaId (MedicalExaminerServiceAreaId);

	CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridHealthPractitioners` AS
		SELECT
			`me`.`Id` AS `Id`,
			(CASE
				WHEN (NULLIF(`me`.`MiddleName`, '') IS NULL) THEN
					(CASE
						WHEN (`t`.`Id` = 2) THEN LTRIM(CONCAT(`me`.`FirstName`, ' ', `me`.`LastName`))
						ELSE LTRIM(CONCAT(IFNULL(`me`.`Title`, ''), ' ', `me`.`FirstName`, ' ', `me`.`LastName`))
					END)
				ELSE
					(CASE
						WHEN (`t`.`Id` = 2) THEN LTRIM(CONCAT(`me`.`FirstName`, ' ', `me`.`MiddleName`, ' ', `me`.`LastName`))
						ELSE LTRIM(CONCAT(IFNULL(`me`.`Title`, ''), ' ', `me`.`FirstName`, ' ', `me`.`MiddleName`, ' ', `me`.`LastName`))
					END)
			END) AS `Name`,
			`me`.`ProfRegNumber` AS `ProfRegNumber`,
			`me`.`PreferredEmail` AS `Email`,
			`me`.`MobilePhone` AS `PhoneNumber`,
			IF((`eas`.`Name` = 'Unknown'), '', `eas`.`Name`) AS `Availability`,
			`ea`.`AvailabilityNotes` AS `AvailabilityNotes`,
			(
				SELECT GROUP_CONCAT(`dc`.`Name` SEPARATOR ',')
				FROM `MssPlatformDb`.`dbo_Capabilities` `dc`
				LEFT JOIN `MssPlatformDb`.`dbo_FeeGroupExaminerCapabilities` `dfgec` ON (`dfgec`.`FeeGroupExaminerId` = `fge`.`Id`)
				WHERE (`dfgec`.`CapabilityId` = `dc`.`Id`)
					AND (`dc`.`CapabilityTypeId` = 1)
					AND (`t`.`Id` = 2)
			) AS `Qualifications`,
			(
				SELECT GROUP_CONCAT(`dpc`.`Code` SEPARATOR ',')
				FROM `MssPlatformDb`.`dbo_MedicalExaminerServiceAreas_PostCodes` `dmesapc`
				LEFT JOIN `MssPlatformDb`.`dbo_PostCodes` `dpc` ON (`dmesapc`.`PostCodeId` = `dpc`.`Id`)
				WHERE (`dmesapc`.`MedicalExaminerServiceAreaId` = `dmesa`.`Id`)
					AND (`t`.`Id` = 2)
			) AS `ServiceArea`,
			`ea`.`WeeklyCapacity` AS `WeeklyCapacity`,
			(
				SELECT COUNT(0)
				FROM `MssPlatformDb`.`dbo_OrderExams` `doe`
				WHERE (`me`.`Id` = `doe`.`AssignedExaminerId`)
					AND (`doe`.`ExamTypeId` = 1)
			) AS `CasesAssigned`,
			`dmesa`.`Radius` AS `Radius`,
			`dmesa`.`RadiusUnitId` AS `RadiusUnitId`,
			IF((`eas`.`Id` = 1), 1, 0) AS `Available`,
			`me`.`FollowUpped` AS `FollowUp`,
			`me`.`LastCompletedDate` AS `LastCompletedDate`,
			(
				SELECT MIN(`doea`.`Date`)
				FROM `MssPlatformDb`.`dbo_OrderExams` `doe`
				JOIN `MssPlatformDb`.`dbo_OrderExamAppointments` `doea` ON (`doea`.`Id` = `doe`.`Id`)
				WHERE (`me`.`Id` = `doe`.`AssignedExaminerId`)
					AND (`doe`.`ExamTypeId` = 1)
					AND (`doea`.`Date` IS NOT NULL)
			) AS `MinScheduledAppointmentTime`,
			(
				SELECT MAX(`doea`.`Date`)
				FROM `MssPlatformDb`.`dbo_OrderExams` `doe`
				JOIN `MssPlatformDb`.`dbo_OrderExamAppointments` `doea` ON (`doea`.`Id` = `doe`.`Id`)
				WHERE (`me`.`Id` = `doe`.`AssignedExaminerId`)
					AND (`doe`.`ExamTypeId` = 1)
					AND (`doea`.`Date` IS NOT NULL)
			) AS `MaxScheduledAppointmentTime`,
			`me`.`ApprovedDate` AS `ActiveDate`,
			`me`.`FirstAssignationDate` AS `FirstAssignationDate`,
			`me`.`DateOfBirth` AS `DateOfBirthday`,
			`me`.`FollowUpDate` AS `FollowUpDate`,
			`me`.`LicenceExpiry` AS `LicenceExpiry`,
			`ea`.`AwayFrom` AS `AwayFrom`,
			`ea`.`ReturnDate` AS `ReturnDate`,
			`e`.`CreateTime` AS `CreateTime`,
			`e`.`IsActive` AS `IsActive`
		FROM 
			`MssPlatformDb`.`dbo_MedicalExaminers` `me`
			LEFT JOIN `MssPlatformDb`.`dbo_Examiners` `e` ON (`e`.`Id` = `me`.`Id`)
			LEFT JOIN `MssPlatformDb`.`dbo_ExaminerAvailabilities` `ea` ON (`e`.`Id` = `ea`.`ExaminerId`)
			LEFT JOIN `MssPlatformDb`.`dbo_MedicalExaminerServiceAreas` `dmesa` ON (`me`.`Id` = `dmesa`.`Id`)
			LEFT JOIN `MssPlatformDb`.`dbo_ExaminerAvailabilityStatuses` `eas` ON (`eas`.`Id` = `ea`.`StatusId`)
			LEFT JOIN `MssPlatformDb`.`dbo_FeeGroupExaminers` `fge` ON (`fge`.`Id` = `me`.`Id`)
			LEFT JOIN `MssPlatformDb`.`dbo_ExaminerTypes` `t` ON (`t`.`Id` = `e`.`ExaminerTypeId`)
		WHERE (`t`.`Id` = 2) and (`fge`.`ExaminerStatusId` = 0);

COMMIT;