ALTER TABLE MssPlatformDb.dbo_Activities 
DROP COLUMN IsNADadjustable;

DELETE FROM MssPlatformDb.dbo_ApplicationSettings
WHERE `Key` = 'ExamsNADShowTime';