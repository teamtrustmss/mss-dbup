START TRANSACTION;

	ALTER TABLE dbo_OrderExams ADD PreferredContactTypeId int NOT NULL DEFAULT 0;
	ALTER TABLE dbo_OrderExams ADD PreferredContactTime datetime(6) NULL;
	ALTER TABLE dbo_OrderExams ADD MdgRef int NULL;
	
	ALTER TABLE dbo_OrderingCustomerDivisions ADD Password varchar(50) NULL;

	INSERT INTO dbo_ApplicationSettings (`Key`,ContainerName,Value,Comment) VALUES
		('MDG_eMedServiceUrl','Distribution.MSS.API','',NULL),
		('MDG_eMedServiceUrl','Presentation.MssPortal','',NULL);

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('4a0b9713-8ed1-ee11-9b16-489ebd28a7c6','11001','Cancelled on Tele-interview',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('8f603aa9-d053-4ea8-a34e-85c7e9aa667e','11002','On Hold on Tele-interview',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('795b94a8-56db-494a-836f-c311c01ac5da','11003','Active on Tele-interview',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('f4435101-1aca-45a4-8887-22ce85a18622','11004','Tele-interview case has been created in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('c5121ee2-e402-4963-ba76-df60d29e42a2','11005','Tele-interview case could not be created in MDG, moved to request queue',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('dccf030e-ce12-4cd2-95af-72f14ceb99e5','11006','Tele-interview case status has been changed in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('86444ed9-79d1-42ec-9a31-91653b84bc5a','11007','Tele-interview case status could not be changed in MDG, moved to request queue',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('427fc1bc-d16b-432f-b2c6-6630ce776d74','11008','Special instructions to Interviewer added',7,NULL,NULL,0,0,0,64490,3,0,NULL,0,0,1,1,'The following special instructions have been added to this exam order: [#Examiner Special Instruction#]',NOW(),NOW(),0);

COMMIT;