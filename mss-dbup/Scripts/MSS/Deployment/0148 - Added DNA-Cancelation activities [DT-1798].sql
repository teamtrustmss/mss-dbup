START TRANSACTION;
	INSERT INTO dbo_Activities
		(Id, Code, Description, ActivityTypeId, MaxDaysLimit, DefaultNumberOfDays, VisibleToExaminer, VisibleToOrderingCustomer, AllowMedicalExaminerUse, AllowedOrderStatuses, NextActionDateCalculationMode, SuspendedTimeService, NextActivityId, CommentRequired, CalculateNADFromScheduledDate, IsActive, NotificationType, SystemActivityComment, CreateTime, ModifyTime, AllowWebhookNotifications)
	VALUES
		('550e8400-e29b-41d4-a716-446655440000', '11020', 'CAN1 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440001', '11022', 'CAN2 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440002', '11024', 'CAN3 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440003', '11026', 'CAN4 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440004', '11028', 'DNA1 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440005', '11030', 'DNA2 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440006', '11032', 'DNA3 Product Code has been added to the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440007', '11021', 'CAN1 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440008', '11023', 'CAN2 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-446655440009', '11025', 'CAN3 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-44665544000A', '11027', 'CAN4 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-44665544000B', '11029', 'DNA1 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-44665544000C', '11031', 'DNA2 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0),
		('550e8400-e29b-41d4-a716-44665544000D', '11033', 'DNA3 Product Code has been removed from the Tele-interview case in MDG', 8, NULL, NULL, 0, 1, 0, 30795, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0);

COMMIT;