START TRANSACTION;

	CREATE INDEX grid_AllOrderExams_AssignedTo ON grid_AllOrderExams (AssignedTo);
	CREATE INDEX grid_AllOrderExams_AssignedToDESC ON grid_AllOrderExams (AssignedTo DESC);
	CREATE INDEX grid_AllOrderExams_AssignedCSR ON grid_AllOrderExams (AssignedCSR);
	CREATE INDEX grid_AllOrderExams_AssignedCSRDESC ON grid_AllOrderExams (AssignedCSR DESC);
	CREATE INDEX grid_AllOrderExams_Status ON grid_AllOrderExams (Status);
	CREATE INDEX grid_AllOrderExams_StatusDESC ON grid_AllOrderExams (Status DESC);
	CREATE INDEX grid_AllOrderExams_ExamType ON grid_AllOrderExams (ExamType);
	CREATE INDEX grid_AllOrderExams_ExamTypeDESC ON grid_AllOrderExams (ExamType DESC);
	CREATE INDEX grid_AllOrderExams_TimeService ON grid_AllOrderExams (TimeService);
	CREATE INDEX grid_AllOrderExams_TimeServiceDESC ON grid_AllOrderExams (TimeService DESC);
	CREATE INDEX grid_AllOrderExams_Applicant ON grid_AllOrderExams (Applicant);
	CREATE INDEX grid_AllOrderExams_ApplicantDESC ON grid_AllOrderExams (Applicant DESC);
	CREATE INDEX grid_AllOrderExams_OrderingCustomer ON grid_AllOrderExams (OrderingCustomer);
	CREATE INDEX grid_AllOrderExams_OrderingCustomerDESC ON grid_AllOrderExams (OrderingCustomer DESC);
	CREATE INDEX grid_AllOrderExams_OrderingCustomerId ON grid_AllOrderExams (OrderingCustomerId);
	CREATE INDEX grid_AllOrderExams_OrderingCustomerIdDESC ON grid_AllOrderExams (OrderingCustomerId DESC);
	CREATE INDEX grid_AllOrderExams_PolicyNumber ON grid_AllOrderExams (PolicyNumber);
	CREATE INDEX grid_AllOrderExams_PolicyNumberDESC ON grid_AllOrderExams (PolicyNumber DESC);
	CREATE INDEX grid_AllOrderExams_OrderExamNumber ON grid_AllOrderExams (OrderExamNumber);
	CREATE INDEX grid_AllOrderExams_OrderExamNumberDESC ON grid_AllOrderExams (OrderExamNumber DESC);
	CREATE INDEX grid_AllOrderExams_IsUrgentCase ON grid_AllOrderExams (IsUrgentCase);
	CREATE INDEX grid_AllOrderExams_HighNetWorth ON grid_AllOrderExams (HighNetWorth);
	CREATE INDEX grid_AllOrderExams_International ON grid_AllOrderExams (International);
	
COMMIT;