START TRANSACTION;

	ALTER TABLE dbo_OrderExamsReportsData ADD InactiveTimeService int DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_OrderExamsReportsDataNew ADD InactiveTimeService int DEFAULT 0 NOT NULL;
	ALTER TABLE grid_AllOrderExams ADD InactiveTimeService int DEFAULT 0 NOT NULL;
	INSERT INTO reporting_Fields (Id,Name,PropertyName,PropertyTypeId,CreateTime,ModifyTime,IsActive) VALUES
		('FE1AA194-EDCD-11EB-95CC-0242AC125555','Inactive Time Service','InactiveTimeService',5,'2022-10-19 10:00:00','2022-10-19 10:00:00',1);

#new function go get number of weekends and holidays for date range
	DROP FUNCTION IF EXISTS `dbo_GetWeekendsAndHolidaysForDateRange`;

DELIMITER $$

	CREATE FUNCTION `dbo_GetWeekendsAndHolidaysForDateRange`(
		first_date DATETIME, 
		second_date DATETIME
	) RETURNS INT
		LANGUAGE SQL
		DETERMINISTIC
	BEGIN
	
		DECLARE start_date DATE;
		DECLARE end_date DATE;
	
		IF (first_date < second_date) THEN
	    	SET start_date = first_date;
	    	SET end_date = second_date;
	  	ELSE
	    	SET start_date = second_date;
	    	SET end_date = first_date;
	  	END IF;
	
		RETURN DATEDIFF(end_date, start_date) + 1 - (5 * (DATEDIFF(end_date, start_date) DIV 7) + MID('1234555512344445123333451222234511112345001234550', 7 * WEEKDAY(start_date) + WEEKDAY(end_date) + 1, 1)
			- (SELECT COUNT(*) FROM dbo_PublicHolidays ph WHERE ph.Date >= start_date AND ph.Date <= end_date AND DAYNAME(ph.Date) != 'Sunday' AND DAYNAME(ph.Date) != 'Saturday'));
	
	END $$

DELIMITER ;




#new function to get number of days that OrderExam was in chosen status
	DROP FUNCTION IF EXISTS `dbo_GetInactiveTimeServiceForExamState`;

DELIMITER $$

	CREATE FUNCTION `dbo_GetInactiveTimeServiceForExamState`(
	 	_orderExamId VARCHAR(64),
	 	_examState INT
	) RETURNS INT
	    READS SQL DATA
	BEGIN
		DECLARE _currentInactiveDays INT DEFAULT 0;
		DECLARE _inactiveTimeService INT DEFAULT 0;
	    DECLARE _lastUsedActivityOrder INT DEFAULT 0;
	    DECLARE _finishedCursor INT DEFAULT 0;
	    DECLARE _order INT;
	    DECLARE _fromCreateTime DATETIME(6);
	    DECLARE _toCreateTime DATETIME(6);
	 	DECLARE _firstDate DATETIME(6);
	 	
	    BEGIN
			DECLARE db_cursor CURSOR FOR 
				SELECT E.CreateTime, E.ActivityOrder
					FROM dbo_OrderExamActivities E
				  	WHERE OrderExamId = _orderExamId
						AND E.CreateTime >= _firstDate
				  		AND E.ToExamState = _examState
				  		AND E.FromExamState <> _examState
				  		AND IsChangeStatusReason = 0
				  	ORDER BY E.ActivityOrder;
				 
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET _finishedCursor = 1;
		    
		    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;
		 	SELECT A.CreateTime INTO _firstDate
		 	  FROM (
		 	  	SELECT E.CreateTime, E.Id FROM dbo_OrderExamActivities E 
		 	  		INNER JOIN dbo_Activities A ON A.Id = E.ActivityId 
		      			WHERE E.OrderExamId = _orderExamId
		      		ORDER BY E.CreateTime
		      		LIMIT 1
		      	) A;
		    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;
		  
		    OPEN db_cursor;
		    cursorLoop: LOOP
		 		FETCH db_cursor INTO _toCreateTime, _order;
		 	
		        IF (_finishedCursor = 1) THEN 
					LEAVE cursorloop;
				END IF;
		 	
				SET _fromCreateTime = NULL;
				SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;
				SELECT E.CreateTime, E.ActivityOrder INTO _fromCreateTime, _lastUsedActivityOrder
					FROM dbo_OrderExamActivities E 
				  		WHERE OrderExamId = _orderExamId
				  			AND E.ActivityOrder > _order
				  			AND E.FromExamState = _examState
				  			AND E.ToExamState <> _examState
				  			AND E.ActivityOrder > _lastUsedActivityOrder
				  			AND IsChangeStatusReason = 0
				  	ORDER BY E.ActivityOrder
				  	LIMIT 1;
				SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;
				
			    IF (_fromCreateTime IS NULL) THEN
			    	SET _fromCreateTime = NOW();
			    	SET _finishedCursor = 1;
			    END IF;
			   
				SET _currentInactiveDays = DATEDIFF(_fromCreateTime, _toCreateTime)
					- dbo_GetWeekendsAndHolidaysForDateRange(_toCreateTime, _fromCreateTime);
			
				IF(_currentInactiveDays < 0) THEN
					SET _currentInactiveDays = 0;
				END	IF;
			
				SET _inactiveTimeService = _inactiveTimeService + _currentInactiveDays;
			END LOOP cursorLoop;
	 		CLOSE db_cursor;
	 	END;
	 
	 	IF(_inactiveTimeService < 0) THEN
	 		SET _inactiveTimeService = 0;
	 	END IF;
	 
	 	RETURN _inactiveTimeService;
	END $$

DELIMITER ;



#new function to get number of days that OrderExam was inactive
	DROP FUNCTION IF EXISTS `dbo_GetInactiveTimeServiceForExam`;

DELIMITER $$

	CREATE FUNCTION `dbo_GetInactiveTimeServiceForExam`(
	 	_orderExamId VARCHAR(64)
	) RETURNS INT
	    READS SQL DATA
	BEGIN
		RETURN dbo_GetInactiveTimeServiceForExamState(_orderExamId, 256)
			+ dbo_GetInactiveTimeServiceForExamState(_orderExamId, 32768);
	END $$

DELIMITER ;




#InactiveTimeService column logic adding to afterInsert_OrderExam trigger
	DROP TRIGGER IF EXISTS `afterInsert_OrderExam`;

DELIMITER $$

	CREATE TRIGGER `afterInsert_OrderExam` AFTER INSERT ON `dbo_OrderExams` FOR EACH ROW
		INSERT INTO grid_AllOrderExams (Id,OrderId,IsActive,OrderExamNumber,ExamType, NextActionDate,NextActionDateSort,Origin,TypeOfCover,OrderingCustomer,ExamPostCode,ApplicantFirstName,ApplicantLastName,
		   ApplicantDoB,PolicyNumber,ClientReference2,IsUrgentCase,IsTouched,OrderDate,`Status`,AppointmentScheduled,ExaminerFirstName,ExaminerLastName,ExamOwnerId,CsrFirstName,CsrLastName,
		   GpName,ClinicName,IsAnonymised,HighNetWorth,International,HasLinkedOrders, AssignedToPrefix,AssignedTo,Applicant,AssignedCSR,LatestActivityId,TimeService,InactiveTimeService,OrderingCustomerTeamMemberId,
		   OrderingCustomerTeamId,ApplicantPostCode,CreatorName)  SELECT 
		    orderExam.Id                                        AS Id,
		    ocOrder.Id                                          AS OrderId,
		    orderExam.IsActive                                  AS IsActive,
		    orderExam.ExamCustomId                              AS OrderExamNumber,
		    orderExam.ExamTypeId                                AS ExamType,
		    (SELECT dbo_GetExamNextActionDate(orderExam.Id))    AS NextActionDate,
		    IFNULL((SELECT dbo_GetExamNextActionDate(orderExam.Id)), '9999-12-31')            AS NextActionDateSort,
		    ocOrder.OrderingChannelId                           AS Origin,
		    ocOrder.CoverTypeId                                 AS TypeOfCover,
		    oc.CompanyName                                      AS OrderingCustomer,
		    appointment.PostCode                                AS ExamPostCode,
		    applicant.FirstName                                 AS ApplicantFirstName,
		    applicant.LastName                                  AS ApplicantLastName,
		    applicant.DateOfBirth                               AS ApplicantDoB,
		    ocOrder.PolicyNumber                                AS PolicyNumber,
		    ocOrder.ClientReference2                            AS ClientReference2,
		    orderExam.IsUrgent                                  AS IsUrgentCase,
		    
		    
		    0                                                   AS IsTouched,
		    orderExam.CreateTime                                AS OrderDate,
		    orderExam.StatusId                                  AS `Status`,
		    appointment.`Date`                                  AS AppointmentScheduled,
		    medicalExaminer.FirstName                           AS ExaminerFirstName,
		    medicalExaminer.LastName                            AS ExaminerLastName,
		    examOwner.Id                                        AS ExamOwnerId, 
		    examOwner.FirstName                                 AS CsrFirstName,
		    examOwner.LastName                                  AS CsrLastName,
		    gpPractice.DoctorName                               AS GpName,
		    clinic.`Name`                                       AS ClinicName,
		    ocOrder.IsAnonymised                                AS IsAnonymised,
		    
		    (SELECT IFNULL((SELECT 1 
		            FROM dbo_OrderExamRequirements R  
		            WHERE orderExam.Id = R.OrderExamId 
		            AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
		            LIMIT 1), 0))                               AS HighNetWorth,
		    (SELECT IFNULL((SELECT 1 
		        FROM dbo_OrderExamRequirements R  
		        WHERE orderExam.Id = R.OrderExamId 
		        AND R.`Code` = 'INT FEE' 
		        LIMIT 1), 0) )                                  AS International,
		    (SELECT IFNULL((SELECT 1 
		            FROM dbo_LinkedOrders LO 
		            WHERE LO.OrderId = orderExam.OrderId 
		            OR LO.ParentOrderId = orderExam.OrderId
		            LIMIT 1), 0))                               AS HasLinkedOrders,
		    (SELECT AssignedToPrefix 
		        FROM dbo_OrderExamActivities OEA 
		        WHERE OEA.OrderExamId = orderExam.Id 
		        ORDER BY ActivityOrder DESC LIMIT 1)               AS AssignedToPrefix,
		    (SELECT (CASE WHEN AssignedToPrefix IS NULL 
		            THEN AssignedTo 
		            ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo) 
		            END) 
		        FROM dbo_OrderExamActivities OEA 
		        WHERE OEA.OrderExamId = orderExam.Id 
		        ORDER BY ActivityOrder DESC LIMIT 1)               AS AssignedTo,
		    CONCAT(applicant.FirstName, ' ', applicant.LastName)AS Applicant,
		    CONCAT(examOwner.FirstName, ' ', examOwner.LastName)AS AssignedCSR,
		    (SELECT Id 
		        FROM dbo_OrderExamActivities E 
		        WHERE E.OrderExamId=orderExam.Id 
		        ORDER BY E.ActivityOrder DESC 
		        LIMIT 1)                                        AS LatestActivityId,
		    IFNULL((SELECT dbo_GetTimeServiceForActivity(LatestActivityId, NULL)),0) AS TimeService,
		    IFNULL((SELECT dbo_GetInactiveTimeServiceForExam(orderExam.Id)),0) AS InactiveTimeService,
		    ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,                            
		    IFNULL(ocOrder.OrderingCustomerTeamId, '00000000-0000-0000-0000-000000000000') AS OrderingCustomerTeamId,
		    (SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
		        FROM dbo_Applicants a 
		        LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
		        WHERE applicant.Id = a.Id
		        GROUP BY a.Id, aa.ApplicantId
		    )                                                    AS ApplicantPostCode,
		    (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
		        FROM dbo_OrderExamActivities OEA 
		        INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
		        WHERE OEA.PreviousActivityId IS NULL 
		            AND OEA.OrderExamId= orderExam.Id
		        ORDER BY OEA.ActivityOrder ASC
		        LIMIT 1
		    )                                        AS CreatorName
		 FROM dbo_OrderExams orderExam
		    INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId 
		    INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId 
		    INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id
		    LEFT JOIN dbo_OrderExamAppointments appointment ON appointment.Id = orderExam.Id
		    LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId
		    LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId
		    LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId
		    LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId
		    WHERE orderExam.Id = NEW.Id $$

DELIMITER ;




#InactiveTimeService column logic adding to afterUpdate_OrderExams trigger
		DROP TRIGGER IF EXISTS `afterUpdate_OrderExams`;
	
DELIMITER $$
	CREATE TRIGGER `afterUpdate_OrderExams` AFTER UPDATE ON `dbo_OrderExams` FOR EACH ROW BEGIN
	
	    UPDATE grid_AllOrderExams gAOE 
	
	    LEFT JOIN dbo_Users examOwner ON examOwner.Id = NEW.ExamOwnerId
	
	    LEFT JOIN dbo_MedicalExaminers examiner ON examiner.Id = NEW.AssignedExaminerId
	
	    SET 
	
	        gAOE.IsActive = NEW.IsActive,
	
	        gAOE.ExamType = NEW.ExamTypeId,
	
	        gAOE.IsUrgentCase = NEW.IsUrgent,
	
	        gAOE.`Status` = NEW.StatusId,
	
	        gAOE.ExamOwnerId = NEW.ExamOwnerId,
	
	        gAOE.CsrFirstName = examOwner.FirstName,
	
	        gAOE.CsrLastName = examOwner.LastName,
	
	        gAOE.AssignedCSR = CONCAT(examOwner.FirstName, ' ', examOwner.LastName),
	
	        gAOE.HighNetWorth = (SELECT IFNULL((SELECT 1 
	
	            FROM dbo_OrderExamRequirements R  
	
	            WHERE NEW.Id = R.OrderExamId 
	
	            AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
	
	            LIMIT 1), 0)),
	
	        gAOE.International = (SELECT IFNULL((SELECT 1 
	
	            FROM dbo_OrderExamRequirements R  
	
	            WHERE NEW.Id = R.OrderExamId 
	
	            AND R.`Code` = 'INT FEE' 
	
	            LIMIT 1), 0) ),
	
	        gAOE.HasLinkedOrders = (SELECT IFNULL((SELECT 1 
	
	            FROM dbo_LinkedOrders LO 
	
	            WHERE LO.OrderId = NEW.OrderId 
	
	            OR LO.ParentOrderId = NEW.OrderId
	
	            LIMIT 1), 0)),
	
	        gAOE.CreatorName = (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
	
	            FROM dbo_OrderExamActivities OEA 
	
	            INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
	
	            WHERE OEA.PreviousActivityId IS NULL 
	
	                AND OEA.OrderExamId= NEW.Id
	
	            ORDER BY OEA.ActivityOrder ASC
	
	            LIMIT 1),
	
	        gAOE.ExaminerFirstName = examiner.FirstName,
	
	        gAOE.ExaminerLastName = examiner.LastName,
	        
	        gAOE.InactiveTimeService = 
	        (
		        SELECT IF(NEW.StatusId = 32 OR NEW.StatusId = 64 OR NEW.StatusId = 16384,
		        	gAOE.InactiveTimeService,
		        	IFNULL(dbo_GetInactiveTimeServiceForExam (gAOE.Id), gAOE.InactiveTimeService))
	        )
	
	    WHERE gAOE.Id = NEW.Id;        
	
	END $$

DELIMITER ;




#InactiveTimeService column logic adding to dbo_UpdateOrderExamsReportsData procedure
	DROP PROCEDURE IF EXISTS `dbo_UpdateOrderExamsReportsData`;

DELIMITER $$

	CREATE PROCEDURE `dbo_UpdateOrderExamsReportsData`()
    MODIFIES SQL DATA
	BEGIN
		 DECLARE `should_rollback` BOOL DEFAULT FALSE;
		 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `should_rollback` = TRUE;
			 SET autocommit = 0;
			 
			 START TRANSACTION;
	 
			 DELETE FROM dbo_OrderExamsReportsData;
			 drop table if exists orderExamTempData;
			 SET @@group_concat_max_len = 256000;
			 create temporary table orderExamTempData select orderExam.`Id` as orderExamId,
					 (SELECT GROUP_CONCAT(NULLIF(b.`Code`,'') ORDER BY E.CreateTime ASC, b.Code ASC separator ', ')
					                     FROM dbo_OrderExamActivities E
					                     INNER JOIN dbo_Activities b ON E.ActivityId = b.Id
					                     WHERE E.OrderExamId = orderExam.Id
					                     GROUP BY E.OrderExamId)                         AS ActivityDescriptionsAll,
					 (SELECT ae.EmailAddress FROM dbo_ApplicantEmails ae
										 INNER JOIN dbo_Applicants a ON ae.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id
										 ORDER BY ae.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantEmail,
					 (SELECT ap.`Number` FROM dbo_ApplicantPhones ap
										 INNER JOIN dbo_Applicants a ON ap.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id AND ap.PhoneType = 3
										 ORDER BY ap.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantMobileNumber,
					 (SELECT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
						 FROM dbo_Applicants a 
						 LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
						 WHERE applicant.Id = a.Id 
							 AND aa.IsPreferred = 1
						 GROUP BY a.Id)                                  AS ApplicantPostCode,
						 ocOrder.Id                                          AS OrderId,
						 ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,    
						 ocOrder.OrderingCustomerId,
						 ocOrder.OrderingCustomerTeamId,
						 ocOrder.OrderingChannelId                           AS Origin,
						 ocOrder.CoverTypeId                                 AS TypeOfCover,
						 ocOrder.PolicyNumber                                AS PolicyNumber,				
						 (SELECT CASE WHEN NULLIF(AssignedToPrefix, '') IS NULL 
								 THEN AssignedTo 
								 ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo)
								 END
						 from dbo_OrderExamActivities OEA 
						 where OEA.OrderExamId = orderExam.Id 
						 order by CreateTime desc, ActivityOrder desc
						 LIMIT 1)                                            AS AssignedTo,
					 (SELECT CASE
						 WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 THEN 
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 WHEN examiner.ExaminerTypeId = 4 THEN
								 (SELECT GROUP_CONCAT( DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 ELSE '' 
					 END)                                                AS Skill,
					 (SELECT CASE
						 WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 
							 THEN 
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 WHEN examiner.ExaminerTypeId = 4 
							 THEN
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 ELSE '' 
					 END)                                                AS Qualification,
					 oc.CompanyName                                      AS OrderingCustomer,
					 oc.LastChargeReview                            		AS LastChargeReview,
					 CONCAT(applicant.FirstName, ' ', applicant.LastName) AS ApplicantName,
					 applicant.DateOfBirth                               AS ApplicantDoB,
					 (SELECT a.`Name` FROM dbo_Genders a 
						 WHERE a.Id = applicant.GenderId)                AS ApplicantGender,
					 applicant.LastName                                  AS ApplicantLastName,
					 applicant.FirstName                                 AS ApplicantFirstName,
					 examType.`Name`                                     AS ExamType,
                     IFNULL(dbo_GetTimeServiceForActivity ((SELECT Id
                         FROM dbo_OrderExamActivities E
							WHERE E.OrderExamId = orderExam.`Id`
                         ORDER BY E.CreateTime DESC
                         LIMIT 1), NULL), 0)                             AS TimeService,
					 0 													 AS TotalTimeService,
					 (SELECT IF(orderExam.`StatusId` = 32 OR orderExam.`StatusId` = 64 OR orderExam.`StatusId` = 16384,
						 IFNULL((SELECT InactiveTimeService 
								 FROM grid_AllOrderExams gaoe
								 WHERE gaoe.Id = orderExam.`Id`), 0),
						 IFNULL(dbo_GetInactiveTimeServiceForExam (orderExam.`Id`), 0))
				     ) 													 AS InactiveTimeService,
					 li.`Name`                                           AS LifeIndicator
					 from dbo_OrderExams orderExam
					 INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId
					 INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId
					 INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id
					 INNER JOIN dbo_OcExamTypes examType ON orderExam.ExamTypeId = examType.Id
					 LEFT JOIN dbo_Examiners examiner ON orderExam.AssignedExaminerId = examiner.Id
					 LEFT JOIN dbo_LifeIndicators li ON li.`Id` = ocOrder.`LifeIndicatorId`
					 WHERE ocOrder.IsAnonymised = 0;
			 
			 SET @@group_concat_max_len = 256000;
			 INSERT INTO dbo_OrderExamsReportsData 
			 (
				 ActivityDescriptionsAll,AddressLine1,AddressLine2,AddressLine3,ApplicantDoB,ApplicantEmail,ApplicantFirstName,
				 ApplicantGender,ApplicantLastName,ApplicantMobileNumber,ApplicantName,ApplicantPostCode,AppointmentScheduled, 
				 AssignedCSR,AssignedTo,AssignedToPrefix,BestContact,BilledDate,City,ClinicName,CreatorName,
				 CsrFirstName,CsrLastName,ExaminerFirstName,ExaminerLastName,
				 ExaminerPrepaymentFlag,ExamOwnerId,ExamPostCode,ExamType,GpName,GpEmail,GPPracticePostCode,GroupPractice,GpPaymentType,GpAccountNo, GpSortCode,PayeeEmailAddress, HighNetWorth,Id,
				 InvoiceReferenceNumber,IsActive,IsUrgentCase,LatestActivityComment,
				 LatestActivityDescription,MiscFeeDescription,MiscChargeDescription,OcContact,OcServiceDescription,
				 OrderDate,OrderExamNumber,OrderId,OrderingCustomer, LastChargeReview, OrderingCustomerTeam,OrderingCustomerTeamMemberId,Origin,PayeeName,PolicyNumber,PracticeName,Qualification,Requirement,
				 RequirementProblemReason,RequirementStatus,Skill,`Status`,TypeOfCover,CreateTime,ExamLocation, ExaminerEmail,GPPracticeFax,TimeService,TotalTimeService,InactiveTimeService,
				 LabFeeTotal,MeFeeTotal,MiscFeeTotal,MiscCharge, OcCharge, LifeIndicator, BankName
			 )
			 SELECT
			         temp.ActivityDescriptionsAll,
					 generalPractitioner.AddressLine1                    AS AddressLine1,
					 generalPractitioner.AddressLine2                    AS AddressLine2,
					 generalPractitioner.AddressLine3                    AS AddressLine3,    
					 temp.ApplicantDoB,  
					 temp.ApplicantEmail,
					 temp.ApplicantFirstName,
					 temp.ApplicantGender,
					 temp.ApplicantLastName,
					 temp.ApplicantMobileNumber,
					 temp.ApplicantName,
					 temp.ApplicantPostCode,
					 appointment.`Date`                                  AS AppointmentScheduled,
					 CONCAT(examOwner.FirstName, ' ', examOwner.LastName) AS AssignedCSR,
					 temp.AssignedTo,
					 (SELECT AssignedToPrefix 
						 from dbo_OrderExamActivities OEA 
						 where OEA.OrderExamId = orderExam.Id 
						 order by CreateTime desc, ActivityOrder desc
						 LIMIT 1)                                        AS AssignedToPrefix,
					 generalPractitioner.BestContact                     AS BestContact,
					 (SELECT a.CreateTime FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
						 WHERE a.OrderExamId = orderExam.Id AND a.ToExamState = 16
						 ORDER BY a.CreateTime
						 LIMIT 1)                                        AS BilledDate,
					 generalPractitioner.Town                            AS City,
					 clinic.`Name`                                       AS ClinicName,
					 (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
						 FROM dbo_OrderExamActivities OEA 
						 INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
						 WHERE OEA.PreviousActivityId IS null AND OEA.OrderExamId= orderExam.Id
						 ORDER BY OEA.CreateTime ASC 
						 LIMIT 1)                                        AS CreatorName,
					 examOwner.FirstName                                 AS CsrFirstName,
					 examOwner.LastName                                  AS CsrLastName,
					 medicalExaminer.FirstName                           AS ExaminerFirstName,
					 medicalExaminer.LastName                            AS ExaminerLastName,
					 (SELECT CASE (SELECT COUNT(*) FROM dbo_OrderExamActivities a
										 INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
										 INNER JOIN dbo_OrderExams c ON a.OrderExamId = c.Id
										 WHERE c.Id = orderExam.Id AND c.ExamTypeId = 4 
											 AND b.`Code` IN('300'))
										 WHEN 0 
										 THEN 'No' 
										 ELSE 'Yes' 
										 END)                            AS ExaminerPrepaymentFlag,
					 examOwner.Id                                        AS ExamOwnerId,
					 appointment.PostCode                                AS ExamPostCode,
					 temp.ExamType,
					 TRIM(CONCAT(
					 generalPractitioner.Title, 
					 ' ', 
					 generalPractitioner.FirstName, 
					 ' ', 
					 generalPractitioner.LastName))                      AS GpName,
					 generalPractitioner.Email                           AS GpEmail,
					 generalPractitioner.PostCode                        AS GPPracticePostCode,
					 gpPracticeName.SurgeryName                          AS GroupPractice,                 
					 (CASE
							WHEN generalPractitioner.PaymentType  = 1 THEN "Bacs"
							WHEN generalPractitioner.PaymentType = 2 THEN "Cheque"
							ELSE "-"
						END)  AS GpPaymentType,
					 generalPractitioner.AccountNo                       AS GpAccountNo,
					 generalPractitioner.SortCode                        AS GpSortCode,
					 generalPractitioner.PayeeEmailAddress				 AS PayeeEmailAddress,
					 (Select IFNULL(
						 (SELECT 1 From dbo_OrderExamRequirements R  
						 WHERE orderExam.Id = R.OrderExamId 
						 AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
						 AND R.IsDeactivated = 0 LIMIT 1), 0) )          AS HighNetWorth,
					 orderExam.Id                                        AS Id,
					 generalPractitioner.InvoiceReferenceNumber          AS InvoiceReferenceNumber,
					 orderExam.IsActive                                  AS IsActive,
					 orderExam.IsUrgent                                  AS IsUrgentCase,
					 (SELECT E.Note 
						 FROM dbo_OrderExamActivities E 
						 WHERE E.OrderExamId=orderExam.Id 
						 ORDER BY E.CreateTime DESC, E.ActivityOrder desc
						 LIMIT 1)                                        AS LatestActivityComment,
					 (SELECT b.`Description` 
						 FROM dbo_OrderExamActivities E 
						 INNER JOIN dbo_Activities b ON E.ActivityId = b.Id 
						 WHERE E.OrderExamId=orderExam.Id 
						 ORDER BY E.CreateTime DESC, E.ActivityOrder desc
						 LIMIT 1)                                        AS LatestActivityDescription,
					 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
						 FROM dbo_MiscellaneousCharges a
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 2
						 ORDER BY a.Description
					 )                                                   AS MiscFeeDescription,
					 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
						 FROM dbo_MiscellaneousCharges a
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 1
						 ORDER BY a.Description
					 )                         							 AS MiscChargeDescription,
					 IFNULL(ocTeamMember.`Name`, '')                     AS OcContact,
					 (SELECT GROUP_CONCAT(DISTINCT E.`Description` ORDER BY REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(E.Description,'-',''), '(', ''), ')', ''), '/', ''), '''', '')  ASC separator ', ' )
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.StatusId NOT IN(3,4,5) 
							 AND E.IsDeactivated = 0
				     )                            						 AS OcServiceDescription,
					 orderExam.CreateTime                                AS OrderDate,
					 orderExam.ExamCustomId                              AS OrderExamNumber,
					 temp.OrderId,
					 temp.OrderingCustomer,
					 LastChargeReview,
					 team.`Name`                                         AS OrderingCustomerTeam,
					 temp.OrderingCustomerTeamMemberId,                           
					 temp.Origin,
					 generalPractitioner.PayeeName                       AS PayeeName,
					 temp.PolicyNumber,
					 generalPractitioner.PracticeName                    AS PracticeName,
					 temp.Qualification,
					 (SELECT GROUP_CONCAT(DISTINCT E.`Code` SEPARATOR ', ')
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.IsDeactivated = 0
						 GROUP BY E.OrderExamId)                         AS Requirement,
					 NULL                                        AS RequirementProblemReason,
					 (SELECT GROUP_CONCAT(DISTINCT E.StatusId SEPARATOR ', ')
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.IsDeactivated = 0
						 GROUP BY E.OrderExamId)                         AS RequirementStatus,
					 temp.Skill,
					 (SELECT a.`Name` FROM dbo_OrderExamStatusTypes a 
						 WHERE a.Id = orderExam.StatusId)                AS `Status`,
					 temp.TypeOfCover,
					 NOW()                                               AS CreateTime,
					 TRIM(CONCAT(
						 IFNULL(appointment.AddressLine1, ''),
						 IFNULL(CONCAT(appointment.AddressLine2, ', '), ''),
						 IFNULL(CONCAT(appointment.AddressLine3, ', '), ''),
						 IFNULL(CONCAT(appointment.City, ', '), ''),
						 IFNULL(CONCAT(appointment.PostCode, ', '), ''),
						 IFNULL(CONCAT(appointment.Country, ', '), '')           
					 ))                                                  AS ExamLocation,
					 medicalExaminer.PreferredEmail                      AS ExaminerEmail,
					 generalPractitioner.Fax                             AS GPPracticeFax,
					 temp.TimeService,
					 temp.TotalTimeService,
					 temp.InactiveTimeService,
					 0 AS LabFeeTotal,
					 0 AS MeFeeTotal,
					 0 AS MiscFeeTotal,
					 0 AS MiscCharge,
					 0 AS OcCharge,
					 temp.LifeIndicator,
					 dbo_Bank.Name as BankName 
				 FROM dbo_OrderExams orderExam
					 inner join orderExamTempData temp on temp.orderExamId = orderExam.Id
					 LEFT JOIN dbo_OrderingCustomerTeams team ON temp.OrderingCustomerTeamId = team.Id
					 LEFT JOIN dbo_OrderExamAppointments appointment ON orderExam.Id = appointment.Id
					 LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_GeneralPractitioners generalPractitioner ON generalPractitioner.Id = orderExam.Id
					 LEFT JOIN dbo_GpPractices gpPracticeName ON gpPracticeName.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId
					 LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId
					 LEFT JOIN dbo_ExaminerBankAccount EBA ON orderExam.AssignedExaminerId = EBA.ExaminerId 
					 LEFT JOIN dbo_Bank ON EBA.BankId = dbo_Bank.Id 
					 LEFT JOIN dbo_GridOrderingCustomerTeamMembers ocTeamMember ON temp.OrderingCustomerTeamMemberId = ocTeamMember.Id;

				 UPDATE dbo_OrderExamsReportsData
                      SET TotalTimeService = TimeService;			 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT  OrderExamId, SUM(IFNULL(A.ExaminerFee, 0)) SUMExaminerFee
					 FROM (
						 SELECT a.Id, a.OrderExamId, a.ExaminerFee
							 FROM dbo_OrderExamRequirements a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 LEFT JOIN dbo_Examiners c ON b.AssignedExaminerId = c.Id AND c.ExaminerTypeId != 5
						 INNER JOIN dbo_AllowedExaminers ae ON a.MedicalExaminationId = ae.MedicalExaminationId 
						 AND (SELECT COUNT(*)    FROM dbo_AllowedExaminers aa WHERE aa.ExaminerType = 5 AND aa.MedicalExaminationId = a.MedicalExaminationId AND a.OrderExamId=BB.id) = 0    
						 WHERE a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType != 5
								 AND a.OrderExamId=BB.id
						 GROUP BY a.Id, a.OrderExamId, a.ExaminerFee
					 ) A
					 GROUP BY OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MeFeeTotal = CC.SUMExaminerFee;
				 
				 
				 UPDATE dbo_OrderExamsReportsData
				 JOIN (
					 SELECT  OrderExamId, SUM(IFNULL(SS.ExaminerFee,0)) SUMExaminerFee
					 FROM (
						 SELECT DISTINCT a.Id, a.OrderExamId, a.ExaminerFee
							 FROM dbo_OrderExamRequirements a 
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_MedicalExaminations m ON a.MedicalExaminationId = m.Id 
						 INNER JOIN dbo_AllowedExaminers ae ON m.Id = ae.MedicalExaminationId
						 INNER JOIN dbo_ExaminationFees f ON f.MedicalExaminationId = m.Id
						 INNER JOIN dbo_LaboratoryExaminations e ON f.Id = e.Id
						 WHERE a.OrderExamId = BB.Id AND a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType = 5
						 ) SS
						 GROUP BY SS.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
					 SET LabFeeTotal=CC.SUMExaminerFee;
	 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
						 SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
							 FROM dbo_MiscellaneousCharges a 
							 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
							 INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
							 WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 2
							 GROUP BY a.OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MiscFeeTotal=CC.SUMCharge;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
						 SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
							 FROM dbo_MiscellaneousCharges a 
							 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
							 INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
							 WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 1
							 GROUP BY a.OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MiscCharge = CC.SUMCharge;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT SUM(IFNULL(a.OcCharge,0))  SUMOcCharge, a.OrderExamId 
						 FROM dbo_OrderExamRequirements a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId = BB.Id AND a.StatusId != 5 AND a.IsDeactivated = 0
						 GROUP BY a.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET OcCharge = CC.SUMOcCharge;
				 
				 
				 WITH cte AS ( SELECT NextActionDate, OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) AS rn FROM dbo_OrderExamActivities)
				 UPDATE dbo_OrderExamsReportsData oerd
				 JOIN cte ON cte.rn = 1 AND cte.OrderExamId = oerd.Id
				 SET oerd.NextActionDate = cte.NextActionDate;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT MIN(a.CreateTime) CreateTime, a.OrderExamId 
						 FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 WHERE a.OrderExamId = BB.Id AND a.ToExamState IN(32,64,16384)
						 GROUP BY a.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET CompletedDate = DATE_FORMAT(CC.CreateTime, "%Y-%m-%d %H:%i:%s");
				 
				 
				 WITH cte AS 
					 (SELECT CONCAT(b.Description, CASE WHEN a.Note IS NULL THEN '' ELSE CONCAT(': ', a.Note) END) topNote, 
					 a.OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) 
					 AS rn 
					 FROM dbo_OrderExamActivities a 
					 INNER JOIN dbo_Activities b  ON a.ActivityId = b.Id 
					 WHERE a.ToExamState = 32)
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN cte ON cte.rn=1 AND cte.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET CancellationReason = cte.topNote;
			 
			 IF `should_rollback` THEN
				 ROLLBACK;
			 ELSE
				 COMMIT;
			 END IF;
			 SET autocommit = 1;
		END $$

DELIMITER ;

CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `dbo_GridAllOrderExamsReports` AS
select
    `ActivityCommentsAll` AS `ActivityCommentsAll`,
    `ActivityDescriptionsAll` AS `ActivityDescriptionsAll`,
    `AddressLine1` AS `AddressLine1`,
    `AddressLine2` AS `AddressLine2`,
    `AddressLine3` AS `AddressLine3`,
    `ApplicantDoB` AS `ApplicantDoB`,
    `ApplicantEmail` AS `ApplicantEmail`,
    `ApplicantFirstName` AS `ApplicantFirstName`,
    `ApplicantGender` AS `ApplicantGender`,
    `ApplicantLastName` AS `ApplicantLastName`,
    `ApplicantMobileNumber` AS `ApplicantMobileNumber`,
    `ApplicantName` AS `ApplicantName`,
    `ApplicantPostCode` AS `ApplicantPostCode`,
    `AppointmentScheduled` AS `AppointmentScheduled`,
    `AssignedCSR` AS `AssignedCSR`,
    `AssignedTo` AS `AssignedTo`,
    `AssignedToPrefix` AS `AssignedToPrefix`,
    `BestContact` AS `BestContact`,
    `BilledDate` AS `BilledDate`,
    `CancellationReason` AS `CancellationReason`,
    `City` AS `City`,
    `ClinicName` AS `ClinicName`,
    `CompletedDate` AS `CompletedDate`,
    `CreatorName` AS `CreatorName`,
    `CsrFirstName` AS `CsrFirstName`,
    `CsrLastName` AS `CsrLastName`,
    `ExaminerFirstName` AS `ExaminerFirstName`,
    `ExaminerLastName` AS `ExaminerLastName`,
    `ExaminerPrepaymentFlag` AS `ExaminerPrepaymentFlag`,
    `ExamOwnerId` AS `ExamOwnerId`,
    `ExamPostCode` AS `ExamPostCode`,
    `ExamType` AS `ExamType`,
    `GpName` AS `GpName`,
    `GPPracticePostCode` AS `GPPracticePostCode`,
    `GroupPractice` AS `GroupPractice`,
    `HighNetWorth` AS `HighNetWorth`,
    `Id` AS `Id`,
    `InvoiceReferenceNumber` AS `InvoiceReferenceNumber`,
    `IsActive` AS `IsActive`,
    `IsUrgentCase` AS `IsUrgentCase`,
    `LabFeeTotal` AS `LabFeeTotal`,
    `LatestActivityComment` AS `LatestActivityComment`,
    `LatestActivityDescription` AS `LatestActivityDescription`,
    `MeFeeTotal` AS `MeFeeTotal`,
    `MiscFeeDescription` AS `MiscFeeDescription`,
    `MiscFeeTotal` AS `MiscFeeTotal`,
    `MiscCharge` AS `MiscCharge`,
    `MiscChargeDescription` AS `MiscChargeDescription`,
    `NextActionDate` AS `NextActionDate`,
    `OcCharge` AS `OcCharge`,
    `OcContact` AS `OcContact`,
    `OcServiceDescription` AS `OcServiceDescription`,
    `OrderDate` AS `OrderDate`,
    `OrderExamNumber` AS `OrderExamNumber`,
    `OrderId` AS `OrderId`,
    `OrderingCustomer` AS `OrderingCustomer`,
    `OrderingCustomerTeam` AS `OrderingCustomerTeam`,
    `OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
    `Origin` AS `Origin`,
    `PayeeName` AS `PayeeName`,
    `PolicyNumber` AS `PolicyNumber`,
    `ClientReference2` AS `ClientReference2`,
    `PracticeName` AS `PracticeName`,
    `Qualification` AS `Qualification`,
    `Requirement` AS `Requirement`,
    `RequirementProblemReason` AS `RequirementProblemReason`,
    `RequirementStatus` AS `RequirementStatus`,
    `Skill` AS `Skill`,
    `Status` AS `Status`,
    `TimeService` AS `TimeService`,
    `TotalTimeService` AS `TotalTimeService`,
    `InactiveTimeService` AS `InactiveTimeService`,
    `TypeOfCover` AS `TypeOfCover`,
    `CreateTime` AS `CreateTime`,
    `ExaminerEmail` AS `ExaminerEmail`,
    `ExamLocation` AS `ExamLocation`,
    `GpEmail` AS `GpEmail`,
    `GPPracticeFax` AS `GPPracticeFax`,
    `LifeIndicator` AS `LifeIndicator`,
    `GpPaymentType` AS `GpPaymentType`,
    `GpAccountNo` AS `GpAccountNo`,
    `GpSortCode` AS `GpSortCode`,
    `LastChargeReview` AS `LastChargeReview`,
    `BankName` AS `BankName`,
    `PayeeEmailAddress` AS `PayeeEmailAddress`
from
    `dbo_OrderExamsReportsData`;

COMMIT;