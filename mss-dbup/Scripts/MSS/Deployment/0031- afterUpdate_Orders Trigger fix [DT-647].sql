START TRANSACTION;

	DROP TRIGGER afterUpdate_Orders;
	
	CREATE TRIGGER `afterUpdate_Orders` AFTER UPDATE ON `dbo_Orders` FOR EACH ROW UPDATE grid_AllOrderExams SET 
				Origin = NEW.OrderingChannelId,
				TypeOfCover = NEW.CoverTypeId,
				PolicyNumber = NEW.PolicyNumber,
				ClientReference2 = NEW.ClientReference2,
				IsAnonymised = NEW.IsAnonymised,
				OrderingCustomerTeamMemberId = NEW.OrderingCustomerTeamMemberId,
				OrderingCustomerTeamId = IFNULL(NEW.OrderingCustomerTeamId, '00000000-0000-0000-0000-000000000000')
			WHERE OrderId = NEW.Id;
COMMIT;