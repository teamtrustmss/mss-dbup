START TRANSACTION;

	INSERT INTO dbo_Activities (Id, Code, Description, ActivityTypeId, MaxDaysLimit, DefaultNumberOfDays, VisibleToExaminer, VisibleToOrderingCustomer, AllowMedicalExaminerUse, AllowedOrderStatuses, NextActionDateCalculationMode, SuspendedTimeService, NextActivityId, CommentRequired, CalculateNADFromScheduledDate, IsActive, NotificationType, SystemActivityComment, CreateTime, ModifyTime, AllowWebhookNotifications)
		VALUES('415f0d39-d48a-ef11-bb72-0a61967cf1f6', '11100', 'Active on Tele-interview', 1, NULL, NULL, 0, 1, 0, 12291, 2, 0, NULL, 0, 0, 1, 0, NULL, NOW(), NOW(), 0);

COMMIT;