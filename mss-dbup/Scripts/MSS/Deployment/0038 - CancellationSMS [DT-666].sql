START TRANSACTION;
	ALTER TABLE dbo_OrderingCustomers ADD SendCancellationSms tinyint(1) DEFAULT 0 NULL;
	INSERT INTO MssPlatformDb.dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
	 ('06b8e432-806a-ed11-8153-0c80635cdde9',222,'SMS Exam is cancelled by the OC','2022-11-22 18:10:37.055132000','2022-11-22 18:10:37.055132000',1);
COMMIT;