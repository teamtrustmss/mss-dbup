START TRANSACTION;

	INSERT INTO dbo_OcExamTypes (Id,Name,Code,SortOrder) VALUES
		(8,'Care','C',80);
	 
	INSERT INTO dbo_ExaminerTypes (Id,Name,SortOrder) VALUES
		(8,'Care',80);
	
	ALTER TABLE dbo_MedicalExaminations ADD CareTypeId int DEFAULT 0 NOT NULL;
	
	CREATE TABLE `dbo_Specialists` (
		`Id` varchar(64) NOT NULL,
		`Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		UNIQUE KEY `Id` (`Id`),
		UNIQUE KEY `Specialists_Name` (`Name`(255))
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO dbo_Specialists (Id, Name, IsActive, CreateTime, ModifyTime) VALUES 
		(UUID(), 'A & E Consultant', 1, NOW(), NOW()),
		(UUID(), 'Audiologist', 1, NOW(), NOW()),
		(UUID(), 'Cardiologist', 1, NOW(), NOW()),
		(UUID(), 'Counsellor', 1, NOW(), NOW()),
		(UUID(), 'Respiratory Physician', 1, NOW(), NOW()),
		(UUID(), 'Chiropractor', 1, NOW(), NOW()),
		(UUID(), 'Psychologist', 1, NOW(), NOW()),
		(UUID(), 'Colorectal Surgeon', 1, NOW(), NOW()),
		(UUID(), 'Dentist', 1, NOW(), NOW()),
		(UUID(), 'Dermatologist', 1, NOW(), NOW()),
		(UUID(), 'Endocrinologist', 1, NOW(), NOW()),
		(UUID(), 'ENT Surgeon', 1, NOW(), NOW()),
		(UUID(), 'Gastroenterologist', 1, NOW(), NOW()),
		(UUID(), 'General Surgeon', 1, NOW(), NOW()),
		(UUID(), 'Geriatrician', 1, NOW(), NOW()),
		(UUID(), 'Occupational Health Physician', 1, NOW(), NOW()),
		(UUID(), 'Gynaecologist', 1, NOW(), NOW()),
		(UUID(), 'Neurologist', 1, NOW(), NOW()),
		(UUID(), 'Neuropsychiatrist', 1, NOW(), NOW()),
		(UUID(), 'Neuropsychologist', 1, NOW(), NOW()),
		(UUID(), 'Rehabilitation Consultant', 1, NOW(), NOW()),
		(UUID(), 'Neurosurgeon', 1, NOW(), NOW()),
		(UUID(), 'Nurse', 1, NOW(), NOW()),
		(UUID(), 'Obstetrician', 1, NOW(), NOW()),
		(UUID(), 'Occupational Therapist', 1, NOW(), NOW()),
		(UUID(), 'Oncologist', 1, NOW(), NOW()),
		(UUID(), 'Ophthalmologist', 1, NOW(), NOW()),
		(UUID(), 'Maxillofacial Surgeon', 1, NOW(), NOW()),
		(UUID(), 'Orthopaedic Surgeon', 1, NOW(), NOW()),
		(UUID(), 'Osteopath', 1, NOW(), NOW()),
		(UUID(), 'Paediatrician', 1, NOW(), NOW()),
		(UUID(), 'Pain Management Specialist (Aneathetist)', 1, NOW(), NOW()),
		(UUID(), 'Physiotherapist', 1, NOW(), NOW()),
		(UUID(), 'Plastic Surgeon', 1, NOW(), NOW()),
		(UUID(), 'Podiatrist', 1, NOW(), NOW()),
		(UUID(), 'Psychiatrist', 1, NOW(), NOW()),
		(UUID(), 'Rheumatologist', 1, NOW(), NOW()),
		(UUID(), 'Speech and Language Therapist', 1, NOW(), NOW()),
		(UUID(), 'Urologist', 1, NOW(), NOW()),
		(UUID(), 'Vascular Surgeon', 1, NOW(), NOW());

	ALTER TABLE dbo_OrderExams
		ADD Symptoms text NULL,
		ADD Conditions text NULL,
		ADD IncidentDate datetime(6) NULL,
		ADD TribunalDate datetime(6) NULL,
		ADD ConsentSignedDate datetime(6) NULL,
		ADD ConsentRenewalSignedDate datetime(6) NULL,
		ADD ConsentFormSentToApplicant datetime(6) NULL,
		ADD SpecialistId varchar(64) NULL,
		ADD KEY `FK_OrderExams_Specialists` (`SpecialistId`),
		ADD CONSTRAINT `FK_OrderExams_Specialists` FOREIGN KEY (`SpecialistId`) REFERENCES `dbo_Specialists` (`Id`);
	
	CREATE TABLE `dbo_OrderExamRepresentatives` (
		`Id` varchar(64) NOT NULL,
		`OrderExamId` varchar(64) NOT NULL,
		`Title` varchar(255) NOT NULL,
		`FirstName` varchar(255) NOT NULL,
		`LastName` varchar(255) NOT NULL,
		`Office` varchar(510) NULL,
		`Ref` varchar(510) NULL,
		`RepresentativeTypeId` int NOT NULL,
		`WorkPhone` varchar(30) NULL,
		`MobilePhone` varchar(30) NULL,
		`Email` varchar(255) NOT NULL,
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		UNIQUE KEY `Id` (`Id`),
		CONSTRAINT `FK_OrderExamRepresentatives_OrderExams` FOREIGN KEY (`OrderExamId`) REFERENCES `dbo_OrderExams` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

	CREATE TABLE `dbo_CareTreatments` (
		`Id` varchar(64) NOT NULL,
		`Code` varchar(255) NOT NULL,
		`Description` varchar(510) NOT NULL,
		`IsNumberRequired` tinyint(1) NOT NULL DEFAULT '0',
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		UNIQUE KEY `Id` (`Id`),
		UNIQUE KEY `CareTreatments_Code` (`Code`(255))
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO `dbo_CareTreatments` (`Id`, `Code`, `Description`, `IsNumberRequired`, `IsActive`, `CreateTime`, `ModifyTime`) VALUES
		(UUID(), 'AX', 'Assessment', 0, 1, NOW(), NOW()),
		(UUID(), 'RX', 'Treatment', 1, 1, NOW(), NOW()),
		(UUID(), 'PR', 'Peer Review', 0, 1, NOW(), NOW()),
		(UUID(), 'OS', 'Other Services', 0, 1, NOW(), NOW()),
		(UUID(), 'TRA', 'Triage Admin Fee', 0, 1, NOW(), NOW()),
		(UUID(), 'DNA', 'Did Not Attend', 0, 1, NOW(), NOW()),
		(UUID(), 'XPS', 'Additional Expenses', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_AX', 'Specialist assessment and rpt', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_NTS', 'Specialist Notes or Records', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_OS', 'Specialist Other Services', 0, 1, NOW(), NOW()),
		(UUID(), 'CMF', 'Case Management Fee', 0, 1, NOW(), NOW()),
		(UUID(), 'SMF', 'Support & Maintenance Fees', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_SC', 'Service Credit', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_CAN', 'Specialist Case Cancelled', 0, 1, NOW(), NOW()),
		(UUID(), 'SP-CMO', 'CMO Fee', 0, 1, NOW(), NOW()),
		(UUID(), 'MRI', 'MRI Scan', 0, 1, NOW(), NOW()),
		(UUID(), 'CAN-FEE', 'Cancellation Fee', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_PSY', 'Specialist Ax & Rpt - Psychology', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_PSYC', 'Sp Ax & Rpt - Psychiatrist', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_NPS', 'Sp Ax & Rpt - Neuropsychiatrist', 0, 1, NOW(), NOW()),
		(UUID(), 'ADD', 'Addendum Report', 0, 1, NOW(), NOW()),
		(UUID(), 'DESK', 'Desktop Report', 0, 1, NOW(), NOW()),
		(UUID(), 'SP_ORTH', 'Sp Ax & Rpt - Orthopaedic', 0, 1, NOW(), NOW()),
		(UUID(), 'INJ', 'Injection', 0, 1, NOW(), NOW()),
		(UUID(), 'X-RAY', 'X-ray', 0, 1, NOW(), NOW()),
		(UUID(), 'SURGERY', 'Surgery/Operation', 0, 1, NOW(), NOW()),
		(UUID(), 'CONS', 'Consultation', 0, 1, NOW(), NOW());

	CREATE TABLE `dbo_OrderExamTreatments` (
		`Id` varchar(64) NOT NULL,
		`OrderExamId` varchar(64) NOT NULL,
		`TreatmentId` varchar(64) NOT NULL,
		`AddedById` varchar(64) NOT NULL,
		`Occurrences` int NOT NULL DEFAULT '1',
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		UNIQUE KEY `Id` (`Id`),
		CONSTRAINT `FK_OrderExamTreatments_CareTreatments` FOREIGN KEY (`TreatmentId`) REFERENCES `dbo_CareTreatments` (`Id`),
		CONSTRAINT `FK_OrderExamTreatments_OrderExams` FOREIGN KEY (`OrderExamId`) REFERENCES `dbo_OrderExams` (`Id`),
		CONSTRAINT `FK_OrderExamTreatments_Users` FOREIGN KEY (`AddedById`) REFERENCES `dbo_Users` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

	CREATE TABLE `dbo_SpecialistDetails` (
		`Id` varchar(64) NOT NULL,
		`Title` varchar(24) DEFAULT NULL,
		`FirstName` varchar(255) DEFAULT NULL,
		`LastName` varchar(255) DEFAULT NULL,
		`AddressLine1` varchar(255) DEFAULT NULL,
		`AddressLine2` varchar(255) DEFAULT NULL,
		`AddressLine3` varchar(255) DEFAULT NULL,
		`Town` varchar(255) DEFAULT NULL,
		`County` varchar(255) DEFAULT NULL,
		`Country` varchar(255) DEFAULT NULL,
		`PostCode` varchar(24) DEFAULT NULL,
		`Phone` varchar(255) DEFAULT NULL,
		`Email` varchar(255) DEFAULT NULL,
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		UNIQUE KEY `Id` (`Id`),
		CONSTRAINT `FK_SpecialistDetails_Exams` FOREIGN KEY (`Id`) REFERENCES `dbo_OrderExams` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

	CREATE TABLE `dbo_CareInvoices` (
		`Id` varchar(64) NOT NULL,
		`OrderExamId` varchar(64) NOT NULL,
		`InvoiceDocumentId` varchar(64) NOT NULL,
		`InvoiceFee` decimal(18,2) NOT NULL DEFAULT '0',
		`InvoiceReferenceNumber` varchar(255) DEFAULT NULL,
		`PayeeName` varchar(255) DEFAULT NULL,
		`PaymentType` int NOT NULL DEFAULT '0',
		`AccountNo` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
		`SortCode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
		`PayeeEmailAddress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
		`VAT` tinyint(1) NOT NULL DEFAULT '0',
		`VATValue` decimal(5,2) NOT NULL DEFAULT '0',
		`InvoiceStatusId` int NOT NULL,
		`InvoiceTypeId` int NOT NULL,
		`InvoicePaymentStatusId` int NOT NULL,
		`IsActive` tinyint(1) NOT NULL DEFAULT '1',
		`CreateTime` datetime(6) NOT NULL,
		`ModifyTime` datetime(6) NOT NULL,
		PRIMARY KEY (`Id`),
		UNIQUE KEY `Id` (`Id`),
		KEY `FK_CareInvoicings_OrderExams` (`OrderExamId`),
		KEY `FK_CareInvoicings_Documents` (`InvoiceDocumentId`),
		CONSTRAINT `FK_CareInvoicings_OrderExams` FOREIGN KEY (`OrderExamId`) REFERENCES `dbo_OrderExams` (`Id`),
		CONSTRAINT `FK_CareInvoicings_Documents` FOREIGN KEY (`InvoiceDocumentId`) REFERENCES `dbo_Documents` (`Id`),
		CHECK (VATValue >= 0 AND VATValue <= 100),
		CHECK (InvoiceFee >= 0)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		('8d431bb7-31e2-488d-a6a4-d54a2c894cf5',810,'Care Invoice',NOW(),NOW(),1);
	
	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,
		AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,
		CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('4d8cf91c-33f0-4392-a517-bf651fbefa98','801','[rep] has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('12c31d3f-2149-4f39-ba38-5566d8754da2','802','[rep] has been removed',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('da937ca2-1cbf-4a9e-b0a1-78fae3851a08','803','New [rep] has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('547a4616-55d0-48ef-87a8-e3e33e097c34','805','[treatment] treatment has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('510237a9-d35a-4f3c-b1dd-5965a08896ac','806','New [treatment] treatment has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('02a366f7-6eb0-4de2-9c3a-47addd4ce7f0','807','[treatment] treatment has been removed',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('99564e7b-9247-4e41-bae3-a80f7e2abadf','808','[date] has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('33b44cb2-7431-44d9-b170-ea7e6a181fef','809','[date] has been updated',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('83716a8c-7e1e-478f-8165-de839f4f284b','810','[invoice] has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('f4b6471a-c943-401a-a52f-a4a015ea12bc','811','[invoice] has been approved',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('49f6500e-a7b9-4fdf-9305-d126af079ec3','812','[invoice] has been rejected',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('8d346eb3-48bd-4953-b2cd-b6eb4dcf9447','813','[invoice] has been sent for payment',1,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('9208da95-cbad-4f75-9f34-cc6302fb1282','814','[invoice] has been paid',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('d18582f0-50f2-4e43-b548-5283e18228ba','815','Another [invoice] has been added',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('220e12d2-8393-4ced-8adf-8743596af442','816','[invoice] has been modified',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);

COMMIT;