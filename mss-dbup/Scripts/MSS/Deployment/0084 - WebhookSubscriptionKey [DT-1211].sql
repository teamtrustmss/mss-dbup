START TRANSACTION;

	ALTER TABLE dbo_WebhookEndpoints ADD UseSubscriptionKey tinyint(1) DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_WebhookEndpoints ADD SubscriptionKeyValue varchar(256) NULL;
	
COMMIT;