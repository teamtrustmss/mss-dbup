START TRANSACTION;

    ALTER TABLE grid_AllOrderExams ADD GpSurgery varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL;

DELIMITER $$ 

	CREATE TRIGGER `afterUpdate_GeneralPractitioner` AFTER UPDATE ON `dbo_GeneralPractitioners` FOR EACH ROW BEGIN 
	    UPDATE grid_AllOrderExams AOE 
	        JOIN dbo_OrderExams oe ON oe.Id = AOE.Id
	    SET 
	        GpSurgery = NEW.PracticeName
	    WHERE NEW.Id = oe.Id
	   	AND oe.ExamTypeId = 4;
	END$$
DELIMITER ;

DELIMITER $$ 
	CREATE TRIGGER `afterInsert_GeneralPractitioner` AFTER INSERT ON `dbo_GeneralPractitioners` FOR EACH ROW BEGIN
	    UPDATE grid_AllOrderExams AOE
	        JOIN dbo_OrderExams oe ON oe.Id = AOE.Id
	    SET 
	        GpSurgery = NEW.PracticeName
	    WHERE NEW.Id = oe.Id
	    AND oe.ExamTypeId = 4;
	END$$
DELIMITER ;

COMMIT;