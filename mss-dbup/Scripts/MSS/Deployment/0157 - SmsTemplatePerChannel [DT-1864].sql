START TRANSACTION;

	ALTER TABLE dbo_Documents ADD EvidenceTypes int DEFAULT 0 NOT NULL;

	-- Add only Nurse, Doctor types for SmsExamCreated or SmsExamCancelled
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 6
		WHERE dt.ReferenceNumber IN (200, 222); -- SmsExamCreated, SmsExamCancelled

	-- Add only Nurse, Doctor, Tele-interview and Care types for other specified types
	UPDATE dbo_Documents d
	INNER JOIN dbo_DocumentTypes dt ON d.DocumentTypeId = dt.Id
	INNER JOIN dbo_Files df2 ON d.Id = df2.Id
	INNER JOIN dbo_FilesInPack df ON df.FileId = df2.Id
	INNER JOIN dbo_Aggregates agg ON agg.PackId = df.PackId
	INNER JOIN dbo_OrderingCustomers oc ON oc.Id = agg.Id
	SET d.EvidenceTypes = oc.EvidenceTypes & 390
		WHERE dt.ReferenceNumber IN (240, 241, 250, 251); -- SmsAppointmentScheduled, SmsAppointmentRescheduled, etc.

COMMIT;