START TRANSACTION;

	ALTER TABLE dbo_Orders ADD ClientReference2 varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL;
	ALTER TABLE dbo_OrderExamsReportsData ADD ClientReference2 varchar(500) NULL;
    ALTER TABLE grid_AllOrderExams ADD ClientReference2 varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL;
	ALTER TABLE dbo_OrderingCustomers ADD IsClientReference2Required tinyint(1) DEFAULT 0 NOT NULL;
	DROP TRIGGER afterInsert_OrderExam;
	DROP TRIGGER afterUpdate_Orders;
	
	CREATE OR REPLACE
		ALGORITHM = UNDEFINED VIEW `dbo_GridAllOrderExams` AS
		select
			`orderExam`.`Id` AS `Id`,
			`orderExam`.`OrderId` AS `OrderId`,
			`orderExam`.`IsActive` AS `IsActive`,
			`orderExam`.`OrderExamNumber` AS `OrderExamNumber`,
			`orderExam`.`ExamType` AS `ExamType`,
			`orderExam`.`NextActionDate` AS `NextActionDate`,
			ifnull(`orderExam`.`NextActionDate`, '9999-12-31') AS `NextActionDateSort`,
			`orderExam`.`Origin` AS `Origin`,
			`orderExam`.`TypeOfCover` AS `TypeOfCover`,
			`orderExam`.`OrderingCustomer` AS `OrderingCustomer`,
			`orderExam`.`ExamPostCode` AS `ExamPostCode`,
			`orderExam`.`ApplicantFirstName` AS `ApplicantFirstName`,
			`orderExam`.`ApplicantLastName` AS `ApplicantLastName`,
			`orderExam`.`ApplicantDoB` AS `ApplicantDoB`,
			`orderExam`.`PolicyNumber` AS `PolicyNumber`,
			`orderExam`.`ClientReference2` AS `ClientReference2`,
			`orderExam`.`IsUrgentCase` AS `IsUrgentCase`,
			`orderExam`.`IsTouched` AS `IsTouched`,
			`orderExam`.`OrderDate` AS `OrderDate`,
			`orderExam`.`Status` AS `Status`,
			`orderExam`.`AppointmentScheduled` AS `AppointmentScheduled`,
			`orderExam`.`ExaminerFirstName` AS `ExaminerFirstName`,
			`orderExam`.`ExaminerLastName` AS `ExaminerLastName`,
			`orderExam`.`ExamOwnerId` AS `ExamOwnerId`,
			`orderExam`.`CsrFirstName` AS `CsrFirstName`,
			`orderExam`.`CsrLastName` AS `CsrLastName`,
			`orderExam`.`GpName` AS `GpName`,
			`orderExam`.`ClinicName` AS `ClinicName`,
			`orderExam`.`HighNetWorth` AS `HighNetWorth`,
			`orderExam`.`International` AS `International`,
			`orderExam`.`AssignedToPrefix` AS `AssignedToPrefix`,
			`orderExam`.`AssignedTo` AS `AssignedTo`,
			`orderExam`.`Applicant` AS `Applicant`,
			`orderExam`.`AssignedCSR` AS `AssignedCSR`,
			`orderExam`.`HasLinkedOrders` AS `HasLinkedOrders`,
			ifnull((select `dbo_GetTimeServiceForActivity`(`orderExam`.`LatestActivityId`, NULL)), 0) AS `TimeService`,
			`orderExam`.`OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
			`orderExam`.`OrderingCustomerTeamId` AS `OrderingCustomerTeamId`,
			`orderExam`.`ApplicantPostCode` AS `ApplicantPostCode`,
			`orderExam`.`CreatorName` AS `CreatorName`,
			`orderExam`.`IsAnonymised` AS `IsAnonymised`
		from
			(
			select
				`orderExam`.`Id` AS `Id`,
				`ocOrder`.`Id` AS `OrderId`,
				`orderExam`.`IsActive` AS `IsActive`,
				`orderExam`.`ExamCustomId` AS `OrderExamNumber`,
				`orderExam`.`ExamTypeId` AS `ExamType`,
				(
				select
					`dbo_GetExamNextActionDate`(`orderExam`.`Id`)) AS `NextActionDate`,
				`ocOrder`.`OrderingChannelId` AS `Origin`,
				`ocOrder`.`CoverTypeId` AS `TypeOfCover`,
				`oc`.`CompanyName` AS `OrderingCustomer`,
				`appointment`.`PostCode` AS `ExamPostCode`,
				`applicant`.`FirstName` AS `ApplicantFirstName`,
				`applicant`.`LastName` AS `ApplicantLastName`,
				`applicant`.`DateOfBirth` AS `ApplicantDoB`,
				`ocOrder`.`PolicyNumber` AS `PolicyNumber`,
				`ocOrder`.`ClientReference2` AS `ClientReference2`,
				`orderExam`.`IsUrgent` AS `IsUrgentCase`,
				0 AS `IsTouched`,
				`orderExam`.`CreateTime` AS `OrderDate`,
				`orderExam`.`StatusId` AS `Status`,
				`appointment`.`Date` AS `AppointmentScheduled`,
				`medicalExaminer`.`FirstName` AS `ExaminerFirstName`,
				`medicalExaminer`.`LastName` AS `ExaminerLastName`,
				`examOwner`.`Id` AS `ExamOwnerId`,
				`examOwner`.`FirstName` AS `CsrFirstName`,
				`examOwner`.`LastName` AS `CsrLastName`,
				`gpPractice`.`DoctorName` AS `GpName`,
				`clinic`.`Name` AS `ClinicName`,
				`ocOrder`.`IsAnonymised` AS `IsAnonymised`,
				(
				select
					ifnull((select 1 from `dbo_OrderExamRequirements` `R` where ((`orderExam`.`Id` = `R`.`OrderExamId`) and ((`R`.`Code` = 'HV FEE') or (`R`.`Code` = 'UHV'))) limit 1), 0)) AS `HighNetWorth`,
				(
				select
					ifnull((select 1 from `dbo_OrderExamRequirements` `R` where ((`orderExam`.`Id` = `R`.`OrderExamId`) and (`R`.`Code` = 'INT FEE')) limit 1), 0)) AS `International`,
				(
				select
					ifnull((select 1 from `dbo_LinkedOrders` `LO` where ((`LO`.`OrderId` = `orderExam`.`OrderId`) or (`LO`.`ParentOrderId` = `orderExam`.`OrderId`)) limit 1), 0)) AS `HasLinkedOrders`,
				(
				select
					`OEA`.`AssignedToPrefix`
				from
					`dbo_OrderExamActivities` `OEA`
				where
					(`OEA`.`OrderExamId` = `orderExam`.`Id`)
				order by
					`OEA`.`CreateTime` desc
				limit 1) AS `AssignedToPrefix`,
				(
				select
					(case
						when (nullif(`OEA`.`AssignedToPrefix`, '') is null) then `OEA`.`AssignedTo`
						else concat(`OEA`.`AssignedToPrefix`, ' - ', `OEA`.`AssignedTo`)
					end)
				from
					`dbo_OrderExamActivities` `OEA`
				where
					(`OEA`.`OrderExamId` = `orderExam`.`Id`)
				order by
					`OEA`.`CreateTime` desc
				limit 1) AS `AssignedTo`,
				concat(`applicant`.`FirstName`, ' ', `applicant`.`LastName`) AS `Applicant`,
				concat(`examOwner`.`FirstName`, ' ', `examOwner`.`LastName`) AS `AssignedCSR`,
				(
				select
					`E`.`Id`
				from
					`dbo_OrderExamActivities` `E`
				where
					(`E`.`OrderExamId` = `orderExam`.`Id`)
				order by
					`E`.`CreateTime` desc
				limit 1) AS `LatestActivityId`,
				`ocOrder`.`OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
				ifnull(`ocOrder`.`OrderingCustomerTeamId`, '00000000-0000-0000-0000-000000000000') AS `OrderingCustomerTeamId`,
				(
				select
					distinct group_concat(distinct `aa`.`PostCode` separator ', ')
				from
					(`dbo_Applicants` `a`
				left join `dbo_ApplicantAddresses` `aa` on
					((`a`.`Id` = `aa`.`ApplicantId`)))
				where
					(`applicant`.`Id` = `a`.`Id`)
				group by
					`a`.`Id`,
					`aa`.`ApplicantId`) AS `ApplicantPostCode`,
				(
				select
					concat(`creator`.`FirstName`, ' ', `creator`.`LastName`)
				from
					(`dbo_OrderExamActivities` `OEA`
				join `dbo_Users` `creator` on
					((`OEA`.`CreatorId` = `creator`.`Id`)))
				where
					((`OEA`.`PreviousActivityId` is null)
						and (`OEA`.`OrderExamId` = `orderExam`.`Id`))
				order by
					`OEA`.`CreateTime`
				limit 1) AS `CreatorName`
			from
				((((((((`dbo_OrderExams` `orderExam`
			join `dbo_Orders` `ocOrder` on
				((`ocOrder`.`Id` = `orderExam`.`OrderId`)))
			join `dbo_OrderingCustomers` `oc` on
				((`oc`.`Id` = `ocOrder`.`OrderingCustomerId`)))
			join `dbo_Applicants` `applicant` on
				((`applicant`.`Id` = `ocOrder`.`Id`)))
			left join `dbo_OrderExamAppointments` `appointment` on
				((`appointment`.`Id` = `orderExam`.`Id`)))
			left join `dbo_MedicalExaminers` `medicalExaminer` on
				((`medicalExaminer`.`Id` = `orderExam`.`AssignedExaminerId`)))
			left join `dbo_GpPracticeIndividualContacts` `gpPractice` on
				((`gpPractice`.`Id` = `orderExam`.`OwnGpIndividualContactId`)))
			left join `dbo_Clinics` `clinic` on
				((`clinic`.`Id` = `orderExam`.`AssignedExaminerId`)))
			left join `dbo_Users` `examOwner` on
				((`examOwner`.`Id` = `orderExam`.`ExamOwnerId`)))) `orderExam`;
		 
       
	CREATE OR REPLACE
		ALGORITHM = UNDEFINED VIEW `dbo_GridAllOrderExamsReports` AS
		select
			`dbo_OrderExamsReportsData`.`ActivityCommentsAll` AS `ActivityCommentsAll`,
			`dbo_OrderExamsReportsData`.`ActivityDescriptionsAll` AS `ActivityDescriptionsAll`,
			`dbo_OrderExamsReportsData`.`AddressLine1` AS `AddressLine1`,
			`dbo_OrderExamsReportsData`.`AddressLine2` AS `AddressLine2`,
			`dbo_OrderExamsReportsData`.`AddressLine3` AS `AddressLine3`,
			`dbo_OrderExamsReportsData`.`ApplicantDoB` AS `ApplicantDoB`,
			`dbo_OrderExamsReportsData`.`ApplicantEmail` AS `ApplicantEmail`,
			`dbo_OrderExamsReportsData`.`ApplicantFirstName` AS `ApplicantFirstName`,
			`dbo_OrderExamsReportsData`.`ApplicantGender` AS `ApplicantGender`,
			`dbo_OrderExamsReportsData`.`ApplicantLastName` AS `ApplicantLastName`,
			`dbo_OrderExamsReportsData`.`ApplicantMobileNumber` AS `ApplicantMobileNumber`,
			`dbo_OrderExamsReportsData`.`ApplicantName` AS `ApplicantName`,
			`dbo_OrderExamsReportsData`.`ApplicantPostCode` AS `ApplicantPostCode`,
			`dbo_OrderExamsReportsData`.`AppointmentScheduled` AS `AppointmentScheduled`,
			`dbo_OrderExamsReportsData`.`AssignedCSR` AS `AssignedCSR`,
			`dbo_OrderExamsReportsData`.`AssignedTo` AS `AssignedTo`,
			`dbo_OrderExamsReportsData`.`AssignedToPrefix` AS `AssignedToPrefix`,
			`dbo_OrderExamsReportsData`.`BestContact` AS `BestContact`,
			`dbo_OrderExamsReportsData`.`BilledDate` AS `BilledDate`,
			`dbo_OrderExamsReportsData`.`CancellationReason` AS `CancellationReason`,
			`dbo_OrderExamsReportsData`.`City` AS `City`,
			`dbo_OrderExamsReportsData`.`ClinicName` AS `ClinicName`,
			`dbo_OrderExamsReportsData`.`CompletedDate` AS `CompletedDate`,
			`dbo_OrderExamsReportsData`.`CreatorName` AS `CreatorName`,
			`dbo_OrderExamsReportsData`.`CsrFirstName` AS `CsrFirstName`,
			`dbo_OrderExamsReportsData`.`CsrLastName` AS `CsrLastName`,
			`dbo_OrderExamsReportsData`.`ExaminerFirstName` AS `ExaminerFirstName`,
			`dbo_OrderExamsReportsData`.`ExaminerLastName` AS `ExaminerLastName`,
			`dbo_OrderExamsReportsData`.`ExaminerPrepaymentFlag` AS `ExaminerPrepaymentFlag`,
			`dbo_OrderExamsReportsData`.`ExamOwnerId` AS `ExamOwnerId`,
			`dbo_OrderExamsReportsData`.`ExamPostCode` AS `ExamPostCode`,
			`dbo_OrderExamsReportsData`.`ExamType` AS `ExamType`,
			`dbo_OrderExamsReportsData`.`GpName` AS `GpName`,
			`dbo_OrderExamsReportsData`.`GPPracticePostCode` AS `GPPracticePostCode`,
			`dbo_OrderExamsReportsData`.`GroupPractice` AS `GroupPractice`,
			`dbo_OrderExamsReportsData`.`HighNetWorth` AS `HighNetWorth`,
			`dbo_OrderExamsReportsData`.`Id` AS `Id`,
			`dbo_OrderExamsReportsData`.`InvoiceReferenceNumber` AS `InvoiceReferenceNumber`,
			`dbo_OrderExamsReportsData`.`IsActive` AS `IsActive`,
			`dbo_OrderExamsReportsData`.`IsUrgentCase` AS `IsUrgentCase`,
			`dbo_OrderExamsReportsData`.`LabFeeTotal` AS `LabFeeTotal`,
			`dbo_OrderExamsReportsData`.`LatestActivityComment` AS `LatestActivityComment`,
			`dbo_OrderExamsReportsData`.`LatestActivityDescription` AS `LatestActivityDescription`,
			`dbo_OrderExamsReportsData`.`MeFeeTotal` AS `MeFeeTotal`,
			`dbo_OrderExamsReportsData`.`MiscFeeDescription` AS `MiscFeeDescription`,
			`dbo_OrderExamsReportsData`.`MiscFeeTotal` AS `MiscFeeTotal`,
			`dbo_OrderExamsReportsData`.`MiscCharge` AS `MiscCharge`,
			`dbo_OrderExamsReportsData`.`MiscChargeDescription` AS `MiscChargeDescription`,
			`dbo_OrderExamsReportsData`.`NextActionDate` AS `NextActionDate`,
			`dbo_OrderExamsReportsData`.`OcCharge` AS `OcCharge`,
			`dbo_OrderExamsReportsData`.`OcContact` AS `OcContact`,
			`dbo_OrderExamsReportsData`.`OcServiceDescription` AS `OcServiceDescription`,
			`dbo_OrderExamsReportsData`.`OrderDate` AS `OrderDate`,
			`dbo_OrderExamsReportsData`.`OrderExamNumber` AS `OrderExamNumber`,
			`dbo_OrderExamsReportsData`.`OrderId` AS `OrderId`,
			`dbo_OrderExamsReportsData`.`OrderingCustomer` AS `OrderingCustomer`,
			`dbo_OrderExamsReportsData`.`OrderingCustomerTeam` AS `OrderingCustomerTeam`,
			`dbo_OrderExamsReportsData`.`OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
			`dbo_OrderExamsReportsData`.`Origin` AS `Origin`,
			`dbo_OrderExamsReportsData`.`PayeeName` AS `PayeeName`,
			`dbo_OrderExamsReportsData`.`PolicyNumber` AS `PolicyNumber`,
			`dbo_OrderExamsReportsData`.`ClientReference2` AS `ClientReference2`,
			`dbo_OrderExamsReportsData`.`PracticeName` AS `PracticeName`,
			`dbo_OrderExamsReportsData`.`Qualification` AS `Qualification`,
			`dbo_OrderExamsReportsData`.`Requirement` AS `Requirement`,
			`dbo_OrderExamsReportsData`.`RequirementProblemReason` AS `RequirementProblemReason`,
			`dbo_OrderExamsReportsData`.`RequirementStatus` AS `RequirementStatus`,
			`dbo_OrderExamsReportsData`.`Skill` AS `Skill`,
			`dbo_OrderExamsReportsData`.`Status` AS `Status`,
			`dbo_OrderExamsReportsData`.`TimeService` AS `TimeService`,
			`dbo_OrderExamsReportsData`.`TotalTimeService` AS `TotalTimeService`,
			`dbo_OrderExamsReportsData`.`TypeOfCover` AS `TypeOfCover`,
			`dbo_OrderExamsReportsData`.`CreateTime` AS `CreateTime`,
			`dbo_OrderExamsReportsData`.`ExaminerEmail` AS `ExaminerEmail`,
			`dbo_OrderExamsReportsData`.`ExamLocation` AS `ExamLocation`,
			`dbo_OrderExamsReportsData`.`GpEmail` AS `GpEmail`,
			`dbo_OrderExamsReportsData`.`GPPracticeFax` AS `GPPracticeFax`,
			`dbo_OrderExamsReportsData`.`LifeIndicator` AS `LifeIndicator`,
			`dbo_OrderExamsReportsData`.`GpPaymentType` AS `GpPaymentType`,
			`dbo_OrderExamsReportsData`.`GpAccountNo` AS `GpAccountNo`,
			`dbo_OrderExamsReportsData`.`GpSortCode` AS `GpSortCode`,
			`dbo_OrderExamsReportsData`.`LastChargeReview` AS `LastChargeReview`,
			`dbo_OrderExamsReportsData`.`BankName` AS `BankName`,
			`dbo_OrderExamsReportsData`.`PayeeEmailAddress` AS `PayeeEmailAddress`
		from
			`dbo_OrderExamsReportsData`;
			
	CREATE DEFINER=`root`@`%` TRIGGER `afterUpdate_Orders` AFTER UPDATE ON `dbo_Orders` FOR EACH ROW 
		UPDATE grid_AllOrderExams SET 
			Origin = NEW.OrderingChannelId,
			TypeOfCover = NEW.CoverTypeId,
			PolicyNumber = NEW.PolicyNumber,
			ClientReference2 = NEW.ClientReference2,
			IsAnonymised = NEW.IsAnonymised,
			OrderingCustomerTeamMemberId = NEW.OrderingCustomerTeamMemberId,
			OrderingCustomerTeamId = IFNULL(NEW.OrderingCustomerTeamId, '00000000-0000-0000-0000-000000000000')
		WHERE OrderId = NEW.Id;
		
	CREATE TRIGGER `afterInsert_OrderExam` AFTER INSERT ON `dbo_OrderExams` FOR EACH ROW 
	    INSERT INTO grid_AllOrderExams (Id,OrderId,IsActive,OrderExamNumber,ExamType, NextActionDate,NextActionDateSort,Origin,TypeOfCover,OrderingCustomer,ExamPostCode,ApplicantFirstName,ApplicantLastName,
	    ApplicantDoB,PolicyNumber,ClientReference2,IsUrgentCase,IsTouched,OrderDate,`Status`,AppointmentScheduled,ExaminerFirstName,ExaminerLastName,ExamOwnerId,CsrFirstName,CsrLastName,
	    GpName,ClinicName,IsAnonymised,HighNetWorth,International,HasLinkedOrders, AssignedToPrefix,AssignedTo,Applicant,AssignedCSR,LatestActivityId,TimeService,OrderingCustomerTeamMemberId,
	    OrderingCustomerTeamId,ApplicantPostCode,CreatorName)  SELECT 
			orderExam.Id                                        AS Id,
			ocOrder.Id                                          AS OrderId,
			orderExam.IsActive                                  AS IsActive,
			orderExam.ExamCustomId                              AS OrderExamNumber,
			orderExam.ExamTypeId                                AS ExamType,
			(SELECT dbo_GetExamNextActionDate(orderExam.Id))    AS NextActionDate,
			IFNULL((SELECT dbo_GetExamNextActionDate(orderExam.Id)), '9999-12-31')            AS NextActionDateSort,
			ocOrder.OrderingChannelId                           AS Origin,
			ocOrder.CoverTypeId                                 AS TypeOfCover,
			oc.CompanyName                                      AS OrderingCustomer,
			appointment.PostCode                                AS ExamPostCode,
			applicant.FirstName                                 AS ApplicantFirstName,
			applicant.LastName                                  AS ApplicantLastName,
			applicant.DateOfBirth                               AS ApplicantDoB,
			ocOrder.PolicyNumber                                AS PolicyNumber,
			ocOrder.ClientReference2                            AS ClientReference2,
			orderExam.IsUrgent                                  AS IsUrgentCase,
			
			
			0                                                   AS IsTouched,
			orderExam.CreateTime                                AS OrderDate,
			orderExam.StatusId                                  AS `Status`,
			appointment.`Date`                                  AS AppointmentScheduled,
			medicalExaminer.FirstName                           AS ExaminerFirstName,
			medicalExaminer.LastName                            AS ExaminerLastName,
			examOwner.Id                                        AS ExamOwnerId, 
			examOwner.FirstName                                 AS CsrFirstName,
			examOwner.LastName                                  AS CsrLastName,
			gpPractice.DoctorName                               AS GpName,
			clinic.`Name`                                       AS ClinicName,
			ocOrder.IsAnonymised                                AS IsAnonymised,
			
			(SELECT IFNULL((SELECT 1 
					FROM dbo_OrderExamRequirements R  
					WHERE orderExam.Id = R.OrderExamId 
					AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
					LIMIT 1), 0))                               AS HighNetWorth,
			(SELECT IFNULL((SELECT 1 
				FROM dbo_OrderExamRequirements R  
				WHERE orderExam.Id = R.OrderExamId 
				AND R.`Code` = 'INT FEE' 
				LIMIT 1), 0) )                                  AS International,
			(SELECT IFNULL((SELECT 1 
					FROM dbo_LinkedOrders LO 
					WHERE LO.OrderId = orderExam.OrderId 
					OR LO.ParentOrderId = orderExam.OrderId
					LIMIT 1), 0))                               AS HasLinkedOrders,
			(SELECT AssignedToPrefix 
				FROM dbo_OrderExamActivities OEA 
				WHERE OEA.OrderExamId = orderExam.Id 
				ORDER BY ActivityOrder DESC LIMIT 1)               AS AssignedToPrefix,
			(SELECT (CASE WHEN AssignedToPrefix IS NULL 
					THEN AssignedTo 
					ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo) 
					END) 
				FROM dbo_OrderExamActivities OEA 
				WHERE OEA.OrderExamId = orderExam.Id 
				ORDER BY ActivityOrder DESC LIMIT 1)               AS AssignedTo,
			CONCAT(applicant.FirstName, ' ', applicant.LastName)AS Applicant,
			CONCAT(examOwner.FirstName, ' ', examOwner.LastName)AS AssignedCSR,
			(SELECT Id 
				FROM dbo_OrderExamActivities E 
				WHERE E.OrderExamId=orderExam.Id 
				ORDER BY E.ActivityOrder DESC 
				LIMIT 1)                                        AS LatestActivityId,
			IFNULL((SELECT dbo_GetTimeServiceForActivity(LatestActivityId, NULL)),0) AS TimeService,
			ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,                            
			IFNULL(ocOrder.OrderingCustomerTeamId, '00000000-0000-0000-0000-000000000000') AS OrderingCustomerTeamId,
			(SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
				FROM dbo_Applicants a 
				LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
				WHERE applicant.Id = a.Id
				GROUP BY a.Id, aa.ApplicantId
			)                                                    AS ApplicantPostCode,
			(SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
				FROM dbo_OrderExamActivities OEA 
				INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
				WHERE OEA.PreviousActivityId IS NULL 
					AND OEA.OrderExamId= orderExam.Id
				ORDER BY OEA.ActivityOrder ASC
				LIMIT 1)                                        AS CreatorName
	 FROM dbo_OrderExams orderExam
		INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId 
		INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId 
		INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id
		LEFT JOIN dbo_OrderExamAppointments appointment ON appointment.Id = orderExam.Id
		LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId
		LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId
		LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId
		LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId
		WHERE orderExam.Id = NEW.Id;
	
COMMIT;