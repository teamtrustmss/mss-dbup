START TRANSACTION;

ALTER TABLE dbo_OrderExamsReportsData
	ADD OrderingCustomerDomicile varchar(50) NULL;
	
ALTER TABLE dbo_OrderExamsReportsDataNew
	ADD OrderingCustomerDomicile varchar(50) NULL;
	
INSERT INTO reporting_Fields (Id,Name,PropertyName,PropertyTypeId,CreateTime,ModifyTime,IsActive) VALUES
	('81198666-ff60-4d1e-abb5-4af89787e696','Ordering Customer domicile','OcDomicile',0,now(),now(),1);
	
DROP PROCEDURE IF EXISTS `dbo_UpdateOrderExamsReportsData`;
	
DELIMITER $$
	CREATE PROCEDURE `MssPlatformDb`.`dbo_UpdateOrderExamsReportsData`()
    MODIFIES SQL DATA
	BEGIN
		 DECLARE `should_rollback` BOOL DEFAULT FALSE;
		 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `should_rollback` = TRUE;
			 SET autocommit = 0;
			 
			 START TRANSACTION;
	 
			 DELETE FROM dbo_OrderExamsReportsData;
			 drop table if exists orderExamTempData;
			 SET @@group_concat_max_len = 256000;
			 create temporary table orderExamTempData select orderExam.`Id` as orderExamId,
					 (SELECT GROUP_CONCAT(NULLIF(b.`Code`,'') ORDER BY E.CreateTime ASC, b.Code ASC separator ', ')
										 FROM dbo_OrderExamActivities E
										 INNER JOIN dbo_Activities b ON E.ActivityId = b.Id
										 WHERE E.OrderExamId = orderExam.Id
										 GROUP BY E.OrderExamId)                         AS ActivityDescriptionsAll,
					 (SELECT ae.EmailAddress FROM dbo_ApplicantEmails ae
										 INNER JOIN dbo_Applicants a ON ae.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id
										 ORDER BY ae.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantEmail,
					 (SELECT ap.`Number` FROM dbo_ApplicantPhones ap
										 INNER JOIN dbo_Applicants a ON ap.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id AND ap.PhoneType = 3
										 ORDER BY ap.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantMobileNumber,
					 (SELECT ap.`Number` FROM dbo_ApplicantPhones ap
										 INNER JOIN dbo_Applicants a ON ap.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id AND ap.PhoneType = 2
										 ORDER BY ap.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantWorkNumber,
					 (SELECT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
						 FROM dbo_Applicants a 
						 LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
						 WHERE applicant.Id = a.Id 
							 AND aa.IsPreferred = 1
						 GROUP BY a.Id)                                  AS ApplicantPostCode,
						 ocOrder.Id                                          AS OrderId,
						 ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,    
						 ocOrder.OrderingCustomerId,
						 ocOrder.OrderingCustomerTeamId,
						 ocOrder.OrderingChannelId                           AS Origin,
						 ocOrder.CoverTypeId                                 AS TypeOfCover,
						 ocOrder.PolicyNumber                                AS PolicyNumber,
						 IF(oc.IsClientReference2Required = 1
							AND ocOrder.ClientReference2 != '',
							ocOrder.ClientReference2, NULL)					 AS ClientReference2,
						 (SELECT CASE WHEN NULLIF(AssignedToPrefix, '') IS NULL 
								 THEN AssignedTo 
								 ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo)
								 END
						 from dbo_OrderExamActivities OEA 
						 where OEA.OrderExamId = orderExam.Id 
						 order by CreateTime desc, ActivityOrder desc
						 LIMIT 1)                                            AS AssignedTo,
					 (SELECT CASE
						 WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 THEN 
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 WHEN examiner.ExaminerTypeId = 4 THEN
								 (SELECT GROUP_CONCAT( DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 ELSE '' 
					 END)                                                AS Skill,
					 (SELECT CASE
						 WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 
							 THEN 
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 WHEN examiner.ExaminerTypeId = 4 
							 THEN
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 ELSE '' 
					 END)                                                AS Qualification,
					 oc.CompanyName                                      AS OrderingCustomer,
					 oc.NextChargeReview                            	 AS NextChargeReview,
					 oc.NextChargeReviewComments                         AS NextChargeReviewComments,
					 oc.ContractStartDate                          		 AS ContractStartDate,
					 oc.AccountManager                          		 AS AccountManager,
					 oc.CQCRegulated                          		 	 AS CQCRegulated,
					 oc.IsActive                        		 		 AS OCStatus,
					 ocType.Name				                         AS OrderingCustomerType,
					 ocDomicile.Name				                     AS OrderingCustomerDomicile,
					 CONCAT(applicant.FirstName, ' ', applicant.LastName) AS ApplicantName,
					 applicant.DateOfBirth                               AS ApplicantDoB,
					 (SELECT a.`Name` FROM dbo_Genders a 
						 WHERE a.Id = applicant.GenderId)                AS ApplicantGender,
					 applicant.LastName                                  AS ApplicantLastName,
					 applicant.FirstName                                 AS ApplicantFirstName,
					 examType.`Name`                                     AS ExamType,
					 IFNULL(dbo_GetTimeServiceForActivity ((SELECT Id
						 FROM dbo_OrderExamActivities E
							WHERE E.OrderExamId = orderExam.`Id`
						 ORDER BY E.CreateTime DESC
						 LIMIT 1), NULL), 0)                             AS TimeService,
					 0 													 AS TotalTimeService,
					 (SELECT IF(orderExam.`StatusId` = 32 OR orderExam.`StatusId` = 64 OR orderExam.`StatusId` = 16384,
						 IFNULL((SELECT InactiveTimeService 
								 FROM grid_AllOrderExams gaoe
								 WHERE gaoe.Id = orderExam.`Id`), 0),
						 IFNULL(dbo_GetInactiveTimeServiceForExam (orderExam.`Id`), 0))
					 ) 													 AS InactiveTimeService,
					 li.`Name`                                           AS LifeIndicator
					 from dbo_OrderExams orderExam
					 INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId
					 INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId
					 INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id
					 INNER JOIN dbo_OcExamTypes examType ON orderExam.ExamTypeId = examType.Id
					 INNER JOIN dbo_OrderingCustomerTypes ocType ON oc.OrderingCustomerTypeId = ocType.Id
					 INNER JOIN dbo_Countries ocDomicile ON oc.DomicileId = ocDomicile.Id
					 LEFT JOIN dbo_Examiners examiner ON orderExam.AssignedExaminerId = examiner.Id
					 LEFT JOIN dbo_LifeIndicators li ON li.`Id` = ocOrder.`LifeIndicatorId`
					 WHERE ocOrder.IsAnonymised = 0;
			 
			 SET @@group_concat_max_len = 256000;
			 INSERT INTO dbo_OrderExamsReportsData 
			 (
				 ActivityDescriptionsAll,AddressLine1,AddressLine2,AddressLine3,ApplicantDoB,ApplicantEmail,ApplicantFirstName,
				 ApplicantGender,ApplicantLastName,ApplicantMobileNumber,ApplicantWorkNumber,ApplicantName,ApplicantPostCode,AppointmentScheduled, 
				 AssignedCSR,AssignedTo,AssignedToPrefix,BestContact,BilledDate,City,ClinicName,CreatorName,
				 CsrFirstName,CsrLastName,ExaminerFirstName,ExaminerLastName,NursePostCode,
				 ExaminerPrepaymentFlag,ExamOwnerId,ExamPostCode,ExamType,GpName,GpEmail,GPPracticePostCode,GroupPractice,GpPaymentType,GpAccountNo, GpSortCode,PayeeEmailAddress, HighNetWorth,Id,
				 InvoiceReferenceNumber,IsActive,IsUrgentCase,LatestActivityComment,
				 LatestActivityDescription,MiscFeeDescription,MiscChargeDescription,OcContact,OcServiceDescription,
				 OrderDate,OrderExamNumber,SnappNAD,FirstContactDateTime,FirstContactHPName,FirstContactType,PONumber,Complexity,ComplexityNote,FirstAppointmentOffered,OnHoldReason,OrderId,OrderingCustomer,OrderingCustomerType,OrderingCustomerDomicile, NextChargeReview, NextChargeReviewComments, ContractStartDate, AccountManager, CQCRegulated, OCStatus, OCLastCreatedExamDate, OrderingCustomerTeam,OrderingCustomerTeamMemberId,Origin,PayeeName,PolicyNumber,ClientReference2,PracticeName,Qualification,Requirement,
				 RequirementProblemReason,AllRequirementProblemReason,RequirementStatus,ICPOrderingCustomers,Skill,`Status`,TypeOfCover,CreateTime,ExamLocation, ExaminerEmail,GPPracticeFax,TimeService,TotalTimeService,InactiveTimeService,
				 LabFeeTotal,MeFeeTotal,MiscFeeTotal,MiscCharge, OcCharge, LifeIndicator, BankName, SortCode, AccountNumber
			 )
			 SELECT
					 temp.ActivityDescriptionsAll,
					 generalPractitioner.AddressLine1                    AS AddressLine1,
					 generalPractitioner.AddressLine2                    AS AddressLine2,
					 generalPractitioner.AddressLine3                    AS AddressLine3,    
					 temp.ApplicantDoB,  
					 temp.ApplicantEmail,
					 temp.ApplicantFirstName,
					 temp.ApplicantGender,
					 temp.ApplicantLastName,
					 temp.ApplicantMobileNumber,
					 temp.ApplicantWorkNumber,
					 temp.ApplicantName,
					 temp.ApplicantPostCode,
					 appointment.`Date`                                  AS AppointmentScheduled,
					 CONCAT(examOwner.FirstName, ' ', examOwner.LastName) AS AssignedCSR,
					 temp.AssignedTo,
					 (SELECT AssignedToPrefix 
						 from dbo_OrderExamActivities OEA 
						 where OEA.OrderExamId = orderExam.Id 
						 order by CreateTime desc, ActivityOrder desc
						 LIMIT 1)                                        AS AssignedToPrefix,
					 generalPractitioner.BestContact                     AS BestContact,
					 (SELECT a.CreateTime FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
						 WHERE a.OrderExamId = orderExam.Id AND a.ToExamState = 16
						 ORDER BY a.CreateTime
						 LIMIT 1)                                        AS BilledDate,
					 generalPractitioner.Town                            AS City,
					 clinic.`Name`                                       AS ClinicName,
					 (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
						 FROM dbo_OrderExamActivities OEA 
						 INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
						 WHERE OEA.PreviousActivityId IS null AND OEA.OrderExamId= orderExam.Id
						 ORDER BY OEA.CreateTime ASC 
						 LIMIT 1)                                        AS CreatorName,
					 examOwner.FirstName                                 AS CsrFirstName,
					 examOwner.LastName                                  AS CsrLastName,
					 medicalExaminer.FirstName                           AS ExaminerFirstName,
					 medicalExaminer.LastName                            AS ExaminerLastName,
					 (SELECT ExAddress.PostCode
						FROM dbo_ExaminerAddresses ExAddress
							WHERE ExAddress.ExaminerId = orderExam.AssignedExaminerId
							AND ExAddress.ExaminerAddressTypeId = 0
							AND orderExam.ExamTypeId = 1
							ORDER BY ExAddress.CreateTime DESC
							LIMIT 1)				 					 AS NursePostCode,
					 (SELECT CASE (SELECT COUNT(*) FROM dbo_OrderExamActivities a
										 INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
										 INNER JOIN dbo_OrderExams c ON a.OrderExamId = c.Id
										 WHERE c.Id = orderExam.Id AND c.ExamTypeId = 4 
											 AND b.`Code` IN('300'))
										 WHEN 0 
										 THEN 'No' 
										 ELSE 'Yes' 
										 END)                            AS ExaminerPrepaymentFlag,
					 examOwner.Id                                        AS ExamOwnerId,
					 appointment.PostCode                                AS ExamPostCode,
					 temp.ExamType,
					 TRIM(CONCAT(
					 generalPractitioner.Title, 
					 ' ', 
					 generalPractitioner.FirstName, 
					 ' ', 
					 generalPractitioner.LastName))                      AS GpName,
					 generalPractitioner.Email                           AS GpEmail,
					 generalPractitioner.PostCode                        AS GPPracticePostCode,
					 gpPracticeName.SurgeryName                          AS GroupPractice,                 
					 (CASE
							WHEN generalPractitioner.PaymentType  = 1 THEN "Bacs"
							WHEN generalPractitioner.PaymentType = 2 THEN "Cheque"
							ELSE "-"
						END)  AS GpPaymentType,
					 generalPractitioner.AccountNo                       AS GpAccountNo,
					 generalPractitioner.SortCode                        AS GpSortCode,
					 generalPractitioner.PayeeEmailAddress				 AS PayeeEmailAddress,
					 (Select IFNULL(
						 (SELECT 1 From dbo_OrderExamRequirements R 
							INNER JOIN dbo_MedicalExaminations ME
								ON R.MedicalExaminationId = ME.Id 
							WHERE orderExam.Id = R.OrderExamId 
							AND (ME.`Code` = 'HV FEE' OR ME.`Code` = 'UHV') 
							AND R.IsDeactivated = 0 LIMIT 1), 0) )       AS HighNetWorth,
					 orderExam.Id                                        AS Id,
					 generalPractitioner.InvoiceReferenceNumber          AS InvoiceReferenceNumber,
					 orderExam.IsActive                                  AS IsActive,
					 orderExam.IsUrgent                                  AS IsUrgentCase,
					 (SELECT E.Note 
						 FROM dbo_OrderExamActivities E 
						 WHERE E.OrderExamId=orderExam.Id 
						 ORDER BY E.CreateTime DESC, E.ActivityOrder desc
						 LIMIT 1)                                        AS LatestActivityComment,
					 (SELECT b.`Description` 
						 FROM dbo_OrderExamActivities E 
						 INNER JOIN dbo_Activities b ON E.ActivityId = b.Id 
						 WHERE E.OrderExamId=orderExam.Id 
						 ORDER BY E.CreateTime DESC, E.ActivityOrder desc
						 LIMIT 1)                                        AS LatestActivityDescription,
					 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
						 FROM dbo_MiscellaneousCharges a
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 2
						 ORDER BY a.Description
					 )                                                   AS MiscFeeDescription,
					 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
						 FROM dbo_MiscellaneousCharges a
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 1
						 ORDER BY a.Description
					 )                         							 AS MiscChargeDescription,
					 IFNULL(ocTeamMember.`Name`, '')                     AS OcContact,
					 (SELECT GROUP_CONCAT(DISTINCT E.`Description` ORDER BY REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(E.Description,'-',''), '(', ''), ')', ''), '/', ''), '''', '')  ASC separator ', ' )
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.StatusId NOT IN(3,4,5) 
							 AND E.IsDeactivated = 0
					 )                            						 AS OcServiceDescription,
					 orderExam.CreateTime                                AS OrderDate,
					 orderExam.ExamCustomId                              AS OrderExamNumber,
					 orderExam.SnappNAD                              	 AS SnappNAD,
					 orderExam.FirstContactDateTime                      AS FirstContactDateTime,
					 orderExam.FirstContactHPName                        AS FirstContactHPName,
					 orderExam.FirstContactType                          AS FirstContactType,
					 orderExam.PONumber                          		 AS PONumber,
					 orderExam.Complexity                          		 AS Complexity,
					 orderExam.ComplexityNote                          	 AS ComplexityNote,
					 orderExam.FirstAppointmentOffered                   AS FirstAppointmentOffered,
					 orderExam.OnHoldReason                   			 AS OnHoldReason,
					 temp.OrderId,
					 temp.OrderingCustomer,
					 temp.OrderingCustomerType,
					 temp.OrderingCustomerDomicile,
					 temp.NextChargeReview,
					 temp.NextChargeReviewComments,
					 temp.ContractStartDate,
					 temp.AccountManager,
					 temp.CQCRegulated,
					 temp.OCStatus,
					 NULL											     AS OCLastCreatedExamDate,
					 team.`Name`                                         AS OrderingCustomerTeam,
					 temp.OrderingCustomerTeamMemberId,                           
					 temp.Origin,
					 generalPractitioner.PayeeName                       AS PayeeName,
					 temp.PolicyNumber,
					 temp.ClientReference2,
					 generalPractitioner.PracticeName                    AS PracticeName,
					 temp.Qualification,
					 (SELECT GROUP_CONCAT(DISTINCT E.`Code` SEPARATOR ', ')
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.IsDeactivated = 0
						 GROUP BY E.OrderExamId)                         AS Requirement,
					 (SELECT GROUP_CONCAT(a.CustomDescription ORDER BY a.ActivityOrder ASC SEPARATOR '; ')
						 FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_OrderExamRequirementActivities oeda ON oeda.Id = a.Id
						 INNER JOIN dbo_OrderExamRequirements d ON oeda.ExamRequirementId = d.Id
						 WHERE a.OrderExamId = orderExam.Id
							 AND d.StatusId = 1 
							 AND d.IsDeactivated = 0
						 GROUP BY a.OrderExamId) 					 	 AS RequirementProblemReason,
					 (SELECT GROUP_CONCAT(a.CustomDescription ORDER BY a.ActivityOrder ASC SEPARATOR '; ')
						 FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_OrderExamRequirementActivities oeda ON oeda.Id = a.Id
						 INNER JOIN dbo_OrderExamRequirements d ON oeda.ExamRequirementId = d.Id
						 WHERE a.OrderExamId = orderExam.Id
							 AND (d.StatusId = 1 OR d.StatusId = 7) 
						 GROUP BY a.OrderExamId) 					 	 AS AllRequirementProblemReason,
					 (SELECT GROUP_CONCAT(DISTINCT E.StatusId SEPARATOR ', ')
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id
							 AND E.IsDeactivated = 0
						 GROUP BY E.OrderExamId)                         AS RequirementStatus,
					 (SELECT GROUP_CONCAT(oc_Inner.CompanyName ORDER BY oc_Inner.CompanyName ASC SEPARATOR '; ')
						 FROM dbo_OrderingCustomers oc_Inner
						 WHERE FIND_IN_SET(oc_Inner.Id,
						 orderExam.AppliedICPOrderingCustomersIds) > 0) AS ICPOrderingCustomers,
					 temp.Skill,
					 (SELECT a.`Name` FROM dbo_OrderExamStatusTypes a 
						 WHERE a.Id = orderExam.StatusId)                AS `Status`,
					 temp.TypeOfCover,
					 NOW()                                               AS CreateTime,
					 TRIM(CONCAT(
						 IFNULL(appointment.AddressLine1, ''),
						 IFNULL(CONCAT(appointment.AddressLine2, ', '), ''),
						 IFNULL(CONCAT(appointment.AddressLine3, ', '), ''),
						 IFNULL(CONCAT(appointment.City, ', '), ''),
						 IFNULL(CONCAT(appointment.PostCode, ', '), ''),
						 IFNULL(CONCAT(appointment.Country, ', '), '')           
					 ))                                                  AS ExamLocation,
					 medicalExaminer.PreferredEmail                      AS ExaminerEmail,
					 generalPractitioner.Fax                             AS GPPracticeFax,
					 temp.TimeService,
					 temp.TotalTimeService,
					 temp.InactiveTimeService,
					 0 AS LabFeeTotal,
					 0 AS MeFeeTotal,
					 0 AS MiscFeeTotal,
					 0 AS MiscCharge,
					 0 AS OcCharge,
					 temp.LifeIndicator,
					 dbo_Bank.Name 										 AS BankName,
					 EBA.SortCode 										 AS SortCode,
					 EBA.AccountNumber 									 AS AccountNumber
				 FROM dbo_OrderExams orderExam
					 inner join orderExamTempData temp on temp.orderExamId = orderExam.Id
					 LEFT JOIN dbo_OrderingCustomerTeams team ON temp.OrderingCustomerTeamId = team.Id
					 LEFT JOIN dbo_OrderExamAppointments appointment ON orderExam.Id = appointment.Id
					 LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_GeneralPractitioners generalPractitioner ON generalPractitioner.Id = orderExam.Id
					 LEFT JOIN dbo_GpPractices gpPracticeName ON gpPracticeName.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId
					 LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId
					 LEFT JOIN dbo_ExaminerBankAccount EBA ON orderExam.AssignedExaminerId = EBA.ExaminerId
					 LEFT JOIN dbo_Bank ON EBA.BankId = dbo_Bank.Id 
					 LEFT JOIN dbo_GridOrderingCustomerTeamMembers ocTeamMember ON temp.OrderingCustomerTeamMemberId = ocTeamMember.Id;
				 UPDATE dbo_OrderExamsReportsData
					  SET TotalTimeService = TimeService;			 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT  OrderExamId, SUM(IFNULL(A.ExaminerFee, 0)) SUMExaminerFee
					 FROM (
						 SELECT a.Id, a.OrderExamId, a.ExaminerFee
							 FROM dbo_OrderExamRequirements a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 LEFT JOIN dbo_Examiners c ON b.AssignedExaminerId = c.Id AND c.ExaminerTypeId != 5
						 INNER JOIN dbo_AllowedExaminers ae ON a.MedicalExaminationId = ae.MedicalExaminationId 
						 AND (SELECT COUNT(*)    FROM dbo_AllowedExaminers aa WHERE aa.ExaminerType = 5 AND aa.MedicalExaminationId = a.MedicalExaminationId AND a.OrderExamId=BB.id) = 0    
						 WHERE a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType != 5
								 AND a.OrderExamId=BB.id
						 GROUP BY a.Id, a.OrderExamId, a.ExaminerFee
					 ) A
					 GROUP BY OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MeFeeTotal = CC.SUMExaminerFee;
				 
				 
				 UPDATE dbo_OrderExamsReportsData
				 JOIN (
					 SELECT  OrderExamId, SUM(IFNULL(SS.ExaminerFee,0)) SUMExaminerFee
					 FROM (
						 SELECT DISTINCT a.Id, a.OrderExamId, a.ExaminerFee
							 FROM dbo_OrderExamRequirements a 
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_MedicalExaminations m ON a.MedicalExaminationId = m.Id 
						 INNER JOIN dbo_AllowedExaminers ae ON m.Id = ae.MedicalExaminationId
						 INNER JOIN dbo_ExaminationFees f ON f.MedicalExaminationId = m.Id
						 INNER JOIN dbo_LaboratoryExaminations e ON f.Id = e.Id
						 WHERE a.OrderExamId = BB.Id AND a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType = 5
						 ) SS
						 GROUP BY SS.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
					 SET LabFeeTotal=CC.SUMExaminerFee;
	 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
						 SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
							 FROM dbo_MiscellaneousCharges a 
							 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
							 INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
							 WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 2
							 GROUP BY a.OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MiscFeeTotal=CC.SUMCharge;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
						 SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
							 FROM dbo_MiscellaneousCharges a 
							 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
							 INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
							 WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 1
							 GROUP BY a.OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MiscCharge = CC.SUMCharge;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT SUM(IFNULL(a.OcCharge,0))  SUMOcCharge, a.OrderExamId 
						 FROM dbo_OrderExamRequirements a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId = BB.Id AND a.StatusId != 5 AND a.IsDeactivated = 0
						 GROUP BY a.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET OcCharge = CC.SUMOcCharge;
				 
				 
				 WITH cte AS ( SELECT NextActionDate, OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) AS rn FROM dbo_OrderExamActivities)
				 UPDATE dbo_OrderExamsReportsData oerd
				 JOIN cte ON cte.rn = 1 AND cte.OrderExamId = oerd.Id
				 SET oerd.NextActionDate = cte.NextActionDate;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT MIN(a.CreateTime) CreateTime, a.OrderExamId 
						 FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 WHERE a.OrderExamId = BB.Id AND a.ToExamState IN(32,64,16384)
						 GROUP BY a.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET CompletedDate = DATE_FORMAT(CC.CreateTime, "%Y-%m-%d %H:%i:%s");
				 
				 
				 WITH cte AS 
					 (SELECT CONCAT(b.Description, CASE WHEN a.Note IS NULL THEN '' ELSE CONCAT(': ', a.Note) END) topNote, 
					 a.OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) 
					 AS rn 
					 FROM dbo_OrderExamActivities a 
					 INNER JOIN dbo_Activities b  ON a.ActivityId = b.Id 
					 WHERE a.ToExamState = 32)
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN cte ON cte.rn=1 AND cte.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET CancellationReason = cte.topNote;
			 
				UPDATE dbo_OrderExamsReportsData doerd
				INNER JOIN dbo_OrderExams oe ON oe.Id = doerd.Id
				INNER JOIN dbo_Orders o ON oe.OrderId = o.Id
				INNER JOIN dbo_OrderingCustomers doc ON doc.Id = o.OrderingCustomerId
				INNER JOIN (
						SELECT MAX(oe.CreateTime) AS LatestExamCreateTime, o.OrderingCustomerId AS OrderingCustomerId
						FROM dbo_Orders o
						INNER JOIN dbo_OrderExams oe ON oe.OrderId = o.Id
						GROUP BY o.OrderingCustomerId
					) AS subquery
				SET doerd.OCLastCreatedExamDate = subquery.LatestExamCreateTime
					WHERE doc.Id = subquery.OrderingCustomerId;
				
				UPDATE dbo_OrderExamsReportsData doerd
				INNER JOIN dbo_OrderExams oe ON oe.Id = doerd.Id
				INNER JOIN dbo_OrderExamRequirements oer ON oer.OrderExamId = doerd.Id
				INNER JOIN dbo_MedicalExaminations me ON me.Id = oer.MedicalExaminationId
				LEFT JOIN dbo_FilesForInstance ffi ON ffi.InstanceId = doerd.Id
				LEFT JOIN dbo_Files files ON files.Id = ffi.FileId
				LEFT JOIN dbo_Documents documents ON documents.Id = files.Id
					SET 
						doerd.InvoiceStatus = oe.InvoiceStatusId,
						doerd.InvoiceType = oe.InvoiceTypeId,
						doerd.InvoicePaymentStatus = oe.InvoicePaymentStatusId,
						doerd.InvoiceApprovalStatus = oe.InvoiceDocumentStatus,
						doerd.InvoiceReceivedDate = (
							SELECT files.CreateTime FROM dbo_FilesForInstance ffi
							LEFT JOIN dbo_Files files ON files.Id = ffi.FileId
							LEFT JOIN dbo_Documents documents ON documents.Id = files.Id
								WHERE ffi.InstanceId = doerd.Id
									AND documents.DocumentTypeId = 'c0951fed-a691-4c38-95da-1b5fd9b8a202'
								ORDER BY CreateTime DESC
								LIMIT 1
						),
						doerd.InvoicePaidDate = oe.InvoicePaidDate,
						doerd.InvoiceAmount = oer.ExaminerFee
					WHERE (doerd.ExamType = 'Own GP Report' OR doerd.ExamType = 'Own GP Medical Screening')
						AND me.InvoiceProcessing = 1;
				
				UPDATE dbo_OrderExamsReportsData AS OERD
				JOIN (
					SELECT 
						a.OrderExamId,
						MAX(CASE WHEN a.Description = 'Mileage' THEN COALESCE(a.Charge, 0) END) AS MiscFeeMileage,
						MAX(CASE WHEN a.Description = 'Tolls' THEN COALESCE(a.Charge, 0) END) AS MiscFeeTolls,
						MAX(CASE WHEN a.Description = 'Postage' THEN COALESCE(a.Charge, 0) END) AS MiscFeePostage,
						MAX(CASE WHEN a.Description = 'Parking' THEN COALESCE(a.Charge, 0) END) AS MiscFeeParking,
						MAX(CASE WHEN a.Description = 'Enhancement' THEN COALESCE(a.Charge, 0) END) AS MiscFeeEnhancement,
						MAX(CASE WHEN a.Description = 'Ferry' THEN COALESCE(a.Charge, 0) END) AS MiscFeeFerry,
						MAX(CASE WHEN a.Description = 'ULEZ' THEN COALESCE(a.Charge, 0) END) AS MiscFeeULEZ,
						MAX(CASE WHEN a.Description NOT IN ('Mileage','Tolls','Postage','Parking','Enhancement','Ferry','ULEZ') THEN COALESCE(a.Charge, 0) END) AS MiscFeeOthers
					FROM dbo_MiscellaneousCharges a
					INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id = a.OrderExamId
					INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
					WHERE a.MiscellaneousChargeType = 2
					GROUP BY a.OrderExamId
				) CC ON CC.OrderExamId = OERD.Id
				SET 
					OERD.MiscFeeMileage = COALESCE(CC.MiscFeeMileage, 0),
					OERD.MiscFeeTolls = COALESCE(CC.MiscFeeTolls, 0),
					OERD.MiscFeePostage = COALESCE(CC.MiscFeePostage, 0),
					OERD.MiscFeeParking = COALESCE(CC.MiscFeeParking, 0),
					OERD.MiscFeeEnhancement = COALESCE(CC.MiscFeeEnhancement, 0),
					OERD.MiscFeeFerry = COALESCE(CC.MiscFeeFerry, 0),
					OERD.MiscFeeULEZ = COALESCE(CC.MiscFeeULEZ, 0),
					OERD.MiscFeeOthers = COALESCE(CC.MiscFeeOthers, 0);
				
				UPDATE dbo_OrderExamsReportsData AS OERD
				INNER JOIN dbo_OrderExams oe ON OERD.Id = oe.Id
				INNER JOIN dbo_Users usr ON oe.ExamAdvisorId = usr.Id
					SET OERD.Advisor = CONCAT(usr.FirstName, ' ', usr.LastName),
						OERD.Brokerage = usr.CompanyName
					WHERE oe.ExamAdvisorId IS NOT NULL AND oe.ExamAdvisorId != '';
					
			 IF `should_rollback` THEN
				 ROLLBACK;
			 ELSE
				 COMMIT;
			 END IF;
			 SET autocommit = 1;
		END $$
		
DELIMITER ;

CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridAllOrderExamsReports` AS
select
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ActivityCommentsAll` AS `ActivityCommentsAll`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ActivityDescriptionsAll` AS `ActivityDescriptionsAll`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AddressLine1` AS `AddressLine1`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AddressLine2` AS `AddressLine2`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AddressLine3` AS `AddressLine3`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantDoB` AS `ApplicantDoB`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantEmail` AS `ApplicantEmail`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantFirstName` AS `ApplicantFirstName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantGender` AS `ApplicantGender`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantLastName` AS `ApplicantLastName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantMobileNumber` AS `ApplicantMobileNumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantWorkNumber` AS `ApplicantWorkNumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantName` AS `ApplicantName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ApplicantPostCode` AS `ApplicantPostCode`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AppointmentScheduled` AS `AppointmentScheduled`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AssignedCSR` AS `AssignedCSR`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AssignedTo` AS `AssignedTo`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AssignedToPrefix` AS `AssignedToPrefix`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`BestContact` AS `BestContact`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`BilledDate` AS `BilledDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CancellationReason` AS `CancellationReason`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`City` AS `City`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ClinicName` AS `ClinicName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CompletedDate` AS `CompletedDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CreatorName` AS `CreatorName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CsrFirstName` AS `CsrFirstName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CsrLastName` AS `CsrLastName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExaminerFirstName` AS `ExaminerFirstName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExaminerLastName` AS `ExaminerLastName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExaminerPrepaymentFlag` AS `ExaminerPrepaymentFlag`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExamOwnerId` AS `ExamOwnerId`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExamPostCode` AS `ExamPostCode`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExamType` AS `ExamType`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GpName` AS `GpName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GPPracticePostCode` AS `GPPracticePostCode`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GroupPractice` AS `GroupPractice`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`HighNetWorth` AS `HighNetWorth`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Id` AS `Id`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoiceReferenceNumber` AS `InvoiceReferenceNumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`IsActive` AS `IsActive`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`IsUrgentCase` AS `IsUrgentCase`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`LabFeeTotal` AS `LabFeeTotal`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`LatestActivityComment` AS `LatestActivityComment`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`LatestActivityDescription` AS `LatestActivityDescription`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MeFeeTotal` AS `MeFeeTotal`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeDescription` AS `MiscFeeDescription`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeTotal` AS `MiscFeeTotal`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeMileage` AS `MiscFeeMileage`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeTolls` AS `MiscFeeTolls`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeePostage` AS `MiscFeePostage`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeParking` AS `MiscFeeParking`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeEnhancement` AS `MiscFeeEnhancement`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeFerry` AS `MiscFeeFerry`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeULEZ` AS `MiscFeeULEZ`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscFeeOthers` AS `MiscFeeOthers`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscCharge` AS `MiscCharge`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`MiscChargeDescription` AS `MiscChargeDescription`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`NextActionDate` AS `NextActionDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OcCharge` AS `OcCharge`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OcContact` AS `OcContact`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OcServiceDescription` AS `OcServiceDescription`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderDate` AS `OrderDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderExamNumber` AS `OrderExamNumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderId` AS `OrderId`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderingCustomer` AS `OrderingCustomer`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderingCustomerTeam` AS `OrderingCustomerTeam`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderingCustomerType` AS `OrderingCustomerType`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OrderingCustomerDomicile` AS `OrderingCustomerDomicile`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Origin` AS `Origin`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`PayeeName` AS `PayeeName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`PolicyNumber` AS `PolicyNumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ClientReference2` AS `ClientReference2`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`PracticeName` AS `PracticeName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Qualification` AS `Qualification`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Requirement` AS `Requirement`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`RequirementProblemReason` AS `RequirementProblemReason`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AllRequirementProblemReason` AS `AllRequirementProblemReason`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`RequirementStatus` AS `RequirementStatus`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Skill` AS `Skill`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Status` AS `Status`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`TimeService` AS `TimeService`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`TotalTimeService` AS `TotalTimeService`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InactiveTimeService` AS `InactiveTimeService`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`TypeOfCover` AS `TypeOfCover`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CreateTime` AS `CreateTime`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExaminerEmail` AS `ExaminerEmail`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ExamLocation` AS `ExamLocation`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GpEmail` AS `GpEmail`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GPPracticeFax` AS `GPPracticeFax`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`LifeIndicator` AS `LifeIndicator`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GpPaymentType` AS `GpPaymentType`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GpAccountNo` AS `GpAccountNo`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`GpSortCode` AS `GpSortCode`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`NextChargeReview` AS `NextChargeReview`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`NextChargeReviewComments` AS `NextChargeReviewComments`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`NursePostCode` AS `NursePostCode`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ContractStartDate` AS `ContractStartDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AccountManager` AS `AccountManager`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`BankName` AS `BankName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`PayeeEmailAddress` AS `PayeeEmailAddress`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`AccountNumber` AS `AccountNumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`SortCode` AS `SortCode`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`SnappNAD` AS `SnappNAD`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`FirstContactDateTime` AS `FirstContactDateTime`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`FirstContactHPName` AS `FirstContactHPName`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`FirstContactType` AS `FirstContactType`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`CQCRegulated` AS `CQCRegulated`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OCStatus` AS `OCStatus`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OCLastCreatedExamDate` AS `OCLastCreatedExamDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoiceStatus` AS `InvoiceStatus`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoiceType` AS `InvoiceType`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoicePaymentStatus` AS `InvoicePaymentStatus`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoiceApprovalStatus` AS `InvoiceApprovalStatus`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoiceReceivedDate` AS `InvoiceReceivedDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoicePaidDate` AS `InvoicePaidDate`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`InvoiceAmount` AS `InvoiceAmount`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ICPOrderingCustomers` AS `ICPOrderingCustomers`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`PONumber` AS `PONumber`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Complexity` AS `Complexity`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`ComplexityNote` AS `ComplexityNote`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`FirstAppointmentOffered` AS `FirstAppointmentOffered`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`OnHoldReason` AS `OnHoldReason`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Advisor` AS `Advisor`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`Brokerage` AS `Brokerage`
from
    `MssPlatformDb`.`dbo_OrderExamsReportsData`;
	
COMMIT;