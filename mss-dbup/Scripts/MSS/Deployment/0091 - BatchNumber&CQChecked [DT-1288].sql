START TRANSACTION;

	ALTER TABLE dbo_OrderExams ADD BatchNumber varchar(30) NULL;
	ALTER TABLE dbo_OrderExams ADD QCChecked tinyint(1) DEFAULT 0 NOT NULL;
	ALTER TABLE grid_AllOrderExams ADD BatchNumber varchar(30) NULL;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime) VALUES
		('fb2c285a-975a-4980-8590-b32abc8fc8a8','664','QC successfully checked',7,NULL,NULL,0,1,0,123082,3,0,NULL,0,0,1,0,NULL,'2023-11-20 16:00:00','2023-11-20 16:00:00');
	
		DROP TRIGGER afterUpdate_OrderExams;
	
DELIMITER //
	CREATE TRIGGER `afterUpdate_OrderExams` AFTER UPDATE ON `dbo_OrderExams` FOR EACH ROW BEGIN
	
	    UPDATE grid_AllOrderExams gAOE 
	
	    LEFT JOIN dbo_Users examOwner ON examOwner.Id = NEW.ExamOwnerId
	
	    LEFT JOIN dbo_MedicalExaminers examiner ON examiner.Id = NEW.AssignedExaminerId
	
	    SET 
	
	        gAOE.IsActive = NEW.IsActive,
	
	        gAOE.ExamType = NEW.ExamTypeId,
	
	        gAOE.IsUrgentCase = NEW.IsUrgent,
	
	        gAOE.`Status` = NEW.StatusId,
	
	        gAOE.ExamOwnerId = NEW.ExamOwnerId,
	
	        gAOE.CsrFirstName = examOwner.FirstName,
	
	        gAOE.CsrLastName = examOwner.LastName,
	
	        gAOE.AssignedCSR = CONCAT(examOwner.FirstName, ' ', examOwner.LastName),
	
	        gAOE.HighNetWorth = (SELECT IFNULL((SELECT 1 
	
	            FROM dbo_OrderExamRequirements R  
	
	            WHERE NEW.Id = R.OrderExamId 
	
	            AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
	
	            LIMIT 1), 0)),
	
	        gAOE.International = (SELECT IFNULL((SELECT 1 
	
	            FROM dbo_OrderExamRequirements R  
	
	            WHERE NEW.Id = R.OrderExamId 
	
	            AND R.`Code` = 'INT FEE' 
	
	            LIMIT 1), 0) ),
	
	        gAOE.HasLinkedOrders = (SELECT IFNULL((SELECT 1 
	
	            FROM dbo_LinkedOrders LO 
	
	            WHERE LO.OrderId = NEW.OrderId 
	
	            OR LO.ParentOrderId = NEW.OrderId
	
	            LIMIT 1), 0)),
	
	        gAOE.ExaminerFirstName = examiner.FirstName,
	
	        gAOE.ExaminerLastName = examiner.LastName,
	        
	        gAOE.InactiveTimeService = 
	        (
		        SELECT IF(NEW.StatusId = 32 OR NEW.StatusId = 64 OR NEW.StatusId = 16384,
		        	gAOE.InactiveTimeService,
		        	IFNULL(dbo_GetInactiveTimeServiceForExam (gAOE.Id), gAOE.InactiveTimeService))
	        ),
	        
	        gAOE.BatchNumber = NEW.BatchNumber
	
	    WHERE gAOE.Id = NEW.Id;        
	
	END//

DELIMITER ;

COMMIT;