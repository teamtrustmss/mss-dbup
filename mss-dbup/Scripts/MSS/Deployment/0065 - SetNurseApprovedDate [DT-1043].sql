START TRANSACTION;

	UPDATE dbo_MedicalExaminers dme
	INNER JOIN dbo_Examiners de ON dme.Id = de.Id
	INNER JOIN dbo_FeeGroupExaminers dfge ON dme.Id = dfge.Id
	SET dme.ApprovedDate = DATE(de.CreateTime)
		WHERE dfge.ExaminerStatusId = 0
		AND dme.ApprovedDate IS NULL
		AND de.ExaminerTypeId = 2;

COMMIT;