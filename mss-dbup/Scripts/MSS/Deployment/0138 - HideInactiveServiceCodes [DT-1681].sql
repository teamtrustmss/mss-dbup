START TRANSACTION;

	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridLaboratoryExaminations` AS
	select
		`e`.`Id` AS `Id`,
		`f`.`IsActive` AS `IsActive`,
		`m`.`IsActive` AS `MedicalExaminationIsActive`,
		`e`.`LaboratoryId` AS `LaboratoryId`,
		`e`.`LabExaminationCode` AS `LabExaminationCode`,
		`e`.`LabExaminationDesription` AS `LabExaminationDesription`,
		`e`.`LabMarkers` AS `LabMarkers`,
		`e`.`SLATurnAroundTime` AS `SLA`,
		`m`.`Id` AS `MSSId`,
		`m`.`Code` AS `MSSCode`,
		`m`.`Description` AS `MSSDescription`,
		`f`.`Fee` AS `Fee`,
		(
		select
			group_concat(distinct `s`.`Vacutainer` separator ', ')
		from
			(`MssPlatformDb`.`dbo_SampleRequirements` `s`
		left join `MssPlatformDb`.`dbo_LaboratoryExaminations_SampleRequirements` `x` on
			((`x`.`SampleRequirementId` = `s`.`Id`)))
		where
			(`x`.`LaboratoryExaminationId` = `e`.`Id`)
		group by
			`x`.`LaboratoryExaminationId`) AS `Vacutainers`
	from
		((`MssPlatformDb`.`dbo_LaboratoryExaminations` `e`
	join `MssPlatformDb`.`dbo_ExaminationFees` `f` on
		((`f`.`Id` = `e`.`Id`)))
	join `MssPlatformDb`.`dbo_MedicalExaminations` `m` on
		((`m`.`Id` = `f`.`MedicalExaminationId`)));

COMMIT;