start transaction;
select @id316 := Id FROM dbo_Activities WHERE Code = 316;
select @id343 := Id FROM dbo_Activities WHERE Code = 343;
select @id322 := Id FROM dbo_Activities WHERE Code = 322;
select @id342 := Id FROM dbo_Activities WHERE Code = 342;
INSERT INTO dbo_WebhookEvents (Id,Description,AlertStatus,IsActive,CreateTime,ModifyTime) VALUES
	 (@id316,'316: Surgery have confirmed they have received the instruction','N',1,Now(),Now());
	
INSERT INTO dbo_WebhookEvents (Id,Description,AlertStatus,IsActive,CreateTime,ModifyTime) VALUES
	 (@id343,'343: Surgery has confirmed that payment has been received','N',1,Now(),Now());
	
INSERT INTO dbo_WebhookEvents (Id,Description,AlertStatus,IsActive,CreateTime,ModifyTime) VALUES
	 (@id322,'322: Surgery waiting for the customer to see the report before they will return it to MSS','N',1,Now(),Now());	
	
INSERT INTO dbo_WebhookEvents (Id,Description,AlertStatus,IsActive,CreateTime,ModifyTime) VALUES
	 (@id342,'342: Practice has posted the report to MSS','N',1,Now(),Now());
commit;