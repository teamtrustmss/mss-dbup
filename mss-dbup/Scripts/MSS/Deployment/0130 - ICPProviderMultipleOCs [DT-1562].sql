START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomers ADD SupportsICP tinyint(1) DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD SupportsICP tinyint(1) DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_OrderExams ADD AppliedICPOrderingCustomersIds text NULL;
	ALTER TABLE grid_AllOrderExams ADD AppliedICPOrderingCustomersIds text NULL;
	
	DROP TRIGGER afterUpdate_OrderExams;
	DROP TRIGGER afterInsert_OrderExam;
	
DELIMITER $$ 
CREATE TRIGGER `afterUpdate_OrderExams` AFTER UPDATE ON `dbo_OrderExams` FOR EACH ROW BEGIN
    UPDATE grid_AllOrderExams gAOE 
    LEFT JOIN dbo_Users examOwner ON examOwner.Id = NEW.ExamOwnerId
    LEFT JOIN dbo_MedicalExaminers examiner ON examiner.Id = NEW.AssignedExaminerId
    SET 
        gAOE.IsActive = NEW.IsActive,
        gAOE.ExamType = NEW.ExamTypeId,
        gAOE.IsUrgentCase = NEW.IsUrgent,
        gAOE.`Status` = NEW.StatusId,
        gAOE.ExamOwnerId = NEW.ExamOwnerId,
        gAOE.CsrFirstName = examOwner.FirstName,
        gAOE.CsrLastName = examOwner.LastName,
        gAOE.AssignedCSR = CONCAT(examOwner.FirstName, ' ', examOwner.LastName),
        gAOE.HighNetWorth = EXISTS (
            SELECT 1 
            FROM dbo_OrderExamRequirements R
			INNER JOIN dbo_MedicalExaminations ME ON ME.Id = R.MedicalExaminationId
                WHERE NEW.Id = R.OrderExamId
                AND (ME.`Code` = 'HV FEE' OR ME.`Code` = 'UHV')
            LIMIT 1
        ),
        gAOE.International = EXISTS (
            SELECT 1
			FROM dbo_OrderExamRequirements R
			INNER JOIN dbo_MedicalExaminations ME ON ME.Id = R.MedicalExaminationId
                WHERE NEW.Id = R.OrderExamId
				AND ME.`Code` = 'INT FEE' 
            LIMIT 1
        ),
        gAOE.HasLinkedOrders = EXISTS (
            SELECT 1 
            FROM dbo_LinkedOrders LO 
				WHERE LO.OrderId = NEW.OrderId 
				OR LO.ParentOrderId = NEW.OrderId
            LIMIT 1
        ),
        gAOE.ExaminerFirstName = examiner.FirstName,
        gAOE.ExaminerLastName = examiner.LastName,
        gAOE.InactiveTimeService = 
        (
            SELECT IF(NEW.StatusId = 32 OR NEW.StatusId = 64 OR NEW.StatusId = 16384,
                gAOE.InactiveTimeService,
                IFNULL(dbo_GetInactiveTimeServiceForExam (gAOE.Id), gAOE.InactiveTimeService))
        ),
        gAOE.SelectedKitTypes = NEW.SelectedKitTypes,
		gAOE.AppliedICPOrderingCustomersIds = NEW.AppliedICPOrderingCustomersIds,
        gAOE.BatchNumber = NEW.BatchNumber,
        gAOE.ExamAdvisorId = NEW.ExamAdvisorId
    WHERE gAOE.Id = NEW.Id;
END$$
DELIMITER ;

DELIMITER $$ 
CREATE TRIGGER `afterInsert_OrderExam` AFTER INSERT ON `dbo_OrderExams` FOR EACH ROW BEGIN
    DECLARE v_AssignedToPrefix VARCHAR(5);
    DECLARE v_AssignedTo VARCHAR(508);
    DECLARE v_LatestActivityId VARCHAR(64);
    DECLARE v_NextActionDate DATETIME(6);
    SELECT 
        AssignedToPrefix,
        (CASE
            WHEN AssignedToPrefix IS NULL THEN AssignedTo
            ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo)
        END),
        Id,
        NextActionDate
    INTO 
        v_AssignedToPrefix,
        v_AssignedTo,
        v_LatestActivityId,
        v_NextActionDate
    FROM dbo_OrderExamActivities
		WHERE OrderExamId = NEW.Id
	ORDER BY ActivityOrder DESC
    LIMIT 1;
    INSERT INTO grid_AllOrderExams (Id, OrderId, IsActive, OrderExamNumber, ExamType, NextActionDate, NextActionDateSort, Origin,
		TypeOfCover, OrderingCustomerId, OrderingCustomer, ExamPostCode, ApplicantFirstName, ApplicantLastName, ApplicantDoB,
		PolicyNumber, ClientReference2, IsUrgentCase, IsTouched, OrderDate, `Status`, AppointmentScheduled, ExaminerFirstName,
		ExaminerLastName, ExamOwnerId, CsrFirstName, CsrLastName, GpName, ClinicName, IsAnonymised, HighNetWorth, International,
		HasLinkedOrders, AssignedToPrefix, AssignedTo, Applicant, AssignedCSR, LatestActivityId, TimeService, InactiveTimeService,
		OrderingCustomerTeamMemberId, OrderingCustomerTeamId, ApplicantPostCode, SelectedKitTypes, AppliedICPOrderingCustomersIds, ExamAdvisorId)
    SELECT
        orderExam.Id 																		AS Id,
        ocOrder.Id 																			AS OrderId,
        orderExam.IsActive 																	AS IsActive,
        orderExam.ExamCustomId 																AS OrderExamNumber,
        orderExam.ExamTypeId 																AS ExamType,
        v_NextActionDate 																	AS NextActionDate,
        IFNULL(v_NextActionDate, '9999-12-31') 												AS NextActionDateSort,
        ocOrder.OrderingChannelId 															AS Origin,
        ocOrder.CoverTypeId																	AS TypeOfCover,
        oc.Id 																				AS OrderingCustomerId,
        oc.CompanyName 																		AS OrderingCustomer,
        appointment.PostCode 																AS ExamPostCode,
        applicant.FirstName 																AS ApplicantFirstName,
        applicant.LastName 																	AS ApplicantLastName,
        applicant.DateOfBirth 																AS ApplicantDoB,
        ocOrder.PolicyNumber 																AS PolicyNumber,
        ocOrder.ClientReference2 															AS ClientReference2,
        orderExam.IsUrgent 																	AS IsUrgentCase,
        0 																					AS IsTouched,
        orderExam.CreateTime 																AS OrderDate,
        orderExam.StatusId 																	AS `Status`,
        appointment.`Date` 																	AS AppointmentScheduled,
        medicalExaminer.FirstName 															AS ExaminerFirstName,
        medicalExaminer.LastName 															AS ExaminerLastName,
        orderExam.ExamOwnerId 																AS ExamOwnerId,
        examOwner.FirstName 																AS CsrFirstName,
        examOwner.LastName 																	AS CsrLastName,
		NULL																				AS GpName,
		NULL																				AS ClinicName,
        ocOrder.IsAnonymised 																AS IsAnonymised,
        EXISTS (
            SELECT 1
            FROM dbo_OrderExamRequirements R
            INNER JOIN dbo_MedicalExaminations ME ON ME.Id = R.MedicalExaminationId
				WHERE orderExam.Id = R.OrderExamId
				AND (ME.`Code` = 'HV FEE' OR ME.`Code` = 'UHV')
            LIMIT 1
        ) 																					AS HighNetWorth,
        EXISTS (
            SELECT 1
            FROM dbo_OrderExamRequirements R
            INNER JOIN dbo_MedicalExaminations ME ON ME.Id = R.MedicalExaminationId
				WHERE orderExam.Id = R.OrderExamId
				AND ME.`Code` = 'INT FEE'
            LIMIT 1
        ) 																					AS International,
        EXISTS (
            SELECT 1
            FROM dbo_LinkedOrders LO
				WHERE LO.OrderId = orderExam.OrderId
				OR LO.ParentOrderId = orderExam.OrderId
            LIMIT 1
        ) 																					AS HasLinkedOrders,
        v_AssignedToPrefix 																	AS AssignedToPrefix,
        v_AssignedTo 																		AS AssignedTo,
        CONCAT(
			applicant.FirstName,
			' ', applicant.LastName
		) 																					AS Applicant,
        CONCAT(
			examOwner.FirstName,
			' ',
			examOwner.LastName
		) 																					AS AssignedCSR,
        v_LatestActivityId 																	AS LatestActivityId,
        IFNULL(dbo_GetTimeServiceForActivity(v_LatestActivityId, NULL), 0) AS TimeService,
        IFNULL(dbo_GetInactiveTimeServiceForExam(orderExam.Id), 0) AS InactiveTimeService,
        ocOrder.OrderingCustomerTeamMemberId 												AS OrderingCustomerTeamMemberId,
        IFNULL(ocOrder.OrderingCustomerTeamId,
			'00000000-0000-0000-0000-000000000000') 										AS OrderingCustomerTeamId,
        (SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
            FROM dbo_Applicants a
            LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
            WHERE applicant.Id = a.Id
            GROUP BY a.Id, aa.ApplicantId)													AS ApplicantPostCode,
        orderExam.SelectedKitTypes 															AS SelectedKitTypes,
        orderExam.AppliedICPOrderingCustomersIds 											AS AppliedICPOrderingCustomersIds,
        orderExam.ExamAdvisorId 															AS ExamAdvisorId
    FROM dbo_OrderExams orderExam
    INNER JOIN dbo_Orders ocOrder 					ON ocOrder.Id = orderExam.OrderId
    INNER JOIN dbo_OrderingCustomers oc 			ON oc.Id = ocOrder.OrderingCustomerId
    INNER JOIN dbo_Applicants applicant 			ON applicant.Id = ocOrder.Id
    LEFT JOIN dbo_OrderExamAppointments appointment ON appointment.Id = orderExam.Id
    LEFT JOIN dbo_MedicalExaminers medicalExaminer 	ON medicalExaminer.Id = orderExam.AssignedExaminerId
    LEFT JOIN dbo_Users examOwner 					ON examOwner.Id = orderExam.ExamOwnerId
		WHERE orderExam.Id = NEW.Id;
END$$
DELIMITER ;

COMMIT;