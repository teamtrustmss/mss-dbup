START TRANSACTION;

DROP TRIGGER IF EXISTS MssPlatformDb.beforeInsert_OrderExamActivities;
USE MssPlatformDb;

DELIMITER $$
$$
CREATE TRIGGER `beforeInsert_OrderExamActivities` BEFORE INSERT ON `dbo_OrderExamActivities` FOR EACH ROW
BEGIN
    DECLARE _activityOrder INT;
    DECLARE _error INT DEFAULT 1;
    DECLARE _exists1 Int DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET _error = 1;
    
    SELECT MAX(ActivityOrder) INTO _activityOrder
    FROM dbo_OrderExamActivities 
    WHERE OrderExamId = NEW.OrderExamId;
    
    IF _activityOrder IS NOT NULL AND NEW.ActivityOrder <= _activityOrder THEN
        
     SET _activityOrder = 1;

     SELECT count(*) into _exists1 FROM dbo_OrderExamActivityUniqueNumbers WHERE OrderExamId = NEW.OrderExamId;
    
     IF _exists1 <> 0 THEN
         SELECT (MAX(ActivityOrder) + 1) into _activityOrder FROM dbo_OrderExamActivityUniqueNumbers WHERE OrderExamId = NEW.OrderExamId;   
     END IF;
    
     WHILE (_error <> 0) DO
		SET _error = 0;
		SET @newid := (SELECT UUID());
        INSERT INTO dbo_OrderExamActivityUniqueNumbers(Id, OrderExamId, ActivityOrder, CreateTime, ModifyTime)
        VALUES(@newid, NEW.OrderExamId, _activityOrder, NOW(), NOW());
			
		IF _error = 1 THEN
		 SET _activityOrder = _activityOrder + 1;
		END IF;
     END WHILE;
    
    SET NEW.ActivityOrder = _activityOrder;
    END IF;
END$$    

DELIMITER ;

COMMIT;