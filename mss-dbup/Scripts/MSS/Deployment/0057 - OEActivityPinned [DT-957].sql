START TRANSACTION;

	ALTER TABLE MssPlatformDb.dbo_OrderExamActivities ADD Pinned tinyint(1) DEFAULT 0 NOT NULL;

	CREATE OR REPLACE
	ALGORITHM = MERGE VIEW `dbo_GridExamStatusActivities` AS
	SELECT
		`ea`.`Id` AS `Id`,
		`ea`.`OrderExamId` AS `OrderExamId`,
		`ea`.`CreatorId` AS `CreatorId`,
		`ea`.`PreviousActivityId` AS `PreviousActivityId`,
		`ea`.`CustomDescription` AS `Description`,
		`ea`.`FromExamState` AS `FromStatus`,
		`ea`.`ToExamState` AS `Status`,
		`oes`.`Name` AS `StatusDisplay`,
		`a`.`Code` AS `Code`,
		`a`.`VisibleToOrderingCustomer` AS `VisibleToOrderingCustomer`,
		`a`.`VisibleToExaminer` AS `VisibleToExaminer`,
		`ea`.`Note` AS `Note`,
		`ea`.`Pinned` AS `Pinned`,
		`ea`.`IsActive` AS `IsActive`,
		`ea`.`CreateTime` AS `ActivityDateTime`,
		`ea`.`CreateTime` AS `CreateTime`,
		`ea`.`ModifyTime` AS `ModifyTime`,
		(
		SELECT
			`dbo_GetTimeServiceForActivity`(`ea`.`Id`,
			NULL)) AS `TS`,
		`ea`.`AssignedTo` AS `AssignedTo`,
		`ea`.`AssignedToPrefix` AS `AssignedToPrefix`,
		`oc`.`CompanyName` AS `OrderingCustomer`,
		CONCAT(`uChangedBy`.`FirstName`, ' ', `uChangedBy`.`LastName`) AS `ChangedBy`,
		`ea`.`NextActionDate` AS `NextActionDate`,
		(CASE
			WHEN (`ocTM`.`Id` IS NULL) THEN 0
			ELSE 1
		END) AS `IsChangedByOcUser`,
		`ea`.`NotificationType` AS `NotificationType`,
		`a`.`NotificationType` AS `ActivityNotificationType`,
		`ea`.`ActAssignedToId` AS `ActAssignedToId`,
		`ea`.`ActivityOrder` AS `ActivityOrder`
	FROM
		(((((((`MssPlatformDb`.`dbo_OrderExamActivities` `ea`
	JOIN `MssPlatformDb`.`dbo_Activities` `a` ON
		((`ea`.`ActivityId` = `a`.`Id`)))
	JOIN `MssPlatformDb`.`dbo_OrderExams` `e` ON
		((`ea`.`OrderExamId` = `e`.`Id`)))
	JOIN `MssPlatformDb`.`dbo_Orders` `o` ON
		((`e`.`OrderId` = `o`.`Id`)))
	JOIN `MssPlatformDb`.`dbo_OrderingCustomers` `oc` ON
		((`o`.`OrderingCustomerId` = `oc`.`Id`)))
	JOIN `MssPlatformDb`.`dbo_Users` `uChangedBy` ON
		((`ea`.`CreatorId` = `uChangedBy`.`Id`)))
	LEFT JOIN `MssPlatformDb`.`dbo_OrderingCustomerTeamMembers` `ocTM` ON
		((`uChangedBy`.`Id` = `ocTM`.`Id`)))
	LEFT JOIN `MssPlatformDb`.`dbo_OrderExamStatusTypes` `oes` ON
		((`ea`.`ToExamState` = `oes`.`Id`)))
	WHERE
		(`ea`.`IsActive` = 1);
		
COMMIT;