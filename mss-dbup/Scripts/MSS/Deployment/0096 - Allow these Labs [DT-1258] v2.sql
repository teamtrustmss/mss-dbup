START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomers ADD AllowedLaboratoriesIds text NULL;
	UPDATE dbo_OrderingCustomers SET AllowedLaboratoriesIds = (SELECT Id FROM dbo_Laboratories WHERE LaboratoryName = 'Inuvi Diagnostics' LIMIT 1);

COMMIT;