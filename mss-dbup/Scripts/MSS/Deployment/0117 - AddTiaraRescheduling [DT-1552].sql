START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('309876af-beda-4abe-bb97-963b172a701e','11010','Tele-interview appointment has been rescheduled in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('8109a78d-3613-4f66-8cd0-030501fc82d3','11011','The appointment was cancelled due to the rescheduling',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);
		
COMMIT;