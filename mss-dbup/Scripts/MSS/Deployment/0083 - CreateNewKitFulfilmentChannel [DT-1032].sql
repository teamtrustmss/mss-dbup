START TRANSACTION;

	INSERT INTO dbo_OcExamTypes (Id,Name,Code,SortOrder) VALUES
	 (5,'Kit Fulfilment','F',50);
	 
	INSERT INTO dbo_ExaminerTypes (Id,Name,SortOrder) VALUES
	 (6,'Kit Fulfilment',60);
	 
	ALTER TABLE dbo_OrderExams ADD SampleToLabId varchar(64) NULL;
	
	ALTER TABLE dbo_MedicalExaminations ADD IsRequiresEssentialDocument tinyint(1) DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_OrderExams ADD IsEssentialDocumentLoaded tinyint(1) DEFAULT 0 NOT NULL;
	
	ALTER TABLE `dbo_OrderExams`
		ADD KEY `dbo_OrderExams_SampleToLabId` (`SampleToLabId`) USING BTREE;

	ALTER TABLE `dbo_OrderExams`
		ADD CONSTRAINT `FK_OrderExams_Laboratories` FOREIGN KEY (`SampleToLabId`) REFERENCES `dbo_Laboratories` (`Id`);

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime) VALUES
		('77db7c6c-7171-431d-bcd8-162421be3e0b','660','Essential Document Uploaded',7,NULL,NULL,1,1,0,48015,3,0,NULL,0,0,1,0,NULL,'2023-09-28 13:19:22','2023-09-28 13:19:22'),
		('daa697a3-060a-4a8d-8649-c411cc00bba8','661','Kit Sent',7,NULL,2,0,1,0,65536,1,0,NULL,0,0,1,0,NULL,'2016-04-01 07:58:52.540000000','2023-10-03 10:47:01.258237000');

	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
	 ('19a6bd7f-de8d-4895-932f-57ea8f37b30c',131,'Essential Document','2023-09-28 14:25:42','2023-09-28 14:25:42',1),
	 ('f94c568a-6b2b-49dc-a1d8-44cbca9620a3',132,'Royal Mail Label','2023-10-03 11:32:15','2023-10-03 11:32:15',1),
	 ('28f5c394-8e2d-4b06-80ee-6cf2efacfa98',133,'Lab Return Form','2023-10-03 13:01:27','2023-10-04 13:01:27',1);

	INSERT INTO dbo_OrderExamStatusTypes (Id,Name,Code,SortOrder) VALUES
	 (65536,'Kit Sent','KS',170);
	 
	ALTER TABLE dbo_KitTypes MODIFY COLUMN Weight decimal(18,2) DEFAULT 100.00 NOT NULL;
	
	INSERT INTO dbo_ApplicationSettings (`Key`,ContainerName,Value,Comment) VALUES
	 ('RoyalMailAPI','Presentation.MssPortal','https://api.parcel.royalmail.com/api/v1/Orders','Royal Mail API uri'),
	 ('RoyalMailBearerToken','Presentation.MssPortal','44a1c59f-654f-4954-b26a-96ea48992784','Royal Mail Bearer token');

	 
COMMIT;