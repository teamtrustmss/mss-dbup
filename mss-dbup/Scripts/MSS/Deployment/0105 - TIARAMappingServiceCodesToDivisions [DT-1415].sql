START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerDivisions ADD Code varchar(255) NULL;
	
	CREATE TABLE `dbo_OrderingCustomerDivisionsExaminations` (
	  `OrderingCustomerExaminationId` varchar(64) NOT NULL,
	  `OrderingCustomerDivisionId` varchar(64) NOT NULL,
	  PRIMARY KEY (`OrderingCustomerExaminationId`, `OrderingCustomerDivisionId`),
	  CONSTRAINT `FK_OrderingCustomerExamination_Division_Examination` FOREIGN KEY (`OrderingCustomerExaminationId`) REFERENCES `dbo_OrderingCustomerExaminations` (`Id`),
	  CONSTRAINT `FK_OrderingCustomerExamination_Division_Division` FOREIGN KEY (`OrderingCustomerDivisionId`) REFERENCES `dbo_OrderingCustomerDivisions` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

COMMIT;