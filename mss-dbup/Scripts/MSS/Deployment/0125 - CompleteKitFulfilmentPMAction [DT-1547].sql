START TRANSACTION;

	INSERT INTO dbo_ApplicationSettings (`Key`,ContainerName,Value,Comment) VALUES
		('CompleteKitFulfilmentExamTimeSpan','Presentation.MssPortal','90','Number of days after a Kit Fulfillment exam from "Kit Sent" status changes to "Completed" status');

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('b4bb457c-9c81-4ccc-bdd1-c073186f9b7f','759','Status updated to Completed due to {0} days inactivity after the kit was sent',7,NULL,NULL,0,1,0,32768,2,0,NULL,0,0,1,0,NULL,'2024-05-28 10:51:16.900000','2024-05-28 10:51:16.900000',0);
	 
COMMIT;