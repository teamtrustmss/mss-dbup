START TRANSACTION;

	ALTER TABLE dbo_Documents ADD UploadedManually tinyint(1) DEFAULT NULL;
	
	CREATE TABLE dbo_OrderExamInvoiceStatusTypes (
		Id int NOT NULL,
		Name varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
		Code varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
		SortOrder int DEFAULT 0 NOT NULL,
		CONSTRAINT `PRIMARY` PRIMARY KEY (Id)
	)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8mb4
	COLLATE=utf8mb4_0900_ai_ci
	COMMENT='';

	INSERT INTO dbo_OrderExamInvoiceStatusTypes (Id,Name,Code,SortOrder) VALUES
	 (0,'Awaiting invoice','AI',0),
	 (1,'Invoice received','IR',10),
	 (2,'Invoice received (NIR)','IRNIR',20),
	 (3,'No invoice received','NIR',30);
	
	
	CREATE TABLE dbo_OrderExamInvoiceTypes (
		Id int NOT NULL,
		Name varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
		Code varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
		SortOrder int DEFAULT 0 NOT NULL,
		CONSTRAINT `PRIMARY` PRIMARY KEY (Id)
	)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8mb4
	COLLATE=utf8mb4_0900_ai_ci
	COMMENT='';
	
	INSERT INTO dbo_OrderExamInvoiceTypes (Id,Name,Code,SortOrder) VALUES
	 (0,'On completion','C',0),
	 (1,'Advanced','A',10);
	
	
	CREATE TABLE dbo_OrderExamInvoicePaymentStatuses (
		Id int NOT NULL,
		Name varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
		Code varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
		SortOrder int DEFAULT 0 NOT NULL,
		CONSTRAINT `PRIMARY` PRIMARY KEY (Id)
	)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8mb4
	COLLATE=utf8mb4_0900_ai_ci
	COMMENT='';
	
	INSERT INTO dbo_OrderExamInvoicePaymentStatuses (Id,Name,Code,SortOrder) VALUES
	 (0,'Not paid','NP',0),
	 (1,'Paid','C',10);
	 
	ALTER TABLE dbo_OrderExams
		ADD InvoiceStatusId 		int 			NOT NULL,
		ADD InvoiceTypeId 			int 			NOT NULL,
		ADD InvoicePaymentStatusId 	int 			NOT NULL,
		ADD InvoicePaidDate 		datetime(6) 	NULL,
		ADD InvoiceDocumentStatus 	varchar(100) 	NULL;

	 ALTER TABLE dbo_OrderExams
		ADD KEY `dbo_OrderExams_InvoiceStatusId` 		(InvoiceStatusId) 			USING BTREE,
		ADD KEY `dbo_OrderExams_InvoiceTypeId` 			(InvoiceTypeId) 			USING BTREE,
		ADD KEY `dbo_OrderExams_InvoiceDocumentStatus`  (InvoiceDocumentStatus) 	USING BTREE,
		ADD KEY `dbo_OrderExams_InvoicePaymentStatusId` (InvoicePaymentStatusId) 	USING BTREE;

	CREATE INDEX dbo_MedicalExaminations_InvoiceProcessing_IDX USING BTREE ON dbo_MedicalExaminations (InvoiceProcessing);
	CREATE INDEX dbo_OrderExams_ExamCustomId_IDX USING BTREE ON dbo_OrderExams (ExamCustomId);

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,
		VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,
		SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime) VALUES
		('5193d9f8-63c2-ed11-8178-d8f883b919a4','913','Invoice approval status manually set to "Approved"',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('f7f378ee-3a05-4686-8d58-d8f29d0e5f58','903','Invoice auto uploaded [details of invoice uploaded].',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('679e033c-e6cc-4a17-b7cf-0a6d6b375066','904','Invoice manually uploaded [details of invoice uploaded].',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('d7047b2a-79fc-4037-972e-54a2b4da4976','910','Invoice status set to "Invoice received (NIR)".',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('cfbc8024-03bb-4eae-b77a-fe451d4d0d00','908','Invoice status set to "Invoice received".',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('64ecb735-850a-413f-a942-10bb43a8db56','902','A new invoice has replaced an existing invoice. The new invoice details are [details of invoice uploaded].',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('31ac6664-416b-4701-ba23-8a5736558e80','917','An invoice upload was attempted when an invoice is already present. The newer invoice has been discarded.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('effa2020-d564-4ffa-aaa4-97eb1c0cb8fc','920','Invoice uploaded with an OWN GP Exam service code present on the exam.  A copy of the invoice will be sent to the insurance medicals team and automated invoice processing will not continue.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('6858d61c-2607-4760-bc0c-d17dc25b16b9','921','Invoice uploaded with an INT FEE service code present on the exam.  A copy of the invoice will be sent to the international team and automated invoice processing will not continue.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('c2941c4a-61cf-4a81-a2ae-cdec2de11507','919','No invoice processing service codes present on exam.  Invoice processing will not continue.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('d1702ade-3187-4eed-b1f5-9a116ce641cb','914','Invoice awaiting approval.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('d82284e5-ac51-434a-b8b2-a896e07bbba3','901','GP details have been changed manually - details require verification.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('3d6f5360-1daf-439e-88c7-123230efe69e','907','Invoice payment type set to "On completion".',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('56faf477-3732-4e10-b786-f5040872e903','922','Invoice rejected.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('5049bd77-96ed-40ab-8f80-ec2f5d9e239a','923','Invoice approval status changed to awaiting approval.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('c0f2e1b1-e95e-4033-a831-f928d08424e8','909','Invoice status set to "Awaiting invoice".',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('7be233ff-c1c5-4178-b015-00c76fae1b0e','912','Invoice auto approved within DAV limits.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-14 14:30:16.090284000','2023-03-14 14:30:16.090284000'),
		('3ee932e5-7b6c-4ecb-bd31-081995cfc8d7','905','The invoice has been uploaded with incomplete information. Please check.',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,2,NULL,'2023-03-22 10:30:16.090284000','2023-03-22 10:30:16.090284000'),
		('5e854585-59ac-4874-9e22-5253a4120407','911','Invoice status set to "No invoice received"',6,NULL,NULL,0,1,0,24644,2,0,NULL,0,0,1,0,NULL,'2023-03-23 10:30:16.090284000','2023-03-22 10:30:16.090284000'),
		('9a5b32f7-12a7-4017-91ac-cca4b2779b5e','916','Remittance generated at [dateTime]',6,NULL,NULL,0,1,0,4,1,0,NULL,0,0,1,0,NULL,'2023-03-29 11:07:47.487000000','2023-03-29 11:07:47.487000000'),
		('edae7bbb-4c9d-4ca6-b7bb-1dca3413001b','915','Invoice payment status set to "Paid"',6,NULL,NULL,0,1,0,4,1,0,NULL,0,0,1,0,NULL,'2023-03-29 11:07:47.487000000','2023-03-29 11:07:47.487000000');
		
	UPDATE dbo_Activities SET Description = 'Invoice type set to "Advanced"' WHERE Code = '312';

	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		('2c36637b-abe3-4819-be05-861d2e72f4f7',807,'OWN GP Medical E-Mail','2023-03-24 10:30:16.090284000','2023-03-24 10:30:16.090284000',1),
		('c2e9634a-c7e4-4a06-a24e-b26b71dd7ab5',808,'International Medical E-Mail','2023-03-24 10:30:16.090284000','2023-03-24 10:30:16.090284000',1),
		('fddf881a-74db-49c1-9534-6bef13121b5e',809,'PDF Remittance','2023-03-29 11:07:47.487000000','2023-03-29 11:07:47.487000000',1);
		
	INSERT INTO dbo_ApplicationSettings (`Key`,ContainerName,Value,Comment) VALUES
		 ('OwnGPMedicalToInsuranceMedicalsEmailAddress','Presentation.MssPortal','InsuranceMedicals@inuvi.co.uk','OWN GP Medical E-Mail to InsuranceMedicals@inuvi.co.uk'),
		 ('InternationalMedicalToInternationalEmailAddress','Presentation.MssPortal','international@inuvi.co.uk','International Medical E-Mail to international@inuvi.co.uk'),
		 ('OwnGPMedicalToInsuranceMedicalsEmailAddress','Distribution.MSS.API','InsuranceMedicals@inuvi.co.uk','OWN GP Medical E-Mail to InsuranceMedicals@inuvi.co.uk'),
		 ('InternationalMedicalToInternationalEmailAddress','Distribution.MSS.API','international@inuvi.co.uk','International Medical E-Mail to international@inuvi.co.uk');
		
COMMIT;