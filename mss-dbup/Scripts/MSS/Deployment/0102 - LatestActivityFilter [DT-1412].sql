START TRANSACTION;

	ALTER TABLE grid_AllOrderExams ADD LatestActivityCode varchar(50) NULL;
	
	DROP TRIGGER afterInsert_OrderExamActivities;
	
DELIMITER //
	CREATE TRIGGER `afterInsert_OrderExamActivities` AFTER INSERT ON `dbo_OrderExamActivities` FOR EACH ROW BEGIN 
		UPDATE grid_AllOrderExams SET 
			LatestActivityId = NEW.Id,
			LatestActivityCode = IFNULL((SELECT Code FROM dbo_Activities WHERE Id = NEW.ActivityId LIMIT 1), NULL),
			LatestActivityOrder = NEW.ActivityOrder,
			TimeService = IFNULL((SELECT dbo_GetTimeServiceForActivity(NEW.Id, NULL)), 0),
			NextActionDate = NEW.NextActionDate, 
			NextActionDateSort = IFNULL(NEW.NextActionDate, '9999-12-31'),
			AssignedToPrefix = NEW.AssignedToPrefix,
			AssignedTo = (CASE WHEN NEW.AssignedToPrefix IS NULL 
				THEN NEW.AssignedTo 
				ELSE CONCAT(NEW.AssignedToPrefix, ' - ', NEW.AssignedTo) 
				END),
			CreatorName = COALESCE(CreatorName, 
				(SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
				FROM dbo_Users creator 
				WHERE NEW.CreatorId = creator.Id
				AND NEW.PreviousActivityId IS NULL
				LIMIT 1))
		WHERE Id = NEW.OrderExamId AND (NEW.ActivityOrder > LatestActivityOrder OR LatestActivityOrder IS NULL);

		UPDATE dbo_MedicalExaminers
			   INNER JOIN (
					SELECT doe.AssignedExaminerId, MAX(`doea`.`CreateTime`) AS CreateTime
					FROM `MssPlatformDb`.`dbo_OrderExamActivities` `doea`
					LEFT JOIN `MssPlatformDb`.`dbo_OrderExams` `doe` ON (`doea`.`OrderExamId` = `doe`.`Id`)
					LEFT JOIN `MssPlatformDb`.`dbo_Activities` `da` ON (`da`.`Id` = `doea`.`ActivityId`)
						WHERE doea.Id = NEW.Id
						AND `doe`.`ExamTypeId` = 1
						AND doe.AssignedExaminerId IS NOT NULL
						AND `da`.`Code` IN ('610', '611')
						GROUP BY doe.AssignedExaminerId
			   ) AS subquery ON subquery.AssignedExaminerId = dbo_MedicalExaminers.Id
			   SET dbo_MedicalExaminers.LastCompletedDate = DATE(subquery.CreateTime);
	END//

DELIMITER ;

COMMIT;