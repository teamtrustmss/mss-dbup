START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomers ADD MinApplicantAge int DEFAULT 18 NOT NULL;
	
	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
	 ('78ae5286-4cb5-ee11-9b0e-489ebd28a7c6','779','Please note that the applicant is underage.',1,NULL,NULL,1,1,0,16448,2,0,NULL,0,0,1,2,NULL,'2024-01-17 17:24:38.077157','2024-01-17 17:24:38.077157',0);

COMMIT;