START TRANSACTION;

	ALTER TABLE dbo_OrderExamTiaraAudioFiles DROP FOREIGN KEY FK_OrderExamTiaraAudioFiles_Documents;
	ALTER TABLE dbo_OrderExamTiaraAudioFiles ADD CONSTRAINT FK_OrderExamTiaraAudioFiles_Documents
		FOREIGN KEY (DocumentId) REFERENCES dbo_Documents(Id) ON DELETE CASCADE ON UPDATE RESTRICT;

COMMIT;	