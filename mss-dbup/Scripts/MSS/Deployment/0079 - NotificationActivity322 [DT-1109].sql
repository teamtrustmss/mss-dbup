START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsCustomerToViewReport varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsCustomerToViewReportNonReply varchar(256) NULL;
	
	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		 ('2ce16386-2565-4f96-a469-782c7a67361b',318,'Email Customer To View Report','2023-08-29 11:25:43','2023-08-29 11:25:43',1);
		 
COMMIT;