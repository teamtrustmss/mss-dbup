START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomers MODIFY COLUMN AccountManager varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'tbc' NOT NULL;
	ALTER TABLE dbo_OrderingCustomers MODIFY COLUMN NextChargeReview datetime(6) DEFAULT '1900-01-01 00:00:00.000000' NOT NULL;
	ALTER TABLE dbo_OrderingCustomers MODIFY COLUMN ContractStartDate datetime(6) NOT NULL;

COMMIT;