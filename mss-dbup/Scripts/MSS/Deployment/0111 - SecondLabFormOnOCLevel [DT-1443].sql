START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomers ADD LabFormVersion tinyint(1) DEFAULT 0 NOT NULL;
	
	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		('4d80bc85-13e2-ee11-9b16-489ebd28a7c6',104,'Anonymised Lab Request Form','2024-03-14 16:59:58.102330','2024-03-14 16:59:58.102330',1);

COMMIT;