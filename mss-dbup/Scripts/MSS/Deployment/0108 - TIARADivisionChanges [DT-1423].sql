START TRANSACTION;

	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridOrderingCustomerTeamMembers` AS
		select
			`u`.`Id` AS `Id`,
			concat(`u`.`FirstName`, ' ', `u`.`LastName`) AS `Name`,
			`u`.`FirstName` AS `FirstName`,
			`u`.`LastName` AS `LastName`,
			`u`.`UserName` AS `UserName`,
			`u`.`IsActive` AS `IsActive`,
			`oct`.`Name` AS `TeamName`,
			`ocd`.`Name` AS `DivisionName`,
			`ocd`.`Id` AS `DivisionId`,
			'' AS `Rolename`,
			1 AS `CanEdit`,
			`ocd`.`OrderingCustomerId` AS `OrderingCustomerId`,
			`oct`.`Id` AS `TeamId`,
			ifnull(`octm`.`IsApiUser`, 0) AS `IsApiUser`,
			(case
				when (`r`.`Name` like 'OC Admin') then 1
				else 0
			end) AS `IsOcAdmin`
		from
			((((((`MssPlatformDb`.`dbo_OrderingCustomerDivisions` `ocd`
		join `MssPlatformDb`.`dbo_OrderingCustomerTeams` `oct` on
			((`ocd`.`Id` = `oct`.`DivisionId`)))
		join `MssPlatformDb`.`dbo_OrderingCustomerTeams_OrderingCustomerTeamMembers` `octoctm` on
			(((`oct`.`Id` = `octoctm`.`OrderingCustomerTeamId`)
				and (`octoctm`.`IsDefaultTeam` = 1))))
		join `MssPlatformDb`.`dbo_Users` `u` on
			((`octoctm`.`OrderingCustomerTeamMemberId` = `u`.`Id`)))
		join `MssPlatformDb`.`dbo_OrderingCustomerTeamMembers` `octm` on
			((`octm`.`Id` = `u`.`Id`)))
		join `MssPlatformDb`.`dbo_UserRoles` `ur` on
			((`ur`.`UserId` = `u`.`Id`)))
		join `MssPlatformDb`.`dbo_Roles` `r` on
			((`r`.`Id` = `ur`.`RoleId`)));

COMMIT;