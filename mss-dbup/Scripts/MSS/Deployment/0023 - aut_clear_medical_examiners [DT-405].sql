start transaction;

DROP TABLE  IF EXISTS delete_examiners;
CREATE TEMPORARY TABLE delete_examiners
select dme.Id from dbo_MedicalExaminers dme where dme.FirstName like 'Autexaminer%';

DELETE FROM dbo_MedicalExaminerServiceAreas_PostCodes dmesapc 
WHERE dmesapc.MedicalExaminerServiceAreaId IN (SELECT dmesa.Id FROM dbo_MedicalExaminerServiceAreas dmesa WHERE dmesa.Id IN (select Id from delete_examiners));

DELETE dmesa FROM dbo_MedicalExaminerServiceAreas dmesa
WHERE dmesa.Id IN (select Id from delete_examiners);

delete rrcd, rrll, dea, deba, dep from dbo_MedicalExaminers dme
LEFT JOIN dbo_ExaminerBankAccount deba ON deba.ExaminerId = dme.Id 
LEFT JOIN dbo_ExaminerAddresses dea ON dea.ExaminerId = dme.Id 
LEFT JOIN dbo_ExaminerPhones dep ON dep.ExaminerId = dme.Id 
LEFT JOIN reporting_ReportChartData rrcd ON rrcd.MedicalExaminerId = dme.Id 
LEFT JOIN reporting_ReportLoadLogs rrll ON rrll.MedicalExaminerId = dme.Id 
where dme.Id in (select Id from delete_examiners);

DELETE FROM dbo_FeeGroupExaminerCapabilities dfgec WHERE dfgec.FeeGroupExaminerId IN (select Id from delete_examiners);
DELETE FROM dbo_FeeGroupExaminers dfge WHERE dfge.Id IN (select Id from delete_examiners);

DELETE FROM dbo_MedicalExaminers dme where dme.Id in (select Id from delete_examiners);

DELETE FROM dbo_Examiners de where de.Id in (select Id from delete_examiners);

DROP TABLE  delete_examiners;

ROLLBACK;