START TRANSACTION;

	ALTER TABLE dbo_WebhookEndpointsEvents DROP FOREIGN KEY FK_WebhookEndpointsEvents_WebhookEvents;
	ALTER TABLE dbo_WebhookEndpointsEvents
		ADD CONSTRAINT FK_WebhookEndpointsEvents_WebhookEvents FOREIGN KEY (WebhookEventId)
		REFERENCES dbo_WebhookEvents(Id) ON DELETE CASCADE ON UPDATE RESTRICT;
	
	ALTER TABLE `dbo_Activities` ADD `AllowWebhookNotifications` tinyint(1) NOT NULL;
	
	UPDATE `dbo_Activities` da SET da.`AllowWebhookNotifications` = 1 WHERE da.`Id` in (SELECT `Id` FROM `dbo_WebhookEvents`);
	
COMMIT;