START TRANSACTION;

CREATE TABLE `dbo_HistoricalGeneralPractitioners` (
  `Id` varchar(64) NOT NULL,
  `InvoiceReferenceNumber` varchar(255) DEFAULT NULL,
  `PayeeName` varchar(255) DEFAULT NULL,
  `BestContact` varchar(255) DEFAULT NULL,
  `PaymentType` int NOT NULL DEFAULT '0',
  `AccountNo` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `SortCode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PayeeEmailAddress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  CONSTRAINT `FK_HistoricalGeneralPractitioners_Exams` FOREIGN KEY (`Id`) REFERENCES `dbo_OrderExams` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

ALTER TABLE dbo_MedicalExaminations ADD InvoiceProcessing tinyint(1) DEFAULT 0 NOT NULL;
ALTER TABLE dbo_Documents ADD Status varchar(100) DEFAULT NULL;
ALTER TABLE dbo_OrderingCustomerExaminations ADD IsDAV tinyint(1) DEFAULT 0 NOT NULL;
ALTER TABLE dbo_OrderingCustomerExaminations ADD DAV decimal(18,2) DEFAULT 0 NOT NULL;
ALTER TABLE dbo_HistoricalGeneralPractitioners ADD InvoiceFee decimal(18,2) NULL;

INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
	('c0951fed-a691-4c38-95da-1b5fd9b8a202',805,'GP Invoice','2022-07-11 14:23:46','2022-07-11 14:23:46',1);

COMMIT;