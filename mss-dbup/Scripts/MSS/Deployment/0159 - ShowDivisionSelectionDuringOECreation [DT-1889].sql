START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomers ADD ShowDivisionDropdown tinyint(1) DEFAULT 0 NOT NULL;

COMMIT;