start transaction;

DROP TABLE  IF EXISTS delete_ServiceCodes;

CREATE TEMPORARY TABLE delete_ServiceCodes
SELECT dme.Id FROM dbo_MedicalExaminations dme 
where dme.Code LIKE 'AUT-%';

DELETE FROM dbo_OrderingCustomerCharges docc
WHERE docc.AllowedExaminerId IN (SELECT dae.Id FROM dbo_AllowedExaminers dae where dae.MedicalExaminationId IN (SELECT dsc.Id  FROM delete_ServiceCodes dsc));

DELETE FROM dbo_AllowedExaminers dae
where dae.MedicalExaminationId IN (SELECT dsc.Id  FROM delete_ServiceCodes dsc);

DELETE FROM dbo_MedicalExaminations dme
where dme.Id IN (SELECT dsc.Id  FROM delete_ServiceCodes dsc);

DELETE FROM dbo_OrderingCustomerExaminations doce 
where doce.Id IN (SELECT dsc.Id  FROM delete_ServiceCodes dsc);

DROP TABLE delete_ServiceCodes;

COMMIT;
