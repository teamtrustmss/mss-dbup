START TRANSACTION;

	ALTER TABLE dbo_Users ADD HasOrderExamAccess tinyint(1) DEFAULT 1 NOT NULL;
	ALTER TABLE dbo_Users ADD CompanyName varchar(254) NULL;

COMMIT;