START TRANSACTION;

	INSERT INTO dbo_MedicalExaminations (Id,Code,Description,IsLinkedDocument,IsElectronicResults,ExamQuestionnaireAndKitToBeSentToGp,ExamQuestionnaireRequired,FastingRequired,GpDetailsRequired,HivFormRequired,IsPdfResults,SampleSentToLab,SignedAmraRequired,AutomaticAdditionalChargeId,AssigmentTypeId,IsAutomaticallyAddedToOrderingCustomer,IsActive,OrderForAutomaticChargeList,ServiceCodeAddedActivity,IsSendToExaminer,CreateTime,ModifyTime,InvoiceProcessing,IsRequiresEssentialDocument,CareTypeId) VALUES
		('5e093f79-3a4b-ef11-9b2c-f09e4a5e690f','VACCINATION','Vaccination Taking fee',0,0,0,0,0,0,0,0,0,0,NULL,0,1,1,50,NULL,0,NOW(),NOW(),0,0,0);

	INSERT INTO dbo_MedicalExamination_Capabilities (ExaminationId,CapabilityId) VALUES
		('5e093f79-3a4b-ef11-9b2c-f09e4a5e690f','8dd0f741-d911-41ce-a75a-b47d5c8f48c0');

	INSERT INTO dbo_AllowedExaminers (Id,DefaultOcCharge,ExaminerType,MedicalExaminationId,IsActive,CreateTime,ModifyTime) VALUES
		('60093f79-3a4b-ef11-9b2c-f09e4a5e690f',0.00,2,'5e093f79-3a4b-ef11-9b2c-f09e4a5e690f',1,NOW(),NOW());
	
	INSERT INTO dbo_OrderingCustomerExaminations (Id, OrderingCustomerId, MedicalExaminationId, OcCode, OcDescription, IsActive, CreateTime, ModifyTime, IsDAV, DAV, FixedFeeThreshold, ChargingModelId, TiaraCode)
	SELECT 
		UUID(), o.Id, '5e093f79-3a4b-ef11-9b2c-f09e4a5e690f', 'VACCINATION', 'Vaccination', 1, NOW(), NOW(), 0, 0.00, 0.00, 0, NULL
	FROM 
		dbo_OrderingCustomers o;
		
	INSERT INTO dbo_OrderingCustomerCharges (Id,Charge,IsChargeAsPercent,OcMedicalExaminationId,AllowedExaminerId,ServiceTypeId,IsActive,CreateTime,ModifyTime)
	SELECT
		UUID(), 0.00, 0, ocME.Id, '60093f79-3a4b-ef11-9b2c-f09e4a5e690f', 2, 1, NOW(), NOW()
	FROM 
		dbo_OrderingCustomerExaminations ocME
		WHERE ocME.OcCode = 'VACCINATION';

COMMIT;