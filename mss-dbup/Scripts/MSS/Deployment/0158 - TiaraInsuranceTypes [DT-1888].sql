START TRANSACTION;

	CREATE TABLE `dbo_TiaraInsuranceTypes` (
	  `Code` VARCHAR(1) NOT NULL,
	  `Description` varchar(100) NOT NULL,
	  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`Code`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

COMMIT;