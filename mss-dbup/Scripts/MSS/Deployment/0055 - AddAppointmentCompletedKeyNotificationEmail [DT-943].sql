START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsExamAppointmentCompleted varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsExamAppointmentCompletedNonReply varchar(256) NULL;
	
	INSERT INTO dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		 ('d6284ff4-3599-46b3-9bf0-56a8f9b4054e',317,'Email Appointment Completed','2023-04-04 18:00:00','2023-04-04 18:00:00',1);
		 
	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,
		AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,
		CreateTime,ModifyTime) VALUES
		('f4d77a6e-f989-4a96-a56f-d9d8086e8e16','385','Appointment has taken place',1,NULL,NULL,0,1,0,24654,2,0,NULL,0,0,1,0,NULL,'2023-04-05 14:53:50.734542000','2023-04-05 14:53:50.734542000');
		
	INSERT INTO dbo_WebhookEvents (Id,Description,AlertStatus,IsActive,CreateTime,ModifyTime) VALUES
		('f4d77a6e-f989-4a96-a56f-d9d8086e8e16','385: Appointment has taken place','A',1,'2023-04-05 14:53:50','2023-04-05 14:53:50');

COMMIT;