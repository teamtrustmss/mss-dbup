START TRANSACTION;

	ALTER TABLE MssPlatformDb.dbo_HistoricalGeneralPractitioners ADD PreviousInvoiceFee decimal(18,2) NULL;
	ALTER TABLE MssPlatformDb.dbo_HistoricalGeneralPractitioners ADD PreviousAccountNo varchar(8) NULL;
	ALTER TABLE MssPlatformDb.dbo_HistoricalGeneralPractitioners ADD PreviousSortCode varchar(6) NULL;
	
	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,
		VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,
		SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime) VALUES
		('cbae32c6-fd07-4dbb-b28f-d2e1040a5415','924','Invoice field changes have been accepted: [names]',6,NULL,NULL,0,1,0,4,1,0,NULL,0,0,1,0,NULL,'2023-05-03 16:00:00','2023-05-03 16:00:00');
		
COMMIT;