START TRANSACTION;

	INSERT INTO MssPlatformDb.dbo_Activities
    (Id, Code, Description, ActivityTypeId, MaxDaysLimit, DefaultNumberOfDays, VisibleToExaminer, VisibleToOrderingCustomer, AllowMedicalExaminerUse, AllowedOrderStatuses, NextActionDateCalculationMode, SuspendedTimeService, NextActivityId, CommentRequired, CalculateNADFromScheduledDate, IsActive, NotificationType, SystemActivityComment, CreateTime, ModifyTime)
    VALUES('69aa2ba2-e2e8-ed11-836b-b059b21e9175', '063', 'Kit Fulfilled and Posted', 1, NULL, NULL, 0, 1, 1, 8192, 2, 0, NULL, 0, 0, 1, 0, NULL, '2023-05-02 13:12:40.937700000', '2023-05-02 13:12:40.937700000');

		
	INSERT INTO dbo_WebhookEvents (Id,Description,AlertStatus,IsActive,CreateTime,ModifyTime) VALUES
		('69aa2ba2-e2e8-ed11-836b-b059b21e9175','063: Kit Fulfilled and Posted','A',0,'2023-05-03 14:53:50','2023-05-03 14:53:50');

COMMIT;