#script v3 - unassign selected users FROM CSR

START TRANSACTION;

DROP TABLE IF EXISTS delete_Users;
CREATE TEMPORARY TABLE delete_Users
SELECT du.Id, du.UserName
FROM dbo_Users du WHERE (du.UserName LIKE 'aut.%' OR du.Id = 'd0387344-a408-e611-8146-025fe46d3d9d') AND (UserName != 'aut.superadmin');


DELETE FROM reporting_DashboardReports WHERE DashboardId IN (SELECT rds.Id FROM reporting_Dashboards rds WHERE UserId IN (SELECT Id FROM delete_Users));
DELETE FROM reporting_Dashboards WHERE UserId IN (SELECT Id FROM delete_Users);
DELETE FROM reporting_ReportChartData WHERE ReportId IN ( SELECT Id FROM reporting_Reports WHERE UserId IN (SELECT Id FROM delete_Users));
DELETE FROM reporting_ReportChartData WHERE UserId IN (SELECT Id FROM delete_Users);
DELETE FROM reporting_ReportLoadLogs WHERE ReportId IN ( SELECT Id FROM reporting_Reports WHERE UserId IN (SELECT Id FROM delete_Users));
DELETE FROM reporting_ReportLoadLogs WHERE UserId IN (SELECT Id FROM delete_Users);
DELETE FROM reporting_ReportSchedules WHERE ReportId IN ( SELECT Id FROM reporting_Reports WHERE UserId IN (SELECT Id FROM delete_Users));
DELETE FROM reporting_Reports WHERE UserId IN (SELECT Id FROM delete_Users);

DELETE FROM reporting_ReportChartData WHERE UserId IN (SELECT Id FROM delete_Users);

DELETE FROM dbo_AuditLogs dal WHERE UserId IN (SELECT Id FROM delete_Users);
DELETE FROM dbo_PreviousPasswords WHERE UserId IN (SELECT Id FROM delete_Users);

DELETE daa, da, dae FROM dbo_Applicants dap
LEFT JOIN dbo_ApplicantAddresses daa ON daa.ApplicantId = dap.Id 
LEFT JOIN dbo_ApplicantEmails dae ON dae.ApplicantId = dap.Id 
LEFT JOIN dbo_ApplicantPhones da ON da.ApplicantId = dap.Id 
WHERE dap.Id IN (SELECT Id FROM dbo_Orders WHERE OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users) );

DELETE FROM dbo_Applicants WHERE Id IN (SELECT Id FROM dbo_Orders WHERE OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users));

DELETE req, act, deap,msc, gp FROM dbo_OrderExams doe 
LEFT JOIN dbo_OrderExamRequirements req ON req.OrderExamId = doe.Id 
LEFT JOIN dbo_OrderExamActivities act ON act.OrderExamId = doe.Id 
LEFT JOIN dbo_OrderExamAppointments deap ON deap.Id = doe.Id 
LEFT JOIN dbo_MiscellaneousCharges msc ON msc.OrderExamId = doe.Id 
LEFT JOIN dbo_GeneralPractitioners gp ON gp.Id = doe.Id 
WHERE doe.Id IN (SELECT doe.Id FROM dbo_OrderExams doe JOIN dbo_Orders do ON do.Id = doe.OrderId WHERE do.OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users) );

UPDATE dbo_OrderExams doe SET doe.ExamOwnerId = NULL WHERE doe.ExamOwnerId IN (SELECT Id FROM delete_Users);
DELETE doe FROM dbo_OrderExams doe JOIN dbo_Orders do ON do.Id = doe.OrderId WHERE do.OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users);

DELETE FROM dbo_LinkedOrders lk WHERE lk.OrderId IN (SELECT Id FROM dbo_Orders WHERE OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users));
DELETE FROM dbo_LinkedOrders lk WHERE lk.ParentOrderId IN (SELECT Id FROM dbo_Orders WHERE OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users));
DELETE FROM dbo_Orders WHERE OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users);

DELETE FROM dbo_OrderingCustomerTeams_OrderingCustomerTeamMembers doctoctm WHERE doctoctm.OrderingCustomerTeamMemberId IN (SELECT Id FROM delete_Users);
DELETE FROM dbo_OrderingCustomerTeamMembers doctm WHERE doctm.Id IN (SELECT Id FROM delete_Users);
DELETE FROM dbo_UserRoles WHERE UserId IN (SELECT Id FROM delete_Users);
DELETE FROM dbo_OrderExamRequirementActivities WHERE Id IN (SELECT Id FROM dbo_OrderExamActivities WHERE CreatorId IN (SELECT Id FROM delete_Users));
DELETE FROM dbo_OrderExamActivities WHERE CreatorId IN (SELECT Id FROM delete_Users);
DELETE FROM dbo_OrderExamActivities WHERE ActAssignedToId IN (SELECT Id FROM delete_Users);
DELETE FROM dbo_Users WHERE Id IN (SELECT Id FROM delete_Users);

DROP TABLE delete_Users;

ROLLBACK;