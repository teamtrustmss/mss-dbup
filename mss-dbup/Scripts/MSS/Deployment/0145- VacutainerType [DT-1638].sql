START TRANSACTION;

	ALTER TABLE dbo_SampleRequirements ADD TypeId int DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_OrderExams ADD FulfilmentVacutainerTypeId int NULL;
	
	UPDATE dbo_OrderExams SET FulfilmentVacutainerTypeId = 0 WHERE ExamTypeId = 5;

COMMIT;