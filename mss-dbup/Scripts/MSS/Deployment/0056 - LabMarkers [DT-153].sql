START TRANSACTION;

	ALTER TABLE dbo_LaboratoryExaminations ADD LabMarkers varchar(256) NULL;

DELIMITER $$

	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `dbo_GridLaboratoryExaminations` AS
	select
		`e`.`Id` AS `Id`,
		`f`.`IsActive` AS `IsActive`,
		`e`.`LaboratoryId` AS `LaboratoryId`,
		`e`.`LabExaminationCode` AS `LabExaminationCode`,
		`e`.`LabExaminationDesription` AS `LabExaminationDesription`,
		`e`.`LabMarkers`,
		`e`.`SLATurnAroundTime` AS `SLA`,
		`m`.`Id` AS `MSSId`,
		`m`.`Code` AS `MSSCode`,
		`m`.`Description` AS `MSSDescription`,
		`f`.`Fee` AS `Fee`,
		(
		select
			group_concat(distinct `s`.`Vacutainer` separator ', ')
		from
			(`dbo_SampleRequirements` `s`
		left join `dbo_LaboratoryExaminations_SampleRequirements` `x` on
			((`x`.`SampleRequirementId` = `s`.`Id`)))
		where
			(`x`.`LaboratoryExaminationId` = `e`.`Id`)
		group by
			`x`.`LaboratoryExaminationId`) AS `Vacutainers`
	from
		((`dbo_LaboratoryExaminations` `e`
	join `dbo_ExaminationFees` `f` on
		((`f`.`Id` = `e`.`Id`)))
	join `dbo_MedicalExaminations` `m` on
		((`m`.`Id` = `f`.`MedicalExaminationId`)));

DELIMITER ;


COMMIT;