START TRANSACTION;

	ALTER TABLE dbo_KitTypes ADD TypeId int DEFAULT 2 NOT NULL;
	ALTER TABLE dbo_OrderExams CHANGE FulfilmentVacutainerTypeId FulfilmentVacutainerTypeIds VARCHAR(10) NULL;
	UPDATE dbo_OrderExams SET FulfilmentVacutainerTypeIds = '2'
		WHERE ExamTypeId = 5
		AND StatusId NOT IN (32,64,16384,65536)
		AND FulfilmentVacutainerTypeIds = '0';
	
	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `dbo_GridKitTypes` AS
	SELECT
		`k`.`Id` AS `Id`,
		`k`.`IsActive` AS `IsActive`,
		`k`.`CreateTime` AS `CreateTime`,
		`k`.`Name` AS `Name`,
		`k`.`Weight` AS `Weight`,
		`k`.`TypeId` AS `TypeId`,
		(
		SELECT
			group_concat(DISTINCT `s`.`Vacutainer` ORDER BY `s`.`Vacutainer` ASC SEPARATOR ', ')
		FROM
			(`MssPlatformDb`.`dbo_SampleRequirements` `s`
		LEFT JOIN `MssPlatformDb`.`dbo_KitTypes_SampleRequirements` `x` ON
			((`x`.`SampleRequirementId` = `s`.`Id`)))
		WHERE
			(`x`.`KitTypeId` = `k`.`Id`)
		GROUP BY
			`x`.`KitTypeId`) AS `ConcatenatedVacutainers`
	FROM
		(`dbo_KitTypes` `k`
	LEFT JOIN `dbo_OrderingCustomerKitTypes` `ockt` ON
		((`k`.`Id` = `ockt`.`KitTypeId`)))
	WHERE
		(`ockt`.`KitTypeId` IS NULL);
	
	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `dbo_GridOrderingCustomerKitTypes` AS
	select
		`ok`.`Id` AS `Id`,
		`ok`.`OrderingCustomerId` AS `OrderingCustomerId`,
		`ok`.`KitTypeId` AS `KitTypeId`,
		`ok`.`IsActive` AS `IsActive`,
		`ok`.`CreateTime` AS `CreateTime`,
		`k`.`Name` AS `Name`,
		`k`.`Weight` AS `Weight`,
		`k`.`TypeId` AS `TypeId`,
		(
		select
			group_concat(distinct `s`.`Vacutainer` order by `s`.`Vacutainer` ASC separator ', ')
		from
			(`MssPlatformDb`.`dbo_SampleRequirements` `s`
		left join `MssPlatformDb`.`dbo_KitTypes_SampleRequirements` `x` on
			((`x`.`SampleRequirementId` = `s`.`Id`)))
		where
			(`x`.`KitTypeId` = `k`.`Id`)
		group by
			`x`.`KitTypeId`) AS `ConcatenatedVacutainers`
	from
		(`dbo_OrderingCustomerKitTypes` `ok`
	join `dbo_KitTypes` `k` on
		((`k`.`Id` = `ok`.`KitTypeId`)));

COMMIT;