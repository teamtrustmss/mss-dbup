START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerTeams ADD CompletedResultsEmailNonReply varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsEmailAppointmentNonReply varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsEmailOnHoldNonReply varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsEmailQueryNonReply varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsDnaEventNonReply varchar(256) NULL;
	
	INSERT INTO MssPlatformDb.dbo_ApplicationSettings (`Key`,ContainerName,Value,Comment) VALUES
		 ('KeyEventSentFrom','Presentation.MssPortal','inuvi.it@inuvi.co.uk','Email to send notifications about status changes'),
		 ('KeyEventSentFromNonReply','Presentation.MssPortal','no-reply@inuvi.co.uk','Email to send notifications about status changes (no-reply)');

COMMIT;