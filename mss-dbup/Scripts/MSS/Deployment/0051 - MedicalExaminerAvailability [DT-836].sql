START TRANSACTION;

	CREATE TABLE `dbo_ExaminerAvailabilityStatuses` (
	  `Id` int NOT NULL,
	  `Name` varchar(50) NOT NULL,
	  `SortOrder` int NOT NULL DEFAULT '0',
	  PRIMARY KEY (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO MssPlatformDb.dbo_ExaminerAvailabilityStatuses (Id,Name,SortOrder) VALUES
		 (0,'Unknown',0),
		 (1,'Available',0),
		 (2,'New',10),
		 (3,'Holiday',20),
		 (4,'Catch Up',30),
		 (5,'Other',40),
		 (6,'Sick',50),
		 (7,'Remotes Only',60);

	CREATE TABLE `dbo_ExaminerAvailabilities` (
	  `Id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  `AvailabilityNotes` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
	  `AwayFrom` datetime(6) DEFAULT NULL,
	  `ReturnDate` datetime(6) DEFAULT NULL,
	  `WeeklyCapacity` int DEFAULT NULL,
	  `ExaminerId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	  `StatusId` int DEFAULT NULL,
	  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
	  `CreateTime` datetime(6) NOT NULL,
	  `ModifyTime` datetime(6) NOT NULL,
	  PRIMARY KEY (`Id`),
	  UNIQUE KEY `Id` (`Id`),
	  KEY `FK_ExaminerAvailabilities_ExaminerAvailabilitiesStatuses` (`StatusId`) USING BTREE,
	  CONSTRAINT `FK_ExaminerAvailabilities_ExaminerAvailabilitiesStatuses_copy` FOREIGN KEY (`StatusId`) REFERENCES `dbo_ExaminerAvailabilityStatuses` (`Id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	  CONSTRAINT `FK_ExaminerAvailabilities_Examiners` FOREIGN KEY (`ExaminerId`) REFERENCES `dbo_Examiners` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
COMMIT;