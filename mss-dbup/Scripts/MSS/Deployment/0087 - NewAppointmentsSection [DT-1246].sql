START TRANSACTION;

	CREATE TABLE `dbo_OrderExamAppointmentStatuses` (
	  `Id` int NOT NULL,
	  `Name` varchar(50) NOT NULL,
	  `SortOrder` int NOT NULL DEFAULT '0',
	  PRIMARY KEY (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	
	INSERT INTO dbo_OrderExamAppointmentStatuses (Id,Name,SortOrder) VALUES
	 (0,'Scheduled',0),
	 (1,'Appointment Completed',10),
	 (2,'Appointment Cancelled',20),
	 (3,'DNA',30);
	
	ALTER TABLE dbo_OrderExamAppointments ADD AppointmentGroupId varchar(64) NOT NULL;
	
	CREATE TABLE `dbo_OrderExamAppointmentsHistory` (
	  `Id` varchar(64) NOT NULL,
	  `AppointmentId` varchar(64) NOT NULL,
	  `AppointmentGroupId` varchar(64) NOT NULL,
	  `Date` datetime(6) DEFAULT NULL,
	  `StatusId` int NOT NULL,
	  `ModifiedBy` varchar(64) DEFAULT NULL,
	  `Examiner` text,
	  `Travelled` tinyint(1) DEFAULT NULL,
	  `OrderExamCustomId` varchar(255) DEFAULT NULL,
	  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
	  `CreateTime` datetime(6) NOT NULL,
	  `ModifyTime` datetime(6) NOT NULL,
	  PRIMARY KEY (`Id`),
	  UNIQUE KEY `Id` (`Id`),
	  KEY `dbo_OrderExamAppointmentsHistory_StatusId` (`StatusId`) USING BTREE,
	  KEY `FK_OrderExamAppointments_OrderExamAppointmentsHistory` (`AppointmentId`),
	  CONSTRAINT `FK_OrderExamAppointments_OrderExamAppointmentsHistory` FOREIGN KEY (`AppointmentId`) REFERENCES `dbo_OrderExamAppointments` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

COMMIT;