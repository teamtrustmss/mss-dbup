START TRANSACTION;

CREATE INDEX dbo_Orders_OrderingCustomerId_IDX USING BTREE ON dbo_Orders (OrderingCustomerId,IsAnonymised);

COMMIT;