START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('227e5c95-02b4-4756-802d-0acf4e58c8c8','11017','Advisor has been added to the Tele-interview case in MDG',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0),
		('31077fb0-6068-4d31-a2c4-8694bb75183c','11018','Advisor could not be added to the Tele-interview case in MDG, moved to request queue',7,NULL,NULL,0,1,0,64490,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);

COMMIT;