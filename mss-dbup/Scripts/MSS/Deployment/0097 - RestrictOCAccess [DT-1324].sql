START TRANSACTION;

	ALTER TABLE dbo_Users ADD RestrictAccessOcModeId int DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_Users ADD RestrictedOrderingCustomersIds TEXT NULL;
	
	ALTER TABLE grid_AllOrderExams ADD OrderingCustomerId varchar(64) NOT NULL;
	
	DROP TRIGGER afterInsert_OrderExam;
	
	CREATE TRIGGER `afterInsert_OrderExam` AFTER INSERT ON `dbo_OrderExams` FOR EACH ROW INSERT INTO grid_AllOrderExams (Id,OrderId,IsActive,OrderExamNumber,ExamType, NextActionDate,NextActionDateSort,Origin,TypeOfCover,OrderingCustomerId,OrderingCustomer,ExamPostCode,ApplicantFirstName,ApplicantLastName,

		   ApplicantDoB,PolicyNumber,ClientReference2,IsUrgentCase,IsTouched,OrderDate,`Status`,AppointmentScheduled,ExaminerFirstName,ExaminerLastName,ExamOwnerId,CsrFirstName,CsrLastName,

		   GpName,ClinicName,IsAnonymised,HighNetWorth,International,HasLinkedOrders, AssignedToPrefix,AssignedTo,Applicant,AssignedCSR,LatestActivityId,TimeService,InactiveTimeService,OrderingCustomerTeamMemberId,

		   OrderingCustomerTeamId,ApplicantPostCode,SelectedKitTypes)  SELECT 

		    orderExam.Id                                        AS Id,

		    ocOrder.Id                                          AS OrderId,

		    orderExam.IsActive                                  AS IsActive,

		    orderExam.ExamCustomId                              AS OrderExamNumber,

		    orderExam.ExamTypeId                                AS ExamType,

		    (SELECT dbo_GetExamNextActionDate(orderExam.Id))    AS NextActionDate,

		    IFNULL((SELECT dbo_GetExamNextActionDate(orderExam.Id)), '9999-12-31')            AS NextActionDateSort,

		    ocOrder.OrderingChannelId                           AS Origin,

		    ocOrder.CoverTypeId                                 AS TypeOfCover,

		    oc.Id                                      		AS OrderingCustomerId,
		    
		    oc.CompanyName                                      AS OrderingCustomer,

		    appointment.PostCode                                AS ExamPostCode,

		    applicant.FirstName                                 AS ApplicantFirstName,

		    applicant.LastName                                  AS ApplicantLastName,

		    applicant.DateOfBirth                               AS ApplicantDoB,

		    ocOrder.PolicyNumber                                AS PolicyNumber,

		    ocOrder.ClientReference2                            AS ClientReference2,

		    orderExam.IsUrgent                                  AS IsUrgentCase,

		    0                                                   AS IsTouched,

		    orderExam.CreateTime                                AS OrderDate,

		    orderExam.StatusId                                  AS `Status`,

		    appointment.`Date`                                  AS AppointmentScheduled,

		    medicalExaminer.FirstName                           AS ExaminerFirstName,

		    medicalExaminer.LastName                            AS ExaminerLastName,

		    examOwner.Id                                        AS ExamOwnerId, 

		    examOwner.FirstName                                 AS CsrFirstName,

		    examOwner.LastName                                  AS CsrLastName,

		    gpPractice.DoctorName                               AS GpName,

		    clinic.`Name`                                       AS ClinicName,

		    ocOrder.IsAnonymised                                AS IsAnonymised,

		    (SELECT IFNULL((SELECT 1 

		            FROM dbo_OrderExamRequirements R  

		            WHERE orderExam.Id = R.OrderExamId 

		            AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 

		            LIMIT 1), 0))                               AS HighNetWorth,

		    (SELECT IFNULL((SELECT 1 

		        FROM dbo_OrderExamRequirements R  

		        WHERE orderExam.Id = R.OrderExamId 

		        AND R.`Code` = 'INT FEE' 

		        LIMIT 1), 0) )                                  AS International,

		    (SELECT IFNULL((SELECT 1 

		            FROM dbo_LinkedOrders LO 

		            WHERE LO.OrderId = orderExam.OrderId 

		            OR LO.ParentOrderId = orderExam.OrderId

		            LIMIT 1), 0))                               AS HasLinkedOrders,

		    (SELECT AssignedToPrefix 

		        FROM dbo_OrderExamActivities OEA 

		        WHERE OEA.OrderExamId = orderExam.Id 

		        ORDER BY ActivityOrder DESC LIMIT 1)               AS AssignedToPrefix,

		    (SELECT (CASE WHEN AssignedToPrefix IS NULL 

		            THEN AssignedTo 

		            ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo) 

		            END) 

		        FROM dbo_OrderExamActivities OEA 

		        WHERE OEA.OrderExamId = orderExam.Id 

		        ORDER BY ActivityOrder DESC LIMIT 1)               AS AssignedTo,

		    CONCAT(applicant.FirstName, ' ', applicant.LastName)AS Applicant,

		    CONCAT(examOwner.FirstName, ' ', examOwner.LastName)AS AssignedCSR,

		    (SELECT Id 

		        FROM dbo_OrderExamActivities E 

		        WHERE E.OrderExamId=orderExam.Id 

		        ORDER BY E.ActivityOrder DESC 

		        LIMIT 1)                                        AS LatestActivityId,

		    IFNULL((SELECT dbo_GetTimeServiceForActivity(LatestActivityId, NULL)),0) AS TimeService,

		    IFNULL((SELECT dbo_GetInactiveTimeServiceForExam(orderExam.Id)),0) AS InactiveTimeService,

		    ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,                            

		    IFNULL(ocOrder.OrderingCustomerTeamId, '00000000-0000-0000-0000-000000000000') AS OrderingCustomerTeamId,

		    (SELECT DISTINCT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')

		        FROM dbo_Applicants a 

		        LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId

		        WHERE applicant.Id = a.Id

		        GROUP BY a.Id, aa.ApplicantId

		    )                                                    AS ApplicantPostCode,
			
		    orderExam.SelectedKitTypes                          AS SelectedKitTypes

		 FROM dbo_OrderExams orderExam

		    INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId 

		    INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId 

		    INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id

		    LEFT JOIN dbo_OrderExamAppointments appointment ON appointment.Id = orderExam.Id

		    LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId

		    LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId

		    LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId

		    LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId

		    WHERE orderExam.Id = NEW.Id;
			
COMMIT;