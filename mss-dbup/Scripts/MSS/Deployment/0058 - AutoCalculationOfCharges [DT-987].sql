START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerExaminations ADD FixedFeeThreshold decimal(18,2) DEFAULT 0.00 NOT NULL;
	
	CREATE TABLE dbo_OrderingCustomerExaminationChargingModels (
		Id int NOT NULL,
		Name varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
		Code varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
		SortOrder int DEFAULT 0 NOT NULL,
		CONSTRAINT `PRIMARY` PRIMARY KEY (Id)
	)
	ENGINE=InnoDB
	DEFAULT CHARSET=utf8mb4
	COLLATE=utf8mb4_0900_ai_ci
	COMMENT='';

	INSERT INTO dbo_OrderingCustomerExaminationChargingModels (Id,Name,Code,SortOrder) VALUES
	 (0,'Global Service Code','GSC',0),
	 (1,'Fixed','F',10),
	 (2,'Cost Plus','CP',20);
	 
	ALTER TABLE dbo_OrderingCustomerExaminations
		ADD ChargingModelId int NOT NULL DEFAULT 0,
		ADD KEY `FK_OrderingCustomers_OrderingCustomerExaminationChargingModels` (`ChargingModelId`),
		ADD CONSTRAINT `FK_OrderingCustomers_OrderingCustomerExaminationChargingModels` FOREIGN KEY (`ChargingModelId`) REFERENCES `dbo_OrderingCustomerExaminationChargingModels` (`Id`);
		
COMMIT;