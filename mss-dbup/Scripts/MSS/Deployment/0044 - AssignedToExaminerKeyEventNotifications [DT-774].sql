START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsEmailAssignedToExaminer varchar(256) NULL;
	ALTER TABLE dbo_OrderingCustomerTeams ADD KeyEventCommunicationsEmailAssignedToExaminerNonReply varchar(256) NULL;

	INSERT INTO MssPlatformDb.dbo_DocumentTypes (Id,ReferenceNumber,Name,CreateTime,ModifyTime,IsActive) VALUES
		 ('2ea5ec40-f381-4fa1-b8e4-7cf3722c6fad',316,'Email Assigned to Examiner','2023-01-19 18:00:00','2023-01-19 18:00:00',1);

COMMIT;