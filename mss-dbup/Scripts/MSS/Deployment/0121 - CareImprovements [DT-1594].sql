START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('fc3716a4-b91e-4f7f-ab4b-91291f23ba2c','837','General Comment added',7,NULL,NULL,0,0,0,128,3,0,NULL,0,0,1,1,'The following special instructions have been added to this exam order: [#Examiner Special Instruction#]',NOW(),NOW(),0),
		('526cfbf5-a7eb-4e39-b286-a39485d73454','852','General Comment removed',7,NULL,NULL,0,0,0,63887,2,0,NULL,0,0,1,0,NULL,NOW(),NOW(),0);

COMMIT;