START TRANSACTION;

	ALTER TABLE dbo_OrderingCustomerDivisions ADD UsesOccupation tinyint(1) DEFAULT 0 NOT NULL;
	ALTER TABLE dbo_OrderExams ADD Occupation varchar(250) NULL;

COMMIT;