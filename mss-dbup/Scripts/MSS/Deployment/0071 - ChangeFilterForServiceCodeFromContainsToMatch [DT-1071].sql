START TRANSACTION;

	DROP TRIGGER afterInser_dbo_OrderExamRequirements;
	DROP TRIGGER afterDelete_dbo_OrderExamRequirements;

DELIMITER $$ 

	CREATE TRIGGER `afterInser_dbo_OrderExamRequirements` AFTER INSERT ON `dbo_OrderExamRequirements` FOR EACH ROW
	BEGIN 
	    UPDATE grid_AllOrderExams SET 
	        HighNetWorth = (SELECT IFNULL((SELECT 1 
	            FROM dbo_OrderExamRequirements R  
	            WHERE grid_AllOrderExams.Id = R.OrderExamId 
	            AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
	            LIMIT 1), 0)),
	        International = (SELECT IFNULL((SELECT 1 
	            FROM dbo_OrderExamRequirements R  
	            WHERE grid_AllOrderExams.Id = R.OrderExamId 
	            AND R.`Code` = 'INT FEE' 
	            LIMIT 1), 0) ),
	       ServiceCodes = (
	        	SELECT GROUP_CONCAT(CONCAT("'", ME.Code, "'") SEPARATOR ',')
				FROM dbo_OrderExamRequirements OER
				INNER JOIN dbo_MedicalExaminations ME ON OER.MedicalExaminationId = ME.Id
					WHERE OER.OrderExamId = grid_AllOrderExams.Id
	        )
	    WHERE Id = NEW.OrderExamId;
	END$$

DELIMITER ;

DELIMITER $$ 
	CREATE TRIGGER `afterDelete_dbo_OrderExamRequirements` AFTER DELETE ON `dbo_OrderExamRequirements` FOR EACH ROW
	BEGIN 
		UPDATE grid_AllOrderExams SET 
			HighNetWorth = (SELECT IFNULL((SELECT 1 
				FROM dbo_OrderExamRequirements R  
				WHERE grid_AllOrderExams.Id = R.OrderExamId 
				AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
				LIMIT 1), 0)),
			International = (SELECT IFNULL((SELECT 1 
				FROM dbo_OrderExamRequirements R  
				WHERE grid_AllOrderExams.Id = R.OrderExamId 
				AND R.`Code` = 'INT FEE' 
				LIMIT 1), 0) ),
			ServiceCodes = (
	        	SELECT GROUP_CONCAT(CONCAT("'", ME.Code, "'") SEPARATOR ',')
				FROM dbo_OrderExamRequirements OER
				INNER JOIN dbo_MedicalExaminations ME ON OER.MedicalExaminationId = ME.Id
					WHERE OER.OrderExamId = grid_AllOrderExams.Id
	        )
		WHERE Id = OLD.OrderExamId;
	END$$

DELIMITER ;
COMMIT;