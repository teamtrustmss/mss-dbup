START TRANSACTION;

	DROP TRIGGER afterUpdate_dbo_Users;

	UPDATE dbo_Users SET UserName = TRIM(UserName);

DELIMITER //
	CREATE TRIGGER `afterUpdate_dbo_Users` AFTER UPDATE ON `dbo_Users` FOR EACH ROW BEGIN 
		UPDATE grid_AllOrderExams SET 
			CsrFirstName = NEW.FirstName,
			CsrLastName = NEW.LastName,
			AssignedCSR = CONCAT(NEW.FirstName, ' ', NEW.LastName)
		WHERE NEW.Id = ExamOwnerId;
	END//

DELIMITER ;

COMMIT;