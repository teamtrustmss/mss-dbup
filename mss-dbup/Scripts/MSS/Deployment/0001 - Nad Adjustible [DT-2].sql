ALTER TABLE MssPlatformDb.dbo_Activities ADD IsNADadjustable TINYINT(1) DEFAULT 0 NOT NULL;

--  Auto-generated SQL script #202202081637
INSERT INTO MssPlatformDb.dbo_ApplicationSettings (`Key`,ContainerName,Value,Comment)
	VALUES ('ExamsNADShowTime','Presentation.MssPortal','TRUE','Defines output format for NextActionDate on gridAllExamsView');
