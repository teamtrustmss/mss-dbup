START TRANSACTION;

	ALTER TABLE dbo_OrderExamsReportsData ADD SortCode varchar(100) NULL;
	ALTER TABLE dbo_OrderExamsReportsDataNew ADD SortCode varchar(100) NULL;
	ALTER TABLE dbo_OrderExamsReportsData ADD AccountNumber varchar(100) NULL;
	ALTER TABLE dbo_OrderExamsReportsDataNew ADD AccountNumber varchar(100) NULL;
	
	INSERT INTO reporting_Fields (Id,Name,PropertyName,PropertyTypeId,CreateTime,ModifyTime,IsActive) VALUES
		('51b97cfa-3899-4b7e-bfb1-0eda12383cbc','ME Sort Code','SortCode',0,'2023-02-21 14:00:00','2023-02-21 14:00:00',1);
	
	INSERT INTO reporting_Fields (Id,Name,PropertyName,PropertyTypeId,CreateTime,ModifyTime,IsActive) VALUES
		('b709e7b7-1b43-40d0-a351-0368a26b94a8','ME Account Number','AccountNumber',0,'2023-02-21 14:00:00','2023-02-21 14:00:00',1);
		
	DROP PROCEDURE IF EXISTS `dbo_UpdateOrderExamsReportsData`;
	
DELIMITER $$

	CREATE PROCEDURE `dbo_UpdateOrderExamsReportsData`()
    MODIFIES SQL DATA
	BEGIN
		 DECLARE `should_rollback` BOOL DEFAULT FALSE;
		 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `should_rollback` = TRUE;
			 SET autocommit = 0;
			 
			 START TRANSACTION;
	 
			 DELETE FROM dbo_OrderExamsReportsData;
			 drop table if exists orderExamTempData;
			 SET @@group_concat_max_len = 256000;
			 create temporary table orderExamTempData select orderExam.`Id` as orderExamId,
					 (SELECT GROUP_CONCAT(NULLIF(b.`Code`,'') ORDER BY E.CreateTime ASC, b.Code ASC separator ', ')
					                     FROM dbo_OrderExamActivities E
					                     INNER JOIN dbo_Activities b ON E.ActivityId = b.Id
					                     WHERE E.OrderExamId = orderExam.Id
					                     GROUP BY E.OrderExamId)                         AS ActivityDescriptionsAll,
					 (SELECT ae.EmailAddress FROM dbo_ApplicantEmails ae
										 INNER JOIN dbo_Applicants a ON ae.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id
										 ORDER BY ae.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantEmail,
					 (SELECT ap.`Number` FROM dbo_ApplicantPhones ap
										 INNER JOIN dbo_Applicants a ON ap.ApplicantId = a.Id
										 WHERE a.Id = applicant.Id AND ap.PhoneType = 3
										 ORDER BY ap.CreateTime DESC
										 LIMIT 1)                                        AS ApplicantMobileNumber,
					 (SELECT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
						 FROM dbo_Applicants a 
						 LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
						 WHERE applicant.Id = a.Id 
							 AND aa.IsPreferred = 1
						 GROUP BY a.Id)                                  AS ApplicantPostCode,
						 ocOrder.Id                                          AS OrderId,
						 ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,    
						 ocOrder.OrderingCustomerId,
						 ocOrder.OrderingCustomerTeamId,
						 ocOrder.OrderingChannelId                           AS Origin,
						 ocOrder.CoverTypeId                                 AS TypeOfCover,
						 ocOrder.PolicyNumber                                AS PolicyNumber,				
						 (SELECT CASE WHEN NULLIF(AssignedToPrefix, '') IS NULL 
								 THEN AssignedTo 
								 ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo)
								 END
						 from dbo_OrderExamActivities OEA 
						 where OEA.OrderExamId = orderExam.Id 
						 order by CreateTime desc, ActivityOrder desc
						 LIMIT 1)                                            AS AssignedTo,
					 (SELECT CASE
						 WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 THEN 
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 WHEN examiner.ExaminerTypeId = 4 THEN
								 (SELECT GROUP_CONCAT( DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 ELSE '' 
					 END)                                                AS Skill,
					 (SELECT CASE
						 WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 
							 THEN 
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 WHEN examiner.ExaminerTypeId = 4 
							 THEN
								 (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
								 FROM dbo_OrderExamRequirements b
								 INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
								 INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
								 INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
								 WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
								 GROUP BY orderExam.Id)
						 ELSE '' 
					 END)                                                AS Qualification,
					 oc.CompanyName                                      AS OrderingCustomer,
					 oc.NextChargeReview                            	 AS NextChargeReview,
					 oc.NextChargeReviewComments                         AS NextChargeReviewComments,
					 oc.ContractStartDate                          		 AS ContractStartDate,
					 oc.AccountManager                          		 AS AccountManager,
					 CONCAT(applicant.FirstName, ' ', applicant.LastName) AS ApplicantName,
					 applicant.DateOfBirth                               AS ApplicantDoB,
					 (SELECT a.`Name` FROM dbo_Genders a 
						 WHERE a.Id = applicant.GenderId)                AS ApplicantGender,
					 applicant.LastName                                  AS ApplicantLastName,
					 applicant.FirstName                                 AS ApplicantFirstName,
					 examType.`Name`                                     AS ExamType,
                     IFNULL(dbo_GetTimeServiceForActivity ((SELECT Id
                         FROM dbo_OrderExamActivities E
							WHERE E.OrderExamId = orderExam.`Id`
                         ORDER BY E.CreateTime DESC
                         LIMIT 1), NULL), 0)                             AS TimeService,
					 0 													 AS TotalTimeService,
					 (SELECT IF(orderExam.`StatusId` = 32 OR orderExam.`StatusId` = 64 OR orderExam.`StatusId` = 16384,
						 IFNULL((SELECT InactiveTimeService 
								 FROM grid_AllOrderExams gaoe
								 WHERE gaoe.Id = orderExam.`Id`), 0),
						 IFNULL(dbo_GetInactiveTimeServiceForExam (orderExam.`Id`), 0))
				     ) 													 AS InactiveTimeService,
					 li.`Name`                                           AS LifeIndicator
					 from dbo_OrderExams orderExam
					 INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId
					 INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId
					 INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id
					 INNER JOIN dbo_OcExamTypes examType ON orderExam.ExamTypeId = examType.Id
					 LEFT JOIN dbo_Examiners examiner ON orderExam.AssignedExaminerId = examiner.Id
					 LEFT JOIN dbo_LifeIndicators li ON li.`Id` = ocOrder.`LifeIndicatorId`
					 WHERE ocOrder.IsAnonymised = 0;
			 
			 SET @@group_concat_max_len = 256000;
			 INSERT INTO dbo_OrderExamsReportsData 
			 (
				 ActivityDescriptionsAll,AddressLine1,AddressLine2,AddressLine3,ApplicantDoB,ApplicantEmail,ApplicantFirstName,
				 ApplicantGender,ApplicantLastName,ApplicantMobileNumber,ApplicantName,ApplicantPostCode,AppointmentScheduled, 
				 AssignedCSR,AssignedTo,AssignedToPrefix,BestContact,BilledDate,City,ClinicName,CreatorName,
				 CsrFirstName,CsrLastName,ExaminerFirstName,ExaminerLastName,NursePostCode,
				 ExaminerPrepaymentFlag,ExamOwnerId,ExamPostCode,ExamType,GpName,GpEmail,GPPracticePostCode,GroupPractice,GpPaymentType,GpAccountNo, GpSortCode,PayeeEmailAddress, HighNetWorth,Id,
				 InvoiceReferenceNumber,IsActive,IsUrgentCase,LatestActivityComment,
				 LatestActivityDescription,MiscFeeDescription,MiscChargeDescription,OcContact,OcServiceDescription,
				 OrderDate,OrderExamNumber,OrderId,OrderingCustomer, NextChargeReview, NextChargeReviewComments, ContractStartDate, AccountManager, OrderingCustomerTeam,OrderingCustomerTeamMemberId,Origin,PayeeName,PolicyNumber,PracticeName,Qualification,Requirement,
				 RequirementProblemReason,RequirementStatus,Skill,`Status`,TypeOfCover,CreateTime,ExamLocation, ExaminerEmail,GPPracticeFax,TimeService,TotalTimeService,InactiveTimeService,
				 LabFeeTotal,MeFeeTotal,MiscFeeTotal,MiscCharge, OcCharge, LifeIndicator, BankName, SortCode, AccountNumber
			 )
			 SELECT
			         temp.ActivityDescriptionsAll,
					 generalPractitioner.AddressLine1                    AS AddressLine1,
					 generalPractitioner.AddressLine2                    AS AddressLine2,
					 generalPractitioner.AddressLine3                    AS AddressLine3,    
					 temp.ApplicantDoB,  
					 temp.ApplicantEmail,
					 temp.ApplicantFirstName,
					 temp.ApplicantGender,
					 temp.ApplicantLastName,
					 temp.ApplicantMobileNumber,
					 temp.ApplicantName,
					 temp.ApplicantPostCode,
					 appointment.`Date`                                  AS AppointmentScheduled,
					 CONCAT(examOwner.FirstName, ' ', examOwner.LastName) AS AssignedCSR,
					 temp.AssignedTo,
					 (SELECT AssignedToPrefix 
						 from dbo_OrderExamActivities OEA 
						 where OEA.OrderExamId = orderExam.Id 
						 order by CreateTime desc, ActivityOrder desc
						 LIMIT 1)                                        AS AssignedToPrefix,
					 generalPractitioner.BestContact                     AS BestContact,
					 (SELECT a.CreateTime FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
						 WHERE a.OrderExamId = orderExam.Id AND a.ToExamState = 16
						 ORDER BY a.CreateTime
						 LIMIT 1)                                        AS BilledDate,
					 generalPractitioner.Town                            AS City,
					 clinic.`Name`                                       AS ClinicName,
					 (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
						 FROM dbo_OrderExamActivities OEA 
						 INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
						 WHERE OEA.PreviousActivityId IS null AND OEA.OrderExamId= orderExam.Id
						 ORDER BY OEA.CreateTime ASC 
						 LIMIT 1)                                        AS CreatorName,
					 examOwner.FirstName                                 AS CsrFirstName,
					 examOwner.LastName                                  AS CsrLastName,
					 medicalExaminer.FirstName                           AS ExaminerFirstName,
					 medicalExaminer.LastName                            AS ExaminerLastName,
					 (SELECT ExAddress.PostCode
					 	FROM dbo_ExaminerAddresses ExAddress
					 		WHERE ExAddress.ExaminerId = orderExam.AssignedExaminerId
					 		AND ExAddress.ExaminerAddressTypeId = 0
					 		AND orderExam.ExamTypeId = 1
					 		ORDER BY ExAddress.CreateTime DESC
					 		LIMIT 1)				 					 AS NursePostCode,
					 (SELECT CASE (SELECT COUNT(*) FROM dbo_OrderExamActivities a
										 INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
										 INNER JOIN dbo_OrderExams c ON a.OrderExamId = c.Id
										 WHERE c.Id = orderExam.Id AND c.ExamTypeId = 4 
											 AND b.`Code` IN('300'))
										 WHEN 0 
										 THEN 'No' 
										 ELSE 'Yes' 
										 END)                            AS ExaminerPrepaymentFlag,
					 examOwner.Id                                        AS ExamOwnerId,
					 appointment.PostCode                                AS ExamPostCode,
					 temp.ExamType,
					 TRIM(CONCAT(
					 generalPractitioner.Title, 
					 ' ', 
					 generalPractitioner.FirstName, 
					 ' ', 
					 generalPractitioner.LastName))                      AS GpName,
					 generalPractitioner.Email                           AS GpEmail,
					 generalPractitioner.PostCode                        AS GPPracticePostCode,
					 gpPracticeName.SurgeryName                          AS GroupPractice,                 
					 (CASE
							WHEN generalPractitioner.PaymentType  = 1 THEN "Bacs"
							WHEN generalPractitioner.PaymentType = 2 THEN "Cheque"
							ELSE "-"
						END)  AS GpPaymentType,
					 generalPractitioner.AccountNo                       AS GpAccountNo,
					 generalPractitioner.SortCode                        AS GpSortCode,
					 generalPractitioner.PayeeEmailAddress				 AS PayeeEmailAddress,
					 (Select IFNULL(
						 (SELECT 1 From dbo_OrderExamRequirements R  
						 WHERE orderExam.Id = R.OrderExamId 
						 AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
						 AND R.IsDeactivated = 0 LIMIT 1), 0) )          AS HighNetWorth,
					 orderExam.Id                                        AS Id,
					 generalPractitioner.InvoiceReferenceNumber          AS InvoiceReferenceNumber,
					 orderExam.IsActive                                  AS IsActive,
					 orderExam.IsUrgent                                  AS IsUrgentCase,
					 (SELECT E.Note 
						 FROM dbo_OrderExamActivities E 
						 WHERE E.OrderExamId=orderExam.Id 
						 ORDER BY E.CreateTime DESC, E.ActivityOrder desc
						 LIMIT 1)                                        AS LatestActivityComment,
					 (SELECT b.`Description` 
						 FROM dbo_OrderExamActivities E 
						 INNER JOIN dbo_Activities b ON E.ActivityId = b.Id 
						 WHERE E.OrderExamId=orderExam.Id 
						 ORDER BY E.CreateTime DESC, E.ActivityOrder desc
						 LIMIT 1)                                        AS LatestActivityDescription,
					 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
						 FROM dbo_MiscellaneousCharges a
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 2
						 ORDER BY a.Description
					 )                                                   AS MiscFeeDescription,
					 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
						 FROM dbo_MiscellaneousCharges a
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 1
						 ORDER BY a.Description
					 )                         							 AS MiscChargeDescription,
					 IFNULL(ocTeamMember.`Name`, '')                     AS OcContact,
					 (SELECT GROUP_CONCAT(DISTINCT E.`Description` ORDER BY REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(E.Description,'-',''), '(', ''), ')', ''), '/', ''), '''', '')  ASC separator ', ' )
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.StatusId NOT IN(3,4,5) 
							 AND E.IsDeactivated = 0
				     )                            						 AS OcServiceDescription,
					 orderExam.CreateTime                                AS OrderDate,
					 orderExam.ExamCustomId                              AS OrderExamNumber,
					 temp.OrderId,
					 temp.OrderingCustomer,
					 temp.NextChargeReview,
					 temp.NextChargeReviewComments,
					 temp.ContractStartDate,
					 temp.AccountManager,
					 team.`Name`                                         AS OrderingCustomerTeam,
					 temp.OrderingCustomerTeamMemberId,                           
					 temp.Origin,
					 generalPractitioner.PayeeName                       AS PayeeName,
					 temp.PolicyNumber,
					 generalPractitioner.PracticeName                    AS PracticeName,
					 temp.Qualification,
					 (SELECT GROUP_CONCAT(DISTINCT E.`Code` SEPARATOR ', ')
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id 
							 AND E.IsDeactivated = 0
						 GROUP BY E.OrderExamId)                         AS Requirement,
					 (SELECT GROUP_CONCAT(a.CustomDescription ORDER BY a.ActivityOrder ASC SEPARATOR '; ')
					     FROM dbo_OrderExamActivities a
					     INNER JOIN dbo_OrderExamRequirementActivities oeda ON oeda.Id = a.Id
						 INNER JOIN dbo_OrderExamRequirements d ON oeda.ExamRequirementId = d.Id
						 WHERE a.OrderExamId = orderExam.Id
							 AND d.StatusId = 1 
							 AND d.IsDeactivated = 0
						 GROUP BY a.OrderExamId) 					 	 AS RequirementProblemReason,
					 (SELECT GROUP_CONCAT(DISTINCT E.StatusId SEPARATOR ', ')
						 FROM dbo_OrderExamRequirements E
						 WHERE E.OrderExamId = orderExam.Id
							 AND E.IsDeactivated = 0
						 GROUP BY E.OrderExamId)                         AS RequirementStatus,
					 temp.Skill,
					 (SELECT a.`Name` FROM dbo_OrderExamStatusTypes a 
						 WHERE a.Id = orderExam.StatusId)                AS `Status`,
					 temp.TypeOfCover,
					 NOW()                                               AS CreateTime,
					 TRIM(CONCAT(
						 IFNULL(appointment.AddressLine1, ''),
						 IFNULL(CONCAT(appointment.AddressLine2, ', '), ''),
						 IFNULL(CONCAT(appointment.AddressLine3, ', '), ''),
						 IFNULL(CONCAT(appointment.City, ', '), ''),
						 IFNULL(CONCAT(appointment.PostCode, ', '), ''),
						 IFNULL(CONCAT(appointment.Country, ', '), '')           
					 ))                                                  AS ExamLocation,
					 medicalExaminer.PreferredEmail                      AS ExaminerEmail,
					 generalPractitioner.Fax                             AS GPPracticeFax,
					 temp.TimeService,
					 temp.TotalTimeService,
					 temp.InactiveTimeService,
					 0 AS LabFeeTotal,
					 0 AS MeFeeTotal,
					 0 AS MiscFeeTotal,
					 0 AS MiscCharge,
					 0 AS OcCharge,
					 temp.LifeIndicator,
					 dbo_Bank.Name 										 AS BankName,
					 EBA.SortCode 										 AS SortCode,
					 EBA.AccountNumber 									 AS AccountNumber
				 FROM dbo_OrderExams orderExam
					 inner join orderExamTempData temp on temp.orderExamId = orderExam.Id
					 LEFT JOIN dbo_OrderingCustomerTeams team ON temp.OrderingCustomerTeamId = team.Id
					 LEFT JOIN dbo_OrderExamAppointments appointment ON orderExam.Id = appointment.Id
					 LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_GeneralPractitioners generalPractitioner ON generalPractitioner.Id = orderExam.Id
					 LEFT JOIN dbo_GpPractices gpPracticeName ON gpPracticeName.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId
					 LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId
					 LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId
					 LEFT JOIN dbo_ExaminerBankAccount EBA ON orderExam.AssignedExaminerId = EBA.ExaminerId
					 LEFT JOIN dbo_Bank ON EBA.BankId = dbo_Bank.Id 
					 LEFT JOIN dbo_GridOrderingCustomerTeamMembers ocTeamMember ON temp.OrderingCustomerTeamMemberId = ocTeamMember.Id;

				 UPDATE dbo_OrderExamsReportsData
                      SET TotalTimeService = TimeService;			 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT  OrderExamId, SUM(IFNULL(A.ExaminerFee, 0)) SUMExaminerFee
					 FROM (
						 SELECT a.Id, a.OrderExamId, a.ExaminerFee
							 FROM dbo_OrderExamRequirements a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 LEFT JOIN dbo_Examiners c ON b.AssignedExaminerId = c.Id AND c.ExaminerTypeId != 5
						 INNER JOIN dbo_AllowedExaminers ae ON a.MedicalExaminationId = ae.MedicalExaminationId 
						 AND (SELECT COUNT(*)    FROM dbo_AllowedExaminers aa WHERE aa.ExaminerType = 5 AND aa.MedicalExaminationId = a.MedicalExaminationId AND a.OrderExamId=BB.id) = 0    
						 WHERE a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType != 5
								 AND a.OrderExamId=BB.id
						 GROUP BY a.Id, a.OrderExamId, a.ExaminerFee
					 ) A
					 GROUP BY OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MeFeeTotal = CC.SUMExaminerFee;
				 
				 
				 UPDATE dbo_OrderExamsReportsData
				 JOIN (
					 SELECT  OrderExamId, SUM(IFNULL(SS.ExaminerFee,0)) SUMExaminerFee
					 FROM (
						 SELECT DISTINCT a.Id, a.OrderExamId, a.ExaminerFee
							 FROM dbo_OrderExamRequirements a 
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_MedicalExaminations m ON a.MedicalExaminationId = m.Id 
						 INNER JOIN dbo_AllowedExaminers ae ON m.Id = ae.MedicalExaminationId
						 INNER JOIN dbo_ExaminationFees f ON f.MedicalExaminationId = m.Id
						 INNER JOIN dbo_LaboratoryExaminations e ON f.Id = e.Id
						 WHERE a.OrderExamId = BB.Id AND a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType = 5
						 ) SS
						 GROUP BY SS.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
					 SET LabFeeTotal=CC.SUMExaminerFee;
	 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
						 SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
							 FROM dbo_MiscellaneousCharges a 
							 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
							 INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
							 WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 2
							 GROUP BY a.OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MiscFeeTotal=CC.SUMCharge;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
						 SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
							 FROM dbo_MiscellaneousCharges a 
							 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
							 INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
							 WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 1
							 GROUP BY a.OrderExamId
				 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET MiscCharge = CC.SUMCharge;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT SUM(IFNULL(a.OcCharge,0))  SUMOcCharge, a.OrderExamId 
						 FROM dbo_OrderExamRequirements a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
						 WHERE a.OrderExamId = BB.Id AND a.StatusId != 5 AND a.IsDeactivated = 0
						 GROUP BY a.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET OcCharge = CC.SUMOcCharge;
				 
				 
				 WITH cte AS ( SELECT NextActionDate, OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) AS rn FROM dbo_OrderExamActivities)
				 UPDATE dbo_OrderExamsReportsData oerd
				 JOIN cte ON cte.rn = 1 AND cte.OrderExamId = oerd.Id
				 SET oerd.NextActionDate = cte.NextActionDate;
				 
				 
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN (
					 SELECT MIN(a.CreateTime) CreateTime, a.OrderExamId 
						 FROM dbo_OrderExamActivities a
						 INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
						 WHERE a.OrderExamId = BB.Id AND a.ToExamState IN(32,64,16384)
						 GROUP BY a.OrderExamId
					 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET CompletedDate = DATE_FORMAT(CC.CreateTime, "%Y-%m-%d %H:%i:%s");
				 
				 
				 WITH cte AS 
					 (SELECT CONCAT(b.Description, CASE WHEN a.Note IS NULL THEN '' ELSE CONCAT(': ', a.Note) END) topNote, 
					 a.OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) 
					 AS rn 
					 FROM dbo_OrderExamActivities a 
					 INNER JOIN dbo_Activities b  ON a.ActivityId = b.Id 
					 WHERE a.ToExamState = 32)
				 UPDATE dbo_OrderExamsReportsData 
				 JOIN cte ON cte.rn=1 AND cte.OrderExamId = dbo_OrderExamsReportsData.Id
				 SET CancellationReason = cte.topNote;
			 
			 IF `should_rollback` THEN
				 ROLLBACK;
			 ELSE
				 COMMIT;
			 END IF;
			 SET autocommit = 1;
		END $$

DELIMITER ;

	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `dbo_GridAllOrderExamsReports` AS
	SELECT
	    `ActivityCommentsAll` AS `ActivityCommentsAll`,
	    `ActivityDescriptionsAll` AS `ActivityDescriptionsAll`,
	    `AddressLine1` AS `AddressLine1`,
	    `AddressLine2` AS `AddressLine2`,
	    `AddressLine3` AS `AddressLine3`,
	    `ApplicantDoB` AS `ApplicantDoB`,
	    `ApplicantEmail` AS `ApplicantEmail`,
	    `ApplicantFirstName` AS `ApplicantFirstName`,
	    `ApplicantGender` AS `ApplicantGender`,
	    `ApplicantLastName` AS `ApplicantLastName`,
	    `ApplicantMobileNumber` AS `ApplicantMobileNumber`,
	    `ApplicantName` AS `ApplicantName`,
	    `ApplicantPostCode` AS `ApplicantPostCode`,
	    `AppointmentScheduled` AS `AppointmentScheduled`,
	    `AssignedCSR` AS `AssignedCSR`,
	    `AssignedTo` AS `AssignedTo`,
	    `AssignedToPrefix` AS `AssignedToPrefix`,
	    `BestContact` AS `BestContact`,
	    `BilledDate` AS `BilledDate`,
	    `CancellationReason` AS `CancellationReason`,
	    `City` AS `City`,
	    `ClinicName` AS `ClinicName`,
	    `CompletedDate` AS `CompletedDate`,
	    `CreatorName` AS `CreatorName`,
	    `CsrFirstName` AS `CsrFirstName`,
	    `CsrLastName` AS `CsrLastName`,
	    `ExaminerFirstName` AS `ExaminerFirstName`,
	    `ExaminerLastName` AS `ExaminerLastName`,
	    `ExaminerPrepaymentFlag` AS `ExaminerPrepaymentFlag`,
	    `ExamOwnerId` AS `ExamOwnerId`,
	    `ExamPostCode` AS `ExamPostCode`,
	    `ExamType` AS `ExamType`,
	    `GpName` AS `GpName`,
	    `GPPracticePostCode` AS `GPPracticePostCode`,
	    `GroupPractice` AS `GroupPractice`,
	    `HighNetWorth` AS `HighNetWorth`,
	    `Id` AS `Id`,
	    `InvoiceReferenceNumber` AS `InvoiceReferenceNumber`,
	    `IsActive` AS `IsActive`,
	    `IsUrgentCase` AS `IsUrgentCase`,
	    `LabFeeTotal` AS `LabFeeTotal`,
	    `LatestActivityComment` AS `LatestActivityComment`,
	    `LatestActivityDescription` AS `LatestActivityDescription`,
	    `MeFeeTotal` AS `MeFeeTotal`,
	    `MiscFeeDescription` AS `MiscFeeDescription`,
	    `MiscFeeTotal` AS `MiscFeeTotal`,
	    `MiscCharge` AS `MiscCharge`,
	    `MiscChargeDescription` AS `MiscChargeDescription`,
	    `NextActionDate` AS `NextActionDate`,
	    `OcCharge` AS `OcCharge`,
	    `OcContact` AS `OcContact`,
	    `OcServiceDescription` AS `OcServiceDescription`,
	    `OrderDate` AS `OrderDate`,
	    `OrderExamNumber` AS `OrderExamNumber`,
	    `OrderId` AS `OrderId`,
	    `OrderingCustomer` AS `OrderingCustomer`,
	    `OrderingCustomerTeam` AS `OrderingCustomerTeam`,
	    `OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
	    `Origin` AS `Origin`,
	    `PayeeName` AS `PayeeName`,
	    `PolicyNumber` AS `PolicyNumber`,
	    `ClientReference2` AS `ClientReference2`,
	    `PracticeName` AS `PracticeName`,
	    `Qualification` AS `Qualification`,
	    `Requirement` AS `Requirement`,
	    `RequirementProblemReason` AS `RequirementProblemReason`,
	    `RequirementStatus` AS `RequirementStatus`,
	    `Skill` AS `Skill`,
	    `Status` AS `Status`,
	    `TimeService` AS `TimeService`,
	    `TotalTimeService` AS `TotalTimeService`,
	    `InactiveTimeService` AS `InactiveTimeService`,
	    `TypeOfCover` AS `TypeOfCover`,
	    `CreateTime` AS `CreateTime`,
	    `ExaminerEmail` AS `ExaminerEmail`,
	    `ExamLocation` AS `ExamLocation`,
	    `GpEmail` AS `GpEmail`,
	    `GPPracticeFax` AS `GPPracticeFax`,
	    `LifeIndicator` AS `LifeIndicator`,
	    `GpPaymentType` AS `GpPaymentType`,
	    `GpAccountNo` AS `GpAccountNo`,
	    `GpSortCode` AS `GpSortCode`,
	    `NextChargeReview` AS `NextChargeReview`,
		`NextChargeReviewComments` AS `NextChargeReviewComments`,
		`NursePostCode` AS `NursePostCode`,
		`ContractStartDate` AS `ContractStartDate`,
		`AccountManager` AS `AccountManager`,
	    `BankName` AS `BankName`,
	    `PayeeEmailAddress` AS `PayeeEmailAddress`,
		`AccountNumber` AS `AccountNumber`,
	    `SortCode` AS `SortCode`
	FROM
	    `dbo_OrderExamsReportsData`;

COMMIT;