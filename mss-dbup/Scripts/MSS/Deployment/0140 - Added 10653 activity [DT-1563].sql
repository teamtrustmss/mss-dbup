START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime,AllowWebhookNotifications) VALUES
		('e913b76e-b85f-ef11-bb4a-0a61967cf1f6','10653','Multiple DNAs - please create a GPR case and cancel the TI case.',8,NULL,NULL,1,0,0,14602,2,0,NULL,0,0,1,2,NULL,NOW(),NOW(),0);

COMMIT;