START TRANSACTION;

	CREATE TABLE `dbo_KitTypes` (
	  `Id` varchar(64) NOT NULL,
	  `Name` varchar(255) NOT NULL,
	  `Weight` decimal(18,2) NULL DEFAULT 0,
	  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
	  `CreateTime` datetime(6) NOT NULL,
	  `ModifyTime` datetime(6) NOT NULL,
	  PRIMARY KEY (`Id`),
	  UNIQUE KEY `Id` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

	CREATE TABLE `dbo_KitTypes_SampleRequirements` (
	  `KitTypeId` varchar(64) NOT NULL,
	  `SampleRequirementId` varchar(64) NOT NULL,
	  PRIMARY KEY (`KitTypeId`,`SampleRequirementId`),
	  KEY `FK_KitTypesSampleRequirements_SampleRequirements` (`SampleRequirementId`),
	  CONSTRAINT `FK_KitTypesSampleRequirements_KitType` FOREIGN KEY (`KitTypeId`) REFERENCES `dbo_KitTypes` (`Id`),
	  CONSTRAINT `FK_KitTypesSampleRequirements_SampleRequirements` FOREIGN KEY (`SampleRequirementId`) REFERENCES `dbo_SampleRequirements` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

	CREATE TABLE `dbo_OrderingCustomerKitTypes` (
	  `Id` varchar(64) NOT NULL,
	  `OrderingCustomerId` varchar(64) NOT NULL,
	  `KitTypeId` varchar(64) NOT NULL,
	  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
	  `CreateTime` datetime(6) NOT NULL,
	  `ModifyTime` datetime(6) NOT NULL,
	  PRIMARY KEY (`Id`),
	  UNIQUE KEY `Id` (`Id`),
	  KEY `FK_OrderingCustomerKitTypes_KitTypes` (`KitTypeId`),
	  KEY `FK_OrderingCustomerKitTypes_OrderingCustomers` (`OrderingCustomerId`),
	  CONSTRAINT `FK_OrderingCustomerKitTypes_MedicalExaminations` FOREIGN KEY (`KitTypeId`) REFERENCES `dbo_KitTypes` (`Id`),
	  CONSTRAINT `FK_OrderingCustomerKitTypes_OrderingCustomers` FOREIGN KEY (`OrderingCustomerId`) REFERENCES `dbo_OrderingCustomers` (`Id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

	CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridKitTypes` AS
	SELECT
		`k`.`Id` 						AS `Id`,
		`k`.`IsActive` 					AS `IsActive`,
		`k`.`CreateTime` 				AS `CreateTime`,
		`k`.`Name` 						AS `Name`,
		`k`.`Weight` 					AS `Weight`,
		(
			SELECT GROUP_CONCAT(DISTINCT `s`.`Vacutainer` ORDER BY `s`.`Vacutainer` ASC SEPARATOR ', ')
			FROM (`MssPlatformDb`.`dbo_SampleRequirements` `s`
			LEFT JOIN `MssPlatformDb`.`dbo_KitTypes_SampleRequirements` `x`
			ON ((`x`.`SampleRequirementId` = `s`.`Id`)))
			WHERE (`x`.`KitTypeId` = `k`.`Id`)
			GROUP BY `x`.`KitTypeId`
		) 								AS `ConcatenatedVacutainers`
	FROM `MssPlatformDb`.`dbo_KitTypes` `k`
	LEFT JOIN `MssPlatformDb`.`dbo_OrderingCustomerKitTypes` `ockt`
	ON (`k`.`Id` = `ockt`.`KitTypeId`)
	WHERE `ockt`.`KitTypeId` IS NULL;
	
	CREATE OR REPLACE
	ALGORITHM = UNDEFINED VIEW `MssPlatformDb`.`dbo_GridOrderingCustomerKitTypes` AS
	SELECT
		`ok`.`Id` 						AS `Id`,
		`ok`.`OrderingCustomerId` 		AS `OrderingCustomerId`,
		`ok`.`KitTypeId` 				AS `KitTypeId`,
		`ok`.`IsActive` 				AS `IsActive`,
		`ok`.`CreateTime` 				AS `CreateTime`,
		`k`.`Name` 						AS `Name`,
		`k`.`Weight` 					AS `Weight`,
		(
			SELECT GROUP_CONCAT(DISTINCT `s`.`Vacutainer` ORDER BY `s`.`Vacutainer` ASC SEPARATOR ', ')
			FROM (`MssPlatformDb`.`dbo_SampleRequirements` `s`
			LEFT JOIN `MssPlatformDb`.`dbo_KitTypes_SampleRequirements` `x`
			ON ((`x`.`SampleRequirementId` = `s`.`Id`)))
				WHERE (`x`.`KitTypeId` = `k`.`Id`)
			GROUP BY `x`.`KitTypeId`
		)								AS `ConcatenatedVacutainers`
	FROM `MssPlatformDb`.`dbo_OrderingCustomerKitTypes` `ok`
	INNER JOIN `MssPlatformDb`.`dbo_KitTypes` `k` ON `k`.`Id` = `ok`.`KitTypeId`;

COMMIT;