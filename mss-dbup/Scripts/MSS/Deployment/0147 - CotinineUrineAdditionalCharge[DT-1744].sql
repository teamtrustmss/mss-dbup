START TRANSACTION;

	INSERT INTO dbo_MedicalExaminations (Id,Code,Description,IsLinkedDocument,IsElectronicResults,ExamQuestionnaireAndKitToBeSentToGp,ExamQuestionnaireRequired,FastingRequired,GpDetailsRequired,HivFormRequired,IsPdfResults,SampleSentToLab,SignedAmraRequired,AutomaticAdditionalChargeId,AssigmentTypeId,IsAutomaticallyAddedToOrderingCustomer,IsActive,OrderForAutomaticChargeList,ServiceCodeAddedActivity,IsSendToExaminer,CreateTime,ModifyTime,InvoiceProcessing,IsRequiresEssentialDocument,CareTypeId) VALUES
		('fa70cf4a-dda0-402c-a64b-57b8116ce75d','COTININE URINE','Cotinine Urine',0,0,0,0,0,0,0,0,0,0,NULL,0,1,1,50,NULL,0,NOW(),NOW(),0,0,0);

	INSERT INTO dbo_MedicalExamination_Capabilities (ExaminationId,CapabilityId) VALUES
		('fa70cf4a-dda0-402c-a64b-57b8116ce75d','8dd0f741-d911-41ce-a75a-b47d5c8f48c0');

	INSERT INTO dbo_AllowedExaminers (Id,DefaultOcCharge,ExaminerType,MedicalExaminationId,IsActive,CreateTime,ModifyTime) VALUES
		('93daea8b-b2ac-44cd-8997-d006642af7a5',0.00,2,'fa70cf4a-dda0-402c-a64b-57b8116ce75d',1,NOW(),NOW());
	
	INSERT INTO dbo_OrderingCustomerExaminations (Id, OrderingCustomerId, MedicalExaminationId, OcCode, OcDescription, IsActive, CreateTime, ModifyTime, IsDAV, DAV, FixedFeeThreshold, ChargingModelId, TiaraCode)
	SELECT 
		UUID(), o.Id, 'fa70cf4a-dda0-402c-a64b-57b8116ce75d', 'COTININE URINE', 'Cotinine Urine', 1, NOW(), NOW(), 0, 0.00, 0.00, 0, NULL
	FROM 
		dbo_OrderingCustomers o;
		
	INSERT INTO dbo_OrderingCustomerCharges (Id,Charge,IsChargeAsPercent,OcMedicalExaminationId,AllowedExaminerId,ServiceTypeId,IsActive,CreateTime,ModifyTime)
	SELECT
		UUID(), 0.00, 0, ocME.Id, '93daea8b-b2ac-44cd-8997-d006642af7a5', 2, 1, NOW(), NOW()
	FROM 
		dbo_OrderingCustomerExaminations ocME
		WHERE ocME.OcCode = 'COTININE URINE';

COMMIT;