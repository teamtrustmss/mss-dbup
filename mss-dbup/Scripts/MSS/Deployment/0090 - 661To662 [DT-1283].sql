START TRANSACTION;

	INSERT INTO dbo_Activities (Id,Code,Description,ActivityTypeId,MaxDaysLimit,DefaultNumberOfDays,VisibleToExaminer,VisibleToOrderingCustomer,AllowMedicalExaminerUse,AllowedOrderStatuses,NextActionDateCalculationMode,SuspendedTimeService,NextActivityId,CommentRequired,CalculateNADFromScheduledDate,IsActive,NotificationType,SystemActivityComment,CreateTime,ModifyTime) VALUES
		('626d2c4d-a177-4795-892e-8caba727656b','663','Essential Document Uploaded',7,NULL,NULL,1,1,0,48015,3,0,NULL,0,0,1,0,NULL,'2023-11-16 13:00:00','2023-11-16 13:00:00'),
		('4d537b49-381f-44be-984b-879884d5592e','662','Kit Sent',7,NULL,2,0,1,0,65536,1,0,NULL,0,0,1,0,NULL,'2023-11-16 13:00:00','2023-11-16 13:00:00');
		
COMMIT;