START TRANSACTION;

ALTER TABLE dbo_OrderingCustomers DROP COLUMN CancellationFeeWaived;
ALTER TABLE dbo_OrderingCustomers ADD DNAServiceCodeExclusion varchar(2000) NULL;
ALTER TABLE dbo_OrderingCustomers ADD IsPriceFixed tinyint(1) DEFAULT 0 NOT NULL;;
ALTER TABLE dbo_OrderingCustomers ADD DNAPercent int DEFAULT 50 NOT NULL;
ALTER TABLE dbo_OrderingCustomers ADD DNAFixedPrice decimal(18,2) DEFAULT 0 NOT NULL;

COMMIT;