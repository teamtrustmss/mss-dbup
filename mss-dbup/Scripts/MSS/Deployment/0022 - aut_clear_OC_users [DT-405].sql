START TRANSACTION;

DROP TABLE  IF EXISTS delete_OCUsers;
CREATE TEMPORARY TABLE delete_OCUsers
SELECT Id
FROM dbo_OrderingCustomers where CompanyName  LIKE 'AutTestCo%';

DELETE daa, dap, dae FROM dbo_Applicants da
LEFT JOIN dbo_ApplicantAddresses daa ON daa.ApplicantId = da.Id 
LEFT JOIN dbo_ApplicantEmails dae ON dae.ApplicantId = da.Id 
LEFT JOIN dbo_ApplicantPhones dap ON dap.ApplicantId = da.Id 
WHERE da.Id IN (SELECT Id FROM dbo_Orders do where do.OrderingCustomerId IN (SELECT Id from delete_OCUsers));

DELETE FROM dbo_Applicants da WHERE da.Id IN (SELECT Id FROM dbo_Orders do where do.OrderingCustomerId IN (SELECT Id from delete_OCUsers));

DELETE doer, doea, doeaa, dmc, dgp, doe FROM dbo_OrderExams doe 
LEFT JOIN dbo_OrderExamRequirements doer ON doer.OrderExamId  = doe.Id 
LEFT JOIN dbo_OrderExamActivities doea ON doea.OrderExamId = doe.Id 
LEFT JOIN dbo_OrderExamAppointments doeaa ON doeaa.Id = doe.Id 
LEFT JOIN dbo_MiscellaneousCharges dmc ON dmc.OrderExamId = doe.Id 
LEFT JOIN dbo_GeneralPractitioners dgp ON dgp.Id  = doe.Id 
WHERE doe.OrderId IN (SELECT do.Id  FROM dbo_Orders do WHERE do.OrderingCustomerId IN (SELECT Id from delete_OCUsers));

DELETE do FROM dbo_Orders do
WHERE do.OrderingCustomerId IN (SELECT Id from delete_OCUsers);

DELETE FROM dbo_OrderingCustomerTeams_OrderingCustomerTeamMembers doctoctm WHERE doctoctm.OrderingCustomerTeamId IN (SELECT Id FROM dbo_OrderingCustomerTeams doct WHERE doct.DivisionId IN (SELECT Id FROM dbo_OrderingCustomerDivisions WHERE OrderingCustomerId IN (SELECT Id FROM delete_OCUsers)));
DELETE FROM dbo_OrderingCustomerTeamMembers doctm WHERE doctm.Id IN (SELECT Id FROM delete_OCUsers);

DELETE docda, doct FROM dbo_OrderingCustomerDivisions docd
LEFT JOIN dbo_OrderingCustomerDivisionAddresses docda ON docda.DivisionId = docd.Id
LEFT JOIN dbo_OrderingCustomerTeams doct ON doct.DivisionId = docd.Id 
WHERE docd.OrderingCustomerId IN (SELECT Id FROM delete_OCUsers);

DELETE FROM dbo_OrderingCustomerDivisions docd WHERE docd.OrderingCustomerId IN (SELECT Id FROM delete_OCUsers);

DELETE docc FROM dbo_OrderingCustomerExaminations doce
LEFT JOIN dbo_OrderingCustomerCharges docc ON docc.OcMedicalExaminationId = doce.Id
WHERE doce.OrderingCustomerId IN (SELECT Id FROM delete_OCUsers);

DELETE FROM dbo_OrderingCustomerExaminations doce WHERE doce.OrderingCustomerId IN (SELECT Id FROM delete_OCUsers);

DELETE dad, doca, rrcd, rrll FROM dbo_OrderingCustomers doc
LEFT JOIN dbo_AssignmentDelays dad ON dad.OrderingCustomerId = doc.Id 
LEFT JOIN dbo_OrderingCustomerAddresses doca ON doca.OrderingCustomerId = doc.Id
LEFT JOIN reporting_ReportChartData rrcd ON rrcd.OrderingCustomerId = doc.Id 
LEFT JOIN reporting_ReportLoadLogs rrll ON rrll.OrderingCustomerId = doc.Id 
WHERE doc.Id IN (SELECT Id FROM delete_OCUsers);

DELETE FROM dbo_OrderingCustomers doc WHERE doc.Id IN (SELECT Id FROM delete_OCUsers);

DROP TABLE  delete_OCUsers;

COMMIT;