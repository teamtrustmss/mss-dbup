ALTER TABLE MssPlatformDb.dbo_GeneralPractitioners ADD PayeeEmailAddress varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

--  Auto-generated SQL script #202201271300
INSERT INTO MssPlatformDb.reporting_Fields (Id,Name,PropertyName,PropertyTypeId,CreateTime,ModifyTime,IsActive)
	VALUES ('4700DE80-E452-4CF5-89C8-81BE549DA1E0','GP Payee Email Address','PayeeEmailAddress',0,NOW(),NOW(),1);

ALTER TABLE MssPlatformDb.dbo_OrderExamsReportsData ADD PayeeEmailAddress varchar(255) NULL;


CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `dbo_GridAllOrderExamsReports` AS
select
    `dbo_OrderExamsReportsData`.`ActivityCommentsAll` AS `ActivityCommentsAll`,
    `dbo_OrderExamsReportsData`.`ActivityDescriptionsAll` AS `ActivityDescriptionsAll`,
    `dbo_OrderExamsReportsData`.`AddressLine1` AS `AddressLine1`,
    `dbo_OrderExamsReportsData`.`AddressLine2` AS `AddressLine2`,
    `dbo_OrderExamsReportsData`.`AddressLine3` AS `AddressLine3`,
    `dbo_OrderExamsReportsData`.`ApplicantDoB` AS `ApplicantDoB`,
    `dbo_OrderExamsReportsData`.`ApplicantEmail` AS `ApplicantEmail`,
    `dbo_OrderExamsReportsData`.`ApplicantFirstName` AS `ApplicantFirstName`,
    `dbo_OrderExamsReportsData`.`ApplicantGender` AS `ApplicantGender`,
    `dbo_OrderExamsReportsData`.`ApplicantLastName` AS `ApplicantLastName`,
    `dbo_OrderExamsReportsData`.`ApplicantMobileNumber` AS `ApplicantMobileNumber`,
    `dbo_OrderExamsReportsData`.`ApplicantName` AS `ApplicantName`,
    `dbo_OrderExamsReportsData`.`ApplicantPostCode` AS `ApplicantPostCode`,
    `dbo_OrderExamsReportsData`.`AppointmentScheduled` AS `AppointmentScheduled`,
    `dbo_OrderExamsReportsData`.`AssignedCSR` AS `AssignedCSR`,
    `dbo_OrderExamsReportsData`.`AssignedTo` AS `AssignedTo`,
    `dbo_OrderExamsReportsData`.`AssignedToPrefix` AS `AssignedToPrefix`,
    `dbo_OrderExamsReportsData`.`BestContact` AS `BestContact`,
    `dbo_OrderExamsReportsData`.`BilledDate` AS `BilledDate`,
    `dbo_OrderExamsReportsData`.`CancellationReason` AS `CancellationReason`,
    `dbo_OrderExamsReportsData`.`City` AS `City`,
    `dbo_OrderExamsReportsData`.`ClinicName` AS `ClinicName`,
    `dbo_OrderExamsReportsData`.`CompletedDate` AS `CompletedDate`,
    `dbo_OrderExamsReportsData`.`CreatorName` AS `CreatorName`,
    `dbo_OrderExamsReportsData`.`CsrFirstName` AS `CsrFirstName`,
    `dbo_OrderExamsReportsData`.`CsrLastName` AS `CsrLastName`,
    `dbo_OrderExamsReportsData`.`ExaminerFirstName` AS `ExaminerFirstName`,
    `dbo_OrderExamsReportsData`.`ExaminerLastName` AS `ExaminerLastName`,
    `dbo_OrderExamsReportsData`.`ExaminerPrepaymentFlag` AS `ExaminerPrepaymentFlag`,
    `dbo_OrderExamsReportsData`.`ExamOwnerId` AS `ExamOwnerId`,
    `dbo_OrderExamsReportsData`.`ExamPostCode` AS `ExamPostCode`,
    `dbo_OrderExamsReportsData`.`ExamType` AS `ExamType`,
    `dbo_OrderExamsReportsData`.`GpName` AS `GpName`,
    `dbo_OrderExamsReportsData`.`GPPracticePostCode` AS `GPPracticePostCode`,
    `dbo_OrderExamsReportsData`.`GroupPractice` AS `GroupPractice`,
    `dbo_OrderExamsReportsData`.`HighNetWorth` AS `HighNetWorth`,
    `dbo_OrderExamsReportsData`.`Id` AS `Id`,
    `dbo_OrderExamsReportsData`.`InvoiceReferenceNumber` AS `InvoiceReferenceNumber`,
    `dbo_OrderExamsReportsData`.`IsActive` AS `IsActive`,
    `dbo_OrderExamsReportsData`.`IsUrgentCase` AS `IsUrgentCase`,
    `dbo_OrderExamsReportsData`.`LabFeeTotal` AS `LabFeeTotal`,
    `dbo_OrderExamsReportsData`.`LatestActivityComment` AS `LatestActivityComment`,
    `dbo_OrderExamsReportsData`.`LatestActivityDescription` AS `LatestActivityDescription`,
    `dbo_OrderExamsReportsData`.`MeFeeTotal` AS `MeFeeTotal`,
    `dbo_OrderExamsReportsData`.`MiscFeeDescription` AS `MiscFeeDescription`,
    `dbo_OrderExamsReportsData`.`MiscFeeTotal` AS `MiscFeeTotal`,
    `dbo_OrderExamsReportsData`.`MiscCharge` AS `MiscCharge`,
    `dbo_OrderExamsReportsData`.`MiscChargeDescription` AS `MiscChargeDescription`,
    `dbo_OrderExamsReportsData`.`NextActionDate` AS `NextActionDate`,
    `dbo_OrderExamsReportsData`.`OcCharge` AS `OcCharge`,
    `dbo_OrderExamsReportsData`.`OcContact` AS `OcContact`,
    `dbo_OrderExamsReportsData`.`OcServiceDescription` AS `OcServiceDescription`,
    `dbo_OrderExamsReportsData`.`OrderDate` AS `OrderDate`,
    `dbo_OrderExamsReportsData`.`OrderExamNumber` AS `OrderExamNumber`,
    `dbo_OrderExamsReportsData`.`OrderId` AS `OrderId`,
    `dbo_OrderExamsReportsData`.`OrderingCustomer` AS `OrderingCustomer`,
    `dbo_OrderExamsReportsData`.`OrderingCustomerTeam` AS `OrderingCustomerTeam`,
    `dbo_OrderExamsReportsData`.`OrderingCustomerTeamMemberId` AS `OrderingCustomerTeamMemberId`,
    `dbo_OrderExamsReportsData`.`Origin` AS `Origin`,
    `dbo_OrderExamsReportsData`.`PayeeName` AS `PayeeName`,
    `dbo_OrderExamsReportsData`.`PolicyNumber` AS `PolicyNumber`,
    `dbo_OrderExamsReportsData`.`PracticeName` AS `PracticeName`,
    `dbo_OrderExamsReportsData`.`Qualification` AS `Qualification`,
    `dbo_OrderExamsReportsData`.`Requirement` AS `Requirement`,
    `dbo_OrderExamsReportsData`.`RequirementProblemReason` AS `RequirementProblemReason`,
    `dbo_OrderExamsReportsData`.`RequirementStatus` AS `RequirementStatus`,
    `dbo_OrderExamsReportsData`.`Skill` AS `Skill`,
    `dbo_OrderExamsReportsData`.`Status` AS `Status`,
    `dbo_OrderExamsReportsData`.`TimeService` AS `TimeService`,
    `dbo_OrderExamsReportsData`.`TotalTimeService` AS `TotalTimeService`,
    `dbo_OrderExamsReportsData`.`TypeOfCover` AS `TypeOfCover`,
    `dbo_OrderExamsReportsData`.`CreateTime` AS `CreateTime`,
    `dbo_OrderExamsReportsData`.`ExaminerEmail` AS `ExaminerEmail`,
    `dbo_OrderExamsReportsData`.`ExamLocation` AS `ExamLocation`,
    `dbo_OrderExamsReportsData`.`GpEmail` AS `GpEmail`,
    `dbo_OrderExamsReportsData`.`GPPracticeFax` AS `GPPracticeFax`,
    `dbo_OrderExamsReportsData`.`LifeIndicator` AS `LifeIndicator`,
    `dbo_OrderExamsReportsData`.`GpPaymentType` AS `GpPaymentType`,
    `dbo_OrderExamsReportsData`.`GpAccountNo` AS `GpAccountNo`,
    `dbo_OrderExamsReportsData`.`GpSortCode` AS `GpSortCode`,
	`dbo_OrderExamsReportsData`.`LastChargeReview` AS `LastChargeReview`,
    `MssPlatformDb`.`dbo_OrderExamsReportsData`.`BankName` AS `BankName`,
	`MssPlatformDb`.`dbo_OrderExamsReportsData`.`PayeeEmailAddress` AS `PayeeEmailAddress`
from
    `dbo_OrderExamsReportsData`;
	
 DROP PROCEDURE IF EXISTS dbo_UpdateOrderExamsReportsData;

delimiter //
 CREATE  PROCEDURE `MssPlatformDb`.`dbo_UpdateOrderExamsReportsData`()
     MODIFIES SQL DATA
BEGIN
     DECLARE `should_rollback` BOOL DEFAULT FALSE;
     DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `should_rollback` = TRUE;
         #comment DT-136  PayeeEmailAddress
         SET autocommit = 0;
         
         START TRANSACTION;
         
 
         DELETE FROM dbo_OrderExamsReportsData;
         drop table if exists orderExamTempData;
        SET @@group_concat_max_len = 256000;
         create temporary table orderExamTempData select orderExam.`Id` as orderExamId, 
 (SELECT GROUP_CONCAT(DISTINCT '~~~|~~~ ', NULLIF(E.Note,'') separator '')
                     FROM dbo_OrderExamActivities E
                     WHERE E.OrderExamId = orderExam.Id
                     GROUP BY E.OrderExamId)                          AS ActivityCommentsAll,
 (SELECT GROUP_CONCAT(DISTINCT '~~~|~~~ ', NULLIF(b.`Description`,'') separator '')
                     FROM dbo_OrderExamActivities E
                     INNER JOIN dbo_Activities b ON E.ActivityId = b.Id
                     WHERE E.OrderExamId = orderExam.Id
                     GROUP BY E.OrderExamId)                         AS ActivityDescriptionsAll,
 (SELECT ae.EmailAddress FROM dbo_ApplicantEmails ae
                     INNER JOIN dbo_Applicants a ON ae.ApplicantId = a.Id
                     WHERE a.Id = applicant.Id
                     ORDER BY ae.CreateTime DESC
                     LIMIT 1)                                        AS ApplicantEmail,
 (SELECT ap.`Number` FROM dbo_ApplicantPhones ap
                     INNER JOIN dbo_Applicants a ON ap.ApplicantId = a.Id
                     WHERE a.Id = applicant.Id AND ap.PhoneType = 3
                     ORDER BY ap.CreateTime DESC
                     LIMIT 1)                                        AS ApplicantMobileNumber,
 (SELECT GROUP_CONCAT(DISTINCT aa.PostCode SEPARATOR ', ')
                     FROM dbo_Applicants a 
                     LEFT JOIN dbo_ApplicantAddresses aa ON a.Id = aa.ApplicantId
                     WHERE applicant.Id = a.Id 
                         AND aa.IsPreferred = 1
                     GROUP BY a.Id)                                  AS ApplicantPostCode,
                     ocOrder.Id                                          AS OrderId,
                     ocOrder.OrderingCustomerTeamMemberId                AS OrderingCustomerTeamMemberId,    
                     ocOrder.OrderingCustomerId,
                     ocOrder.OrderingCustomerTeamId,
                 ocOrder.OrderingChannelId                           AS Origin,
                 ocOrder.CoverTypeId                                 AS TypeOfCover,
                 ocOrder.PolicyNumber                                AS PolicyNumber,				
                     (SELECT CASE WHEN NULLIF(AssignedToPrefix, '') IS NULL 
                             THEN AssignedTo 
                             ELSE CONCAT(AssignedToPrefix, ' - ', AssignedTo)
                             END
                 from dbo_OrderExamActivities OEA 
                 where OEA.OrderExamId = orderExam.Id 
                 order by CreateTime desc
                 LIMIT 1)                                            AS AssignedTo,
                 (SELECT CASE
                     WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 THEN 
                             (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
                             FROM dbo_OrderExamRequirements b
                             INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
                             INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
                             INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
                             WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
                             GROUP BY orderExam.Id)
                     WHEN examiner.ExaminerTypeId = 4 THEN
                             (SELECT GROUP_CONCAT( DISTINCT e.`Name` SEPARATOR ', ')
                             FROM dbo_OrderExamRequirements b
                             INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
                             INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
                             INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
                             WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 0 AND b.IsDeactivated = 0
                             GROUP BY orderExam.Id)
                     ELSE '' 
                 END)                                                AS Skill,
                 (SELECT CASE
                     WHEN examiner.ExaminerTypeId IN (0,1,2) OR examiner.ExaminerTypeId > 5 
                         THEN 
                             (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
                             FROM dbo_OrderExamRequirements b
                             INNER JOIN dbo_MedicalExaminations c ON b.MedicalExaminationId = c.Id
                             INNER JOIN dbo_MedicalExamination_Capabilities d ON c.Id = d.ExaminationId
                             INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
                             WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
                             GROUP BY orderExam.Id)
                     WHEN examiner.ExaminerTypeId = 4 
                         THEN
                             (SELECT GROUP_CONCAT(DISTINCT e.`Name` SEPARATOR ', ')
                             FROM dbo_OrderExamRequirements b
                             INNER JOIN dbo_FeeGroupExaminers c ON b.MedicalExaminationId = c.Id
                             INNER JOIN dbo_FeeGroupExaminerCapabilities d ON c.Id = d.FeeGroupExaminerId
                             INNER JOIN dbo_Capabilities e ON d.CapabilityId = e.Id
                             WHERE b.OrderExamId = orderExam.Id AND e.CapabilityTypeId = 1 AND b.IsDeactivated = 0
                             GROUP BY orderExam.Id)
                     ELSE '' 
                 END)                                                AS Qualification,
                 oc.CompanyName                                      AS OrderingCustomer,
 				oc.LastChargeReview                            		AS LastChargeReview,
                 CONCAT(applicant.FirstName, ' ', applicant.LastName) AS ApplicantName,
                 applicant.DateOfBirth                               AS ApplicantDoB,
                 (SELECT a.`Name` FROM dbo_Genders a 
                     WHERE a.Id = applicant.GenderId)                AS ApplicantGender,
                 applicant.LastName                                  AS ApplicantLastName,
                 applicant.FirstName                                 AS ApplicantFirstName,
                 examType.`Name`                                     AS ExamType,
                 IFNULL(dbo_GetTimeServiceForActivity ((SELECT Id 
                         FROM dbo_OrderExamActivities E 
                         WHERE E.OrderExamId = orderExam.`Id` 
                         ORDER BY E.CreateTime DESC 
                         LIMIT 1), NULL), 0)                        AS TimeService,
                 IFNULL(dbo_GetTotalTimeServiceForActivity (orderExam.`Id`), 0) AS TotalTimeService,
                 li.`Name`                                          AS LifeIndicator
                 from dbo_OrderExams orderExam
                 INNER JOIN dbo_Orders ocOrder ON ocOrder.Id = orderExam.OrderId
                 INNER JOIN dbo_OrderingCustomers oc ON oc.Id = ocOrder.OrderingCustomerId
                 INNER JOIN dbo_Applicants applicant ON applicant.Id = ocOrder.Id
                 INNER JOIN dbo_OcExamTypes examType ON orderExam.ExamTypeId = examType.Id
                 LEFT JOIN dbo_Examiners examiner ON orderExam.AssignedExaminerId = examiner.Id
                 LEFT JOIN dbo_LifeIndicators li ON li.`Id` = ocOrder.`LifeIndicatorId`
				 WHERE ocOrder.IsAnonymised = 0;
         
         SET @@group_concat_max_len = 256000;
         INSERT INTO dbo_OrderExamsReportsData 
         (
             ActivityCommentsAll,ActivityDescriptionsAll,AddressLine1,AddressLine2,AddressLine3,ApplicantDoB,ApplicantEmail,ApplicantFirstName,
             ApplicantGender,ApplicantLastName,ApplicantMobileNumber,ApplicantName,ApplicantPostCode,AppointmentScheduled, 
             AssignedCSR,AssignedTo,AssignedToPrefix,BestContact,BilledDate,City,ClinicName,CreatorName,
             CsrFirstName,CsrLastName,ExaminerFirstName,ExaminerLastName,
             ExaminerPrepaymentFlag,ExamOwnerId,ExamPostCode,ExamType,GpName,GpEmail,GPPracticePostCode,GroupPractice,GpPaymentType,GpAccountNo, GpSortCode,PayeeEmailAddress, HighNetWorth,Id,
             InvoiceReferenceNumber,IsActive,IsUrgentCase,LatestActivityComment,
             LatestActivityDescription,MiscFeeDescription,MiscChargeDescription,OcContact,OcServiceDescription,
             OrderDate,OrderExamNumber,OrderId,OrderingCustomer, LastChargeReview, OrderingCustomerTeam,OrderingCustomerTeamMemberId,Origin,PayeeName,PolicyNumber,PracticeName,Qualification,Requirement,
             RequirementProblemReason,RequirementStatus,Skill,`Status`,TypeOfCover,CreateTime,ExamLocation, ExaminerEmail,GPPracticeFax,TimeService,TotalTimeService,
             LabFeeTotal,MeFeeTotal,MiscFeeTotal,MiscCharge, OcCharge, LifeIndicator, BankName
         )
         SELECT 
                 temp.ActivityCommentsAll,   
                 temp.ActivityDescriptionsAll,
                 generalPractitioner.AddressLine1                    AS AddressLine1,
                 generalPractitioner.AddressLine2                    AS AddressLine2,
                 generalPractitioner.AddressLine3                    AS AddressLine3,    
                 temp.ApplicantDoB,  
                 temp.ApplicantEmail,
                 temp.ApplicantFirstName,
                 temp.ApplicantGender,
                 temp.ApplicantLastName,
                 temp.ApplicantMobileNumber,
                 temp.ApplicantName,
                 temp.ApplicantPostCode,
                 appointment.`Date`                                  AS AppointmentScheduled,
                 CONCAT(examOwner.FirstName, ' ', examOwner.LastName) AS AssignedCSR,
                 temp.AssignedTo,
                 (SELECT AssignedToPrefix 
                     from dbo_OrderExamActivities OEA 
                     where OEA.OrderExamId = orderExam.Id 
                     order by CreateTime desc 
                     LIMIT 1)                                        AS AssignedToPrefix,
                 generalPractitioner.BestContact                     AS BestContact,
                 (SELECT a.CreateTime FROM dbo_OrderExamActivities a
                     INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
                     WHERE a.OrderExamId = orderExam.Id AND a.ToExamState = 16
                     ORDER BY a.CreateTime
                     LIMIT 1)                                        AS BilledDate,
                 generalPractitioner.Town                            AS City,
                 clinic.`Name`                                       AS ClinicName,
                 (SELECT CONCAT(creator.FirstName, ' ', creator.LastName)
                     FROM dbo_OrderExamActivities OEA 
                     INNER JOIN dbo_Users creator ON OEA.CreatorId = creator.Id
                     WHERE OEA.PreviousActivityId IS null AND OEA.OrderExamId= orderExam.Id
                     ORDER BY OEA.CreateTime ASC 
                     LIMIT 1)                                        AS CreatorName,
                 examOwner.FirstName                                 AS CsrFirstName,
                 examOwner.LastName                                  AS CsrLastName,
                 medicalExaminer.FirstName                           AS ExaminerFirstName,
                 medicalExaminer.LastName                            AS ExaminerLastName,
                 (SELECT CASE (SELECT COUNT(*) FROM dbo_OrderExamActivities a
                                     INNER JOIN dbo_Activities b ON a.ActivityId = b.Id
                                     INNER JOIN dbo_OrderExams c ON a.OrderExamId = c.Id
                                     WHERE c.Id = orderExam.Id AND c.ExamTypeId = 4 
                                         AND b.`Code` IN('300'))
                                     WHEN 0 
                                     THEN 'No' 
                                     ELSE 'Yes' 
                                     END)                            AS ExaminerPrepaymentFlag,
                 examOwner.Id                                        AS ExamOwnerId,
                 appointment.PostCode                                AS ExamPostCode,
                 temp.ExamType,
                 TRIM(CONCAT(
                 generalPractitioner.Title, 
                 ' ', 
                 generalPractitioner.FirstName, 
                 ' ', 
                 generalPractitioner.LastName))                      AS GpName,
                 generalPractitioner.Email                           AS GpEmail,
                 generalPractitioner.PostCode                        AS GPPracticePostCode,
                 gpPracticeName.SurgeryName                          AS GroupPractice,                 
                 (CASE
					    WHEN generalPractitioner.PaymentType  = 1 THEN "Bacs"
					    WHEN generalPractitioner.PaymentType = 2 THEN "Cheque"
					    ELSE "-"
					END)  AS GpPaymentType,
                 generalPractitioner.AccountNo                       AS GpAccountNo,
                 generalPractitioner.SortCode                        AS GpSortCode,
				 generalPractitioner.PayeeEmailAddress				 AS PayeeEmailAddress,
                 (Select IFNULL(
                     (SELECT 1 From dbo_OrderExamRequirements R  
                     WHERE orderExam.Id = R.OrderExamId 
                     AND (R.`Code` = 'HV FEE' OR R.`Code` = 'UHV') 
                     AND R.IsDeactivated = 0 LIMIT 1), 0) )          AS HighNetWorth,
                 orderExam.Id                                        AS Id,
                 generalPractitioner.InvoiceReferenceNumber          AS InvoiceReferenceNumber,
                 orderExam.IsActive                                  AS IsActive,
                 orderExam.IsUrgent                                  AS IsUrgentCase,
                 (SELECT E.Note 
                     FROM dbo_OrderExamActivities E 
                     WHERE E.OrderExamId=orderExam.Id 
                     ORDER BY E.CreateTime DESC
                     LIMIT 1)                                        AS LatestActivityComment,
                 (SELECT b.`Description` 
                     FROM dbo_OrderExamActivities E 
                     INNER JOIN dbo_Activities b ON E.ActivityId = b.Id 
                     WHERE E.OrderExamId=orderExam.Id 
                     ORDER BY E.CreateTime DESC
                     LIMIT 1)                                        AS LatestActivityDescription,
                 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
                     FROM dbo_MiscellaneousCharges a
                     INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
                     WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 2
                     GROUP BY a.OrderExamId
                 )                                                   AS MiscFeeDescription,
                 (SELECT GROUP_CONCAT(DISTINCT a.`Description` SEPARATOR ', ')
                     FROM dbo_MiscellaneousCharges a
                     INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
                     WHERE a.OrderExamId=orderExam.Id AND a.MiscellaneousChargeType = 1
                     GROUP BY a.OrderExamId)                         AS MiscChargeDescription,
                 IFNULL(ocTeamMember.`Name`, '')                     AS OcContact,
                 (SELECT GROUP_CONCAT(DISTINCT E.`Description` separator ', ' )
                     FROM dbo_OrderExamRequirements E
                     WHERE E.OrderExamId = orderExam.Id 
                         AND E.StatusId NOT IN(3,4,5) 
                         AND E.IsDeactivated = 0
                     GROUP BY E.OrderExamId)                         AS OcServiceDescription,
                 orderExam.CreateTime                                AS OrderDate,
                 orderExam.ExamCustomId                              AS OrderExamNumber,
                 temp.OrderId,
                 temp.OrderingCustomer,
 				LastChargeReview,
                 team.`Name`                                         AS OrderingCustomerTeam,
                 temp.OrderingCustomerTeamMemberId,                           
                 temp.Origin,
                 generalPractitioner.PayeeName                       AS PayeeName,
                 temp.PolicyNumber,
                 generalPractitioner.PracticeName                    AS PracticeName,
                 temp.Qualification,
                 (SELECT GROUP_CONCAT(DISTINCT E.`Code` SEPARATOR ', ')
                     FROM dbo_OrderExamRequirements E
                     WHERE E.OrderExamId = orderExam.Id 
                         AND E.IsDeactivated = 0
                     GROUP BY E.OrderExamId)                         AS Requirement,
                 (SELECT a.Note FROM dbo_OrderExamActivities a
                     INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
                     INNER JOIN dbo_OrderExamRequirements d ON a.OrderExamId = d.OrderExamId
                     WHERE a.OrderExamId = orderExam.Id 
                         AND d.StatusId = 1 
                         AND d.IsDeactivated = 0
                     LIMIT 1)                                        AS RequirementProblemReason,
                 (SELECT GROUP_CONCAT(DISTINCT E.StatusId SEPARATOR ', ')
                     FROM dbo_OrderExamRequirements E
                     WHERE E.OrderExamId = orderExam.Id 
                         AND E.IsDeactivated = 0
                     GROUP BY E.OrderExamId)                         AS RequirementStatus,
                 temp.Skill,
                 (SELECT a.`Name` FROM dbo_OrderExamStatusTypes a 
                     WHERE a.Id = orderExam.StatusId)                AS `Status`,
                 temp.TypeOfCover,
                 NOW()                                               AS CreateTime,
                 TRIM(CONCAT(
                     IFNULL(appointment.AddressLine1, ''),
                     IFNULL(CONCAT(appointment.AddressLine2, ', '), ''),
                     IFNULL(CONCAT(appointment.AddressLine3, ', '), ''),
                     IFNULL(CONCAT(appointment.City, ', '), ''),
                     IFNULL(CONCAT(appointment.PostCode, ', '), ''),
                     IFNULL(CONCAT(appointment.Country, ', '), '')           
                 ))                                                  AS ExamLocation,
                 medicalExaminer.PreferredEmail                      AS ExaminerEmail,
                 generalPractitioner.Fax                             AS GPPracticeFax,
                 temp.TimeService,
                 temp.TotalTimeService,
                 0 AS LabFeeTotal,
                 0 AS MeFeeTotal,
                 0 AS MiscFeeTotal,
                  0 AS MiscCharge,
                  0 AS OcCharge,
                  temp.LifeIndicator,
                  dbo_Bank.Name as BankName 
             FROM dbo_OrderExams orderExam
                 inner join orderExamTempData temp on temp.orderExamId = orderExam.Id
                 LEFT JOIN dbo_OrderingCustomerTeams team ON temp.OrderingCustomerTeamId = team.Id
                 LEFT JOIN dbo_OrderExamAppointments appointment ON orderExam.Id = appointment.Id
                 LEFT JOIN dbo_MedicalExaminers medicalExaminer ON medicalExaminer.Id = orderExam.AssignedExaminerId
                 LEFT JOIN dbo_GeneralPractitioners generalPractitioner ON generalPractitioner.Id = orderExam.Id
                 LEFT JOIN dbo_GpPractices gpPracticeName ON gpPracticeName.Id = orderExam.AssignedExaminerId
                 LEFT JOIN dbo_GpPracticeIndividualContacts gpPractice ON gpPractice.Id = orderExam.OwnGpIndividualContactId
                 LEFT JOIN dbo_Clinics clinic ON clinic.Id = orderExam.AssignedExaminerId
                 LEFT JOIN dbo_Users examOwner ON examOwner.Id = orderExam.ExamOwnerId
                 LEFT JOIN dbo_ExaminerBankAccount EBA ON orderExam.AssignedExaminerId = EBA.ExaminerId 
                 LEFT JOIN dbo_Bank ON EBA.BankId = dbo_Bank.Id 
                 LEFT JOIN dbo_GridOrderingCustomerTeamMembers ocTeamMember ON temp.OrderingCustomerTeamMemberId = ocTeamMember.Id;
             
             
             UPDATE dbo_OrderExamsReportsData 
             JOIN (
                 SELECT  OrderExamId, SUM(IFNULL(A.ExaminerFee, 0)) SUMExaminerFee
                 FROM (
                     SELECT a.Id, a.OrderExamId, a.ExaminerFee
                         FROM dbo_OrderExamRequirements a
                     INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
                     INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
                     LEFT JOIN dbo_Examiners c ON b.AssignedExaminerId = c.Id AND c.ExaminerTypeId != 5
                     INNER JOIN dbo_AllowedExaminers ae ON a.MedicalExaminationId = ae.MedicalExaminationId 
                     AND (SELECT COUNT(*)    FROM dbo_AllowedExaminers aa WHERE aa.ExaminerType = 5 AND aa.MedicalExaminationId = a.MedicalExaminationId AND a.OrderExamId=BB.id) = 0    
                     WHERE a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType != 5
                             AND a.OrderExamId=BB.id
                     GROUP BY a.Id, a.OrderExamId, a.ExaminerFee
                 ) A
                 GROUP BY OrderExamId
             ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
             SET MeFeeTotal = CC.SUMExaminerFee;
             
             
             UPDATE dbo_OrderExamsReportsData
             JOIN (
                 SELECT  OrderExamId, SUM(IFNULL(SS.ExaminerFee,0)) SUMExaminerFee
                 FROM (
                     SELECT DISTINCT a.Id, a.OrderExamId, a.ExaminerFee
                         FROM dbo_OrderExamRequirements a 
                     INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
                     INNER JOIN dbo_MedicalExaminations m ON a.MedicalExaminationId = m.Id 
                     INNER JOIN dbo_AllowedExaminers ae ON m.Id = ae.MedicalExaminationId
                     INNER JOIN dbo_ExaminationFees f ON f.MedicalExaminationId = m.Id
                     INNER JOIN dbo_LaboratoryExaminations e ON f.Id = e.Id
                     WHERE a.OrderExamId = BB.Id AND a.StatusId NOT IN(3,4,5) AND a.IsDeactivated = 0 AND ae.IsActive = 1 AND ae.ExaminerType = 5
                     ) SS
                     GROUP BY SS.OrderExamId
                 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
                 SET LabFeeTotal=CC.SUMExaminerFee;
 
             
             UPDATE dbo_OrderExamsReportsData 
             JOIN (
                     SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
                         FROM dbo_MiscellaneousCharges a 
                         INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
                         INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
                         WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 2
                         GROUP BY a.OrderExamId
             ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
             SET MiscFeeTotal=CC.SUMCharge;
             
             
             UPDATE dbo_OrderExamsReportsData 
             JOIN (
                     SELECT SUM(IFNULL(a.Charge,0)) SUMCharge, a.OrderExamId
                         FROM dbo_MiscellaneousCharges a 
                         INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
                         INNER JOIN dbo_OrderExams b  ON a.OrderExamId = b.Id
                         WHERE a.OrderExamId = BB.Id AND a.MiscellaneousChargeType = 1
                         GROUP BY a.OrderExamId
             ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
             SET MiscCharge = CC.SUMCharge;
             
             
             UPDATE dbo_OrderExamsReportsData 
             JOIN (
                 SELECT SUM(IFNULL(a.OcCharge,0))  SUMOcCharge, a.OrderExamId 
                     FROM dbo_OrderExamRequirements a
                     INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
                     INNER JOIN dbo_OrderExams b ON a.OrderExamId = b.Id
                     WHERE a.OrderExamId = BB.Id AND a.StatusId != 5 AND a.IsDeactivated = 0
                     GROUP BY a.OrderExamId
                 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
             SET OcCharge = CC.SUMOcCharge;
             
             
             WITH cte AS ( SELECT NextActionDate, OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) AS rn FROM dbo_OrderExamActivities)
             UPDATE dbo_OrderExamsReportsData oerd
             JOIN cte ON cte.rn = 1 AND cte.OrderExamId = oerd.Id
             SET oerd.NextActionDate = cte.NextActionDate;
             
             
             UPDATE dbo_OrderExamsReportsData 
             JOIN (
                 SELECT MIN(a.CreateTime) CreateTime, a.OrderExamId 
                     FROM dbo_OrderExamActivities a
                     INNER JOIN dbo_OrderExamsReportsData BB ON BB.Id=a.OrderExamId
                     WHERE a.OrderExamId = BB.Id AND a.ToExamState IN(32,64,16384)
                     GROUP BY a.OrderExamId
                 ) CC ON CC.OrderExamId = dbo_OrderExamsReportsData.Id
             SET CompletedDate = CC.CreateTime;
             
             
             WITH cte AS 
                 (SELECT CONCAT(b.Description, CASE WHEN a.Note IS NULL THEN '' ELSE CONCAT(': ', a.Note) END) topNote, 
                 a.OrderExamId, ROW_NUMBER() OVER (PARTITION BY OrderExamId ORDER BY ActivityOrder DESC) 
                 AS rn 
                 FROM dbo_OrderExamActivities a 
                 INNER JOIN dbo_Activities b  ON a.ActivityId = b.Id 
                 WHERE a.ToExamState = 32)
             UPDATE dbo_OrderExamsReportsData 
             JOIN cte ON cte.rn=1 AND cte.OrderExamId = dbo_OrderExamsReportsData.Id
             SET CancellationReason = cte.topNote;
         
         IF `should_rollback` THEN
             ROLLBACK;
         ELSE
             COMMIT;
         END IF;
         SET autocommit = 1;
     END//
delimiter ;