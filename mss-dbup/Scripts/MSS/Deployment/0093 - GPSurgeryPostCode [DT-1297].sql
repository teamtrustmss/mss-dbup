START TRANSACTION;

	ALTER TABLE grid_AllOrderExams ADD GpSurgeryPostCode varchar(24) NULL;
	
	DROP TRIGGER afterInsert_GeneralPractitioner;
	
DELIMITER //
	CREATE TRIGGER `afterInsert_GeneralPractitioner` AFTER INSERT ON `dbo_GeneralPractitioners` FOR EACH ROW BEGIN
			UPDATE grid_AllOrderExams AOE
				JOIN dbo_OrderExams oe ON oe.Id = AOE.Id
			SET 
				GpSurgery = NEW.PracticeName,
				GpSurgeryPostCode = New.PostCode
			WHERE NEW.Id = oe.Id
			AND (oe.ExamTypeId = 4 OR oe.ExamTypeId = 6);
		END//

DELIMITER ;

	DROP TRIGGER afterUpdate_GeneralPractitioner;
	
DELIMITER //
	CREATE TRIGGER `afterUpdate_GeneralPractitioner` AFTER UPDATE ON `dbo_GeneralPractitioners` FOR EACH ROW BEGIN 
			UPDATE grid_AllOrderExams AOE 
				JOIN dbo_OrderExams oe ON oe.Id = AOE.Id
			SET 
				GpSurgery = NEW.PracticeName,
				GpSurgeryPostCode = New.PostCode
			WHERE NEW.Id = oe.Id
			AND (oe.ExamTypeId = 4 OR oe.ExamTypeId = 6);
		END//

DELIMITER ;
	
COMMIT;